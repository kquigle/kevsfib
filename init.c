#include "BubbleModel.h"
/*#include <gsl.h>*/
#include <gsl/gsl_rng.h>

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	BUILDING THE INITIAL STATE :- Init.c
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void initialisation(){
  char namefile5[100];
  FILE *fp0;
// I hope ran3 is defined

  float ran3(long *idum);
  long graine=150;

  float radius,z,x,vx,vz,drop;
  int i,j,k;

  double y0=Z1+10.5*R_B;
  double DD=((0.09-y0)/(2*R_B))/(NF+4);
  double DY=R_B*(2.5+poly);
  /*long graine=150;*/
  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  // If we start from scratch, we put the bubbles on a rectangular grid, with some noise
  if(init==-1) {



	// Now let's create horizontal fibers
	for (j = NGRAINS + 1; j <= NGRAINS + NFIBERS; j++) {
		i = (int)((j - NGRAINS) / LFIBERS) + 1;
		b0[j].velocity.x = 0.;
		b0[j].velocity.z = 0.;
		b0[j].radius = R_F;
		b0[j].fiber = (int)((j - NGRAINS - 1) / LFIBERS) + 1;
		if ((j - NGRAINS - 1) % LFIBERS == 0) {
			/*if(i%2==1)*/  b0[j].position.x = XCP(X1 + (X2 - X1)*ran3(&graine));
			// else b0[j].position.x=XCP(b0[j-1].position.x+8*R_B);
			b0[j].position.z = y0 + (i + 2)*DD*DY;//DD+delta/2.+(i-1.)*(DD+delta)+1.5*R_F*ran3(&graine);
		}
		else {
			b0[j].position.x = XCP(b0[j - 1].position.x + 1.8*R_F);
			b0[j].position.z = b0[j - 1].position.z + R_F*0.1*(ran3(&graine) - 0.5);
		}
		b0[j].mass = M_F;
		//bubble_volume = bubble_volume + M_PI*R_F*R_F;
	}

	// starting by putting the first bubble

    // starting by putting the first bubble
    b0[1].velocity.x=0.005*(ran3(&graine)-0.5);
    b0[1].velocity.z=0.005*(ran3(&graine)-0.5);
    b0[1].radius=R_G*(1.+poly*2.*(ran3(&graine)-0.5));
    b0[1].mass=pow(b0[1].radius,3)*4./3.*M_PI*DENSITY;
    // 		b0[1].moment=0.4*b0[1].mass*pow(b0[1].radius,2);
    b0[1].position.x=X1+2.5*b0[1].radius;
    // 	b0[1].position.z=Z1+LY/5.+b0[1].radius;
    b0[1].position.z=y0;
    // 	b0[1].theta=0.;
    // 	b0[1].omega=0.;
    b0[1].fiber=0;
    //bubble_volume=bubble_volume+PI*pow(b0[1].radius,2.);

    // then the other bubbles one by one
    for(j=2;j<=NGRAINS;j++){

      // We pack them by x-layers
	b0[j].velocity.x = 0.005*(ran3(&graine) - 0.5);
	b0[j].velocity.z = 0.002*(ran3(&graine) - 0.5);
	b0[j].radius = R_G*(1. + poly*2.*(ran3(&graine) - 0.5));

	b0[j].position.x = b0[j - 1].position.x + b0[j - 1].radius + b0[j].radius*1.5;
	b0[j].position.z = b0[j - 1].position.z;
	//		b0[j].position.z=b0[j-1].position.z;
	// 	b0[1].theta=0.;
	// 	b0[1].omega=0.;
	b0[j].fiber = 0;


	// Go to the next line in y
	if (b0[j].position.x>X2 - R_G*2.5) {
		b0[j].position.x = X1 + R_G + b0[j].radius*(1 + ran3(&graine));
		b0[j].position.z = b0[j].position.z + DY;
	}
	for (i = 0; i<NF; i++) {
		for (k = 1; k <= LFIBERS; k++) {
			if (distance(b0[j], b0[NGRAINS + i*LFIBERS + k])<b0[j].radius + R_F*1.8) {
				printf("oui voila %d %d %d %f %f \n", j, NGRAINS + i*LFIBERS + k, i, b0[j].position.x, b0[NGRAINS + i*LFIBERS + k].position.x);
				b0[j].position.x = b0[j].position.x + LFIBERS*R_F*1.8 + 3 * R_G;
			}
			if (b0[j].position.x>X2 - R_G*2.5) {
				b0[j].position.x = X1 + R_G + b0[j].radius*(1 + ran3(&graine));
				b0[j].position.z = b0[j].position.z + DY;
			}

		}
	}
    }
	//	if(b0[j].position.x>X2-R_G*2.5){//
	//	b0[j].position.x=X1+R_G+b0[j].radius*(1+ran3(&graine));
	//	b0[j].position.z=b0[j].position.z+DY;
	//	}


	b0[j].mass = pow(b0[j].radius, 3)*PIC*DENSITY;
	// 			b0[j].moment=0.4*b0[j].mass*pow(b0[j].radius,2);
	//bubble_volume = bubble_volume + M_PI*pow(b0[1].radius, 2.);

    // Now we stack bubbles on the walls, symmetrically
  for (j = NGRAINS + NFIBERS + 1; j <= NGRAINS + NFIBERS + NWALL / 2; j++) {
	  //   fscanf(fp0,"%f %f %f  \n",&x,&y,&radius);
	  if (j == NGRAINS + NFIBERS + 1) {
		  b0[j].position.x = X1;
		  b0[j + NWALL / 2].position.x = X1;
		  b0[j].position.z = Z1;
		  b0[j + NWALL / 2].position.z = b0[NGRAINS].position.z + 5 * R_G;
		  b0[j].radius = R_G;
		  b0[j + NWALL / 2].radius = R_G;
	  }
	  else
	  {
		  if (j == NGRAINS + NFIBERS + NWALL / 2) {
			  b0[j].radius = ((X2 - R_G) - (b0[j - 1].position.x + b0[j - 1].radius)) / 2.;
			  b0[j + NWALL / 2].radius = ((X2 - R_G) - (b0[j - 1 + NWALL / 2].position.x + b0[j - 1 + NWALL / 2].radius)) / 2.;
		  }
		  else {
			  b0[j].radius = R_G*(1. + poly*2.*(ran3(&graine) - 0.5));
			  b0[j + NWALL / 2].radius = R_G*(1. + poly*2.*(ran3(&graine) - 0.5));
		  }
		  b0[j].position.x = b0[j - 1].position.x + b0[j - 1].radius + b0[j].radius;
		  b0[j].position.z = b0[j - 1].position.z;
		  b0[j + NWALL / 2].position.x = b0[j + NWALL / 2 - 1].position.x + b0[j - 1].radius + b0[j].radius;
		  b0[j + NWALL / 2].position.z = b0[j + NWALL / 2 - 1].position.z;
	  }
	  b0[j].velocity.x = 0.;
	  b0[j].velocity.z = 0.;
	  b0[j].mass = pow(b0[j].radius, 3)*4. / 3.*M_PI*DENSITY;
	  b0[j + NWALL / 2].velocity.x = 0.;
	  b0[j + NWALL / 2].velocity.z = 0.;
	  b0[j + NWALL / 2].mass = pow(b0[j + NWALL / 2].radius, 3)*4. / 3.*M_PI*DENSITY;
	  //bubble_volume = bubble_volume + M_PI*pow(b0[1].radius, 2.);
  }	
  // We initialize the beams
//  for (j = 1; j <= NBEAMS; j++) {
//	  b1[j].actif = 0;
 // }



     

   

      //  b0[j].position.z=Z1;
      // 	else b0[j].position.z=b0[j-1].position.z+2*R_B;
      // 	if(fabs(b0[j].position.z)-R_B<=(DHOLE/2.)) b0[j].position.z=b0[j].position.z+DHOLE;
      // 	if(b0[j].position.z>Z2) b0[j].position.z=b0[j-1].position.z

  }

  // Else, we start by reading a position file, and we take the initial positions from there
  else{

    sprintf(namefile5,"./depart_poly30_DH5.dat");
    printf("I'm reading the file %s \n",namefile5);
    fp0=fopen(namefile5,"r");

    for(j=1;j<=NBUBBLES+NWALL+NHOLE;j++){

      fscanf(fp0,"%f \t %f \t %f \t %f \t %f ",&x,&z,&vx,&vz,&radius);
      for(k=0;k<NCONTACTS;k++)	fscanf(fp0,"%d",&i);

      b0[j].position.x=(double)(x);
      b0[j].position.z=(double)(z);

      b0[j].radius=(double)(radius);
      b0[j].mass=(double)(pow(b0[j].radius,3)*4./3.*M_PI*DENSITY);

      // 	if(j<=NBUBBLES)	bubble_area=bubble_area+M_PI*b0[j].radius*b0[j].radius;
      // 			else 	bubble_area=bubble_area+M_PI*R_B*R_B/2.;
      b0[j].velocity.x=0.;
      b0[j].velocity.z=0.;
    }
    // 	for(j=1;j<=NBUBBLES+NWALL;j++){
    //
    //
    // 		if(j<=NBUBBLES+NWALL/2){
    // 		b0[j].velocity.x=U0*b0[j].position.z/b0[NBUBBLES+NWALL].position.z;
    // 		b0[j].velocity.z=0.;
    // 		}
    // 		else{
    // 		b0[j].velocity.x=U0;
    // 		b0[j].velocity.z=0.;
    // 		}
    //
    // 	}



    fclose(fp0);
  }




  printf("The bubble assembly has been successfully built !\n");
  gsl_rng_free (r);
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	RANDOM NUMBER GENERATOR
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

float ran3(long *idum)
{
  static int inext,inextp;
  static long ma[56];
  static int iff=0;
  long mj,mk;
  int i,ii,k;

  if (*idum < 0 || iff == 0) {
    iff=1;
    mj=MSEED-(*idum < 0 ? -*idum : *idum);
    mj %= MBIG;
    ma[55]=mj;
    mk=1;
    for (i=1;i<=54;i++) {
      ii=(21*i) % 55;
      ma[ii]=mk;
      mk=mj-mk;
      if (mk < MZ) mk += MBIG;
      mj=ma[ii];
    }
    for (k=1;k<=4;k++)
      for (i=1;i<=55;i++) {
        ma[i] -= ma[1+(i+30) % 55];
        if (ma[i] < MZ) ma[i] += MBIG;
      }
    inext=0;
    inextp=31;
    *idum=1;
  }
  if (++inext == 56) inext=1;
  if (++inextp == 56) inextp=1;
  mj=ma[inext]-ma[inextp];
  if (mj < MZ) mj += MBIG;
  ma[inext]=mj;
  return mj*FAC;
}
#undef MBIG
#undef MSEED
#undef MZ
#undef FAC

