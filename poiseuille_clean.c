#include "holedef.h"
#include "BubbleModel.h"


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	GLOBAL VARIABLES :-  poiseuille_clean.c
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

// Time t
double t;
// Dimensions of the box
double LX=X2-X1;
double LZ=Z2-Z1;

// Working directory
char directory[70];

// The lists of bubbles
//CAREFUL: first bubble is j=1
sphere b1[NBUBBLES+NWALL+NFIBERS+1],b2[1+NBUBBLES+NWALL+NFIBERS],b0[1+NBUBBLES+NWALL+NFIBERS];

// Total force on a bubble
force forces[NBUBBLES+NWALL+1+NFIBERS];
// Total viscous force on a bubble
force forcesv[NBUBBLES+NWALL+1+NFIBERS];

// Contact lists
int HoL[NX+1][NZ+1];
int AL[1+NBUBBLES+NWALL+NFIBERS],PL[1+NBUBBLES+NWALL+NFIBERS];



// Total forces on the walls: transverse (upper wall), normal (bottom wall), tangential (viscous part)
double upforce=0.,doforce=0.,xforceg=0.,xforced=0.;

// Liquid fraction and bubble area
double liquid_fraction=0.;
double bubble_area=0.;

// Various things
double dt2=dt*dt;
double dteff=dt;
double Z3=Z2;
double U1=U0;
double tstart;
int iter=2;
double cste_stokes;
double overlap=0.,overlap1=0.,overlap0=0.;
int i_cell,k_cell,iv,kv;
int switchy=0,switchy0;

int debit=0;





// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	MAIN ROUTINE
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

// Takes as an argument the number of the directory
int main(int argc, char ** argv)
{
  char chaine[70];

  printf("Let's go !\n");


  // creer les directorys
  sprintf(directory,"./sablier_%s",argv[1]);
  sprintf(chaine,"mkdir ./sablier_%s",argv[1]);

  printf("We'll be working in the directory%s \n",directory);
  system(chaine);

  sprintf(chaine,"cp ./holedef.h %s",directory);
  system(chaine);

  sprintf(chaine,"cp ./newhole_1.15.c %s",directory);
  system(chaine);

  liquid_fraction=0.;
  bubble_area=0.;
  cste_stokes=Facteur*VISCOSITY*R_B;

  // We start by building the initial state of the foam
  initialisation();

  printf("We have %d bubbles in the bulk and %d on the edges) \n",NBUBBLES, NWALL);


  double TCOLL=M_PI*sqrt(M_B/K_B);

  printf("The time of viscous relaxation is %e \n",tau_d);
  printf("The inertial time is  %e \n",tau_v);

  printf("The elastic timescale is %e   \n",TCOLL);

  //printf("The shear timescale %e \n",b0[NBUBBLES+NWALL].position.z/U0);

  printf("The Deborah number is %e \n",U0/b0[NBUBBLES+NWALL].position.z*tau_d);

  printf("The typical overlap is %e \n",U0/b0[NBUBBLES+NWALL].position.z*R_B*friction_coeff/K_B);


  printf("Let's go, then \n \n \n");


  evolution();

  printf("\n I love it when a plan comes together \n");

  return 0;
}
