// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	STRUCTURES  :- couettevisu.c
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
#include "./stress_c80_0.1/couettedef.h"


int boucle;
float couleur;
float red,green,blue;
int ref;
double scale=200000.;
double vscale=40.;

double LX=(X2-X1);
double LZ=(Z2-Z1);

int main(int argc, char ** argv) 
{

	FILE *fp,*fp4,*fp6,*fp42;
	char namefile[55],namefile6[50];
	int i,j,k;	
	float rayon,z,x,vx,vz,omega;
	float vr,vrx,vrz,drop;
	char namefile2[55],commande[255],repertoire[50];
	int initial;
	int v1,v2,v3,v4;
	double U1;
	if(U0==0.) U1=1.;
	else U1=U0;

	initial=atoi(argv[1]);
sprintf(repertoire,"./stress_%s",argv[2]);
//	printf("ENTRER UN NUMERO DE DOSSIER: ");
//	scanf("%d",&ref);
//	printf("%d \n",ref);

//	ref=10;

//	sprintf(namefile6,"%s/trajectoire.dat",repertoire);
//	fp6=fopen(namefile6,"w");

	for(boucle=initial;boucle<=19900;boucle+=1){
//	boucle=10;

// Le fp4 a lire
	sprintf(namefile2,"%s/positions%.6d.dat",repertoire,boucle);
	fp4=fopen(namefile2,"r");
//	sprintf(namefile2,"%s/voisinage%.6d.dat",repertoire,boucle);
//	fp42=fopen(namefile2,"r");

	if(fp4==NULL) {
	break;
	}

// Le fp4 a ecrire
	sprintf(namefile,"%s/test%.6d.eps",repertoire,boucle);
	fp=fopen(namefile,"w");	

//	fprintf(fp,"%%!PS-Adobe-3.0 EPSF-3.0 \n%%%%BoundingBox: 0 %d %d 800 \n",907-30-15*NY,25+15*NX);
	fprintf(fp,"%%!PS-Adobe-3.0 EPSF-3.0 \n%%%%BoundingBox: 0 0 1000 1000 \n");
	fprintf(fp,"%%%%Creator: Vincent.Langlois@ens-lyon.org \n");
	fprintf(fp,"%%%%EndComments \n");
	fprintf(fp,"%%%%DocumentFonts: Courier Times-Bold \n");

	
	fprintf(fp,"/L{2 setlinewidth rlineto gsave stroke grestore}def \n /M{moveto}def  \n");
	fprintf(fp,"/LC{1.00 1 sethsbcolor 2 setlinewidth rlineto gsave stroke grestore}def  \n");
	fprintf(fp,"/LV{1.00 1 sethsbcolor 4 setlinewidth rlineto gsave stroke grestore}def  \n");
	fprintf(fp,"/cercle{1.000000 1 sethsbcolor 3.5 setlinewidth newpath 0 360 arc closepath gsave grestore stroke}def \n");
	fprintf(fp,"/cercleNB{setgray 2.5 setlinewidth newpath 0 360 arc closepath gsave grestore stroke}def \n");
	fprintf(fp,"/tambour{1.000000 0 sethsbcolor 5.1 setlinewidth newpath 0 360 arc closepath gsave grestore stroke}def \n");
	fprintf(fp,"/rect{1. 1 sethsbcolor newpath rectfill closepath gsave grestore stroke}def \n");

	fprintf(fp,"0.5 0.5 scale \n \n \n");
 	fprintf(fp,"%f %f translate \n \n",-X1*scale+20,-Z1*scale+20);

// Horloge

// 	fprintf(fp,"/Helvetica findfont \n 48 scalefont setfont \n");
// 	fprintf(fp,"-300 1000 M \n (t= %d) show \n",boucle);
// 
// // Cadre de la petite barre	
// 	fprintf(fp,"-1 1021 M  502 0 L  0 -22 L  -502 0 L  0 22 L \n");
// 
// 	fprintf(fp,"-1 1021 M  502 0 L  0 -22 L  -502 0 L  0 22 L \n");

//	fprintf(fp,"%f 0 M  %f 0 L \n",-LX*scale,2*LX*scale);

// Cadre du spatio	

//	fprintf(fp,"-2 907 M  %d 0 L  -2 907 M  0 %d L  -1 1021 M\n",25+15*NX,-15-15*NY);
//	fprintf(fp,"%d 907 M  0 %d L   %d 0 L  -1 1021 M\n",-2+25+15*NX,-15-15*NY,-25-15*NX);
// 	fprintf(fp,"0 0 M  %f 0 L  0 %f L %f 0 L 0 %f L \n",scale*LX,scale*(Z2-Z1),-scale*LX,-scale*(Z2-Z1));
// 	fprintf(fp,"0 0 M  %f 0 L  0 %f L %f 0 L 0 %f L \n",scale*LX,scale*(Z3-Z1),-scale*LX,-scale*(Z3-Z1));
// 	for(i=0;i<=NZ;i++) fprintf(fp,"%f %f M %f 0 L\n",-scale*RTAMBOUR,scale*(i*LZ/NZ-RTAMBOUR),scale*LX);
// 	for(i=0;i<=NX;i++) fprintf(fp,"%f %f M 0 %f L \n",scale*(i*LX/NX-RTAMBOUR),-scale*RTAMBOUR,scale*LZ);


// fprintf(fp,"%f %f M  %f %f L \n",-scale*3*R_G,scale*0.03,-scale*RTAMBOUR,0.);
// fprintf(fp,"%f %f M %f %f L \n",scale*3*R_G,scale*0.03,scale*RTAMBOUR,0.);

// Graduation axe horizontal
/*
	for(k=0;k<=16;k+=1) {
	
	fprintf(fp,"%f 907 M 0 5 L \n",k*0.1*1024/1.6-2);
	fprintf(fp,"%f 907 M 0 10 L \n",k*0.5*1024/1.6-2);
	
	}	
*/	
// On fabrique la petite barre qui indique les couleurs

// 	for(k=0;k<=1000;k++){
// 	
// 	couleur=.75-k*.75/1000.;
// 
// 	fprintf(fp,"%f %d  .3 20 %f rect\n",k/2.,1000,couleur);
// 
// 	}
	

// Maintenant le vrai spatio-temporel

	//lecture_positions(boucle);
	fprintf(fp,"/Helvetica findfont \n 30 scalefont setfont \n");
	for(i=1;i<=NBUBBLES+NWALL;i++){

// 	printf("coucou   %d \n",i);
// 	fscanf(fp4,"%f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f",&x,&z,&vx,&vz,&drop,&rayon,&drop,&drop,&drop,&drop);
	fscanf(fp4,"%f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f",&x,&z,&vx,&vz,&rayon,&drop,&drop,&drop,&drop);

//	fscanf(fp42,"%d \t %d \t %d \t %d ",&v1,&v2,&v3,&v4);
//	printf("coucou   %d %f %f %f \n",i,x,z,rayon);

//	fprintf(fp6,"%.12f \t %.12f \t %.12f \t %.12f \t %.12f \n",x,z,vx,vz,omega);

	if(i<=NBUBBLES){
//	couleur=.75*(1-sqrt(vx*vx+vz*vz));
//	couleur=.75*(1-(v1)/2.);
//	couleur=.75-omega;
//	else
// 	if(z>0){
// 	vrx=-fabs(OMEGA_TAMBOUR*sqrt(x*x+z*z)*z/(x*sqrt(1+z*z/(x*x))));}
// 	else{
// 	vrx=fabs(OMEGA_TAMBOUR*sqrt(x*x+z*z)*z/(x*sqrt(1+z*z/(x*x))));}
// 
// 	if(x>0){
// 	vrz=OMEGA_TAMBOUR*sqrt(x*x+z*z)/(sqrt(1+z*z/(x*x)));
// 	}else{
// 	vrz=-OMEGA_TAMBOUR*sqrt(x*x+z*z)/(sqrt(1+z*z/(x*x)));}
// 
// 	vr=sqrt((vx-vrx)*(vx-vrx)+(vz-vrz)*(vz-vrz));
// 	couleur=.75*(1.-vr/sqrt(4.*CONSTANTE_G*R_G)*10);

	

// 	couleur=.75*(1-sqrt(vx*vx+vz*vz)/sqrt(4.*CONSTANTE_G*R_G));
// if(j%2==0) couleur=0.75;
// else couleur=0.;



	couleur=.75*(1-sqrt(vx*vx)/U1);

// 	if(rayon<0.7*R_G)	couleur=.5;
// 	else 	couleur=0.;	
// 	if(rayon>R_G*0.7) couleur=0.;
// 	else couleur=0.5;

//	if(i==224) couleur=0.5;
//	if(i==237) couleur=0.5;

//	fprintf(fp,"%f %f %f %f cercle\n",x*scale,z*scale,rayon*scale,couleur);
//	if(i==984) 	
	fprintf(fp,"%f %f %f 0. cercleNB \n",x*scale,z*scale,rayon*scale);

//	fprintf(fp,"%f %f %f %f cercle\n",x*scale,(z+LZ)*scale,rayon*scale,couleur);
// if(i==1000)	fprintf(fp,"%f %f M %f 1 1 sethsbcolor (%d) show \n",x*scale,z*scale,couleur,i);
	}

	else{ 
	couleur=0.6;
//	if(i==NBUBBLES+NWALL) couleur=0.;
//	if(i==1034) couleur=0.;
	fprintf(fp,"%f %f %f %f cercleNB \n",x*scale,z*scale,rayon*scale,couleur);
// if(i==1055)	fprintf(fp,"%f %f M %f 1 1 sethsbcolor (%d) show \n",x*scale,z*scale,couleur,i);
	}



//	else couleur=0.75;


//	fprintf(fp,"%f %f M %f %f %f LC\n",x*scale,z*scale,vx*vscale,vz*vscale,sqrt(vx*vx+vz*vz));
	}


	
	fprintf(fp,"\n \n \nshowpage");
	
  	fclose(fp);
	fclose(fp4);
//	fclose(fp42);

	sprintf(commande,"convert %s/test%.6d.eps %s/test%.6d.jpg",repertoire,boucle,repertoire,boucle);
	system(commande);


 	printf("coucou   %d \n",boucle);

}

	sprintf(commande,"rm %s/test*.eps",repertoire);
	system(commande);

//	sprintf(commande,"cd %s ; mencoder mf://*.jpg -mf w=800:h=600:fps=10:type=jpg -ovc copy -oac copy -o output.avi",repertoire);
//	system(commande);

//fclose(fp6);


return 0;
}
