CC := gcc
CFLAGS := -Wall -g
LFLAGS := -lm -lgsl -lgslcblas
TARGETS := poiseuille_clean.exe
OBJECTS := init.o equil.o misc.o collisionBounce.o poiseuille_clean.o

all:$(TARGETS)

%.exe:$(OBJECTS) holedef.h
	$(CC) $(OBJECTS) $(LFLAGS) -o $@

%.o:%.c holedef.h
