#include "BubbleModel.h"

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	EVOLUTION ROUTINE    :-  equile.c
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void evolution() {
  char namefile3[70],namefile2[70],namefile0[90];
  //char namefile5[30],namefile1[30],
  FILE *fp2,*fp5,*fp3=0;
  int init2,ic,kc,j,k;
  float ran3(long *idum);
  force bb;

  // The contact lists are set to zero
  for(ic=0;ic<=NX-1;ic++)
    for(kc=0;kc<=NZ-1;kc++){
      HoL[ic][kc]=0;
    }
  PL[0]=0;
  AL[0]=0;

  printf("Contact list is ready !\n");
  printf("Checkng for values,%s,%f,%f,%f,%f",directory,phi0,sinTHETA,Facteur,DHOLE);	

  // We create useful files

  sprintf(namefile0,"%s/debit_%.2f_%f_%.1f_%.1f.dat",directory,phi0,sinTHETA,Facteur,DHOLE);
  // 		sprintf(namefile0,"%s/debit_%s.dat",directory,directory);
  fp5=fopen(namefile0,"a");
  if (fp5 != NULL){
    fclose(fp5);
  }

  if(init==-1){
    sprintf(namefile2,"%s/positions000001.dat",directory);
    fp2=fopen(namefile2,"w");
  }
  else {
    sprintf(namefile2,"%s/positions%.6d.dat",directory,init+1);
    fp2=fopen(namefile2,"w");
  }

  sprintf(namefile3,"%s/contacts000.dat",directory);
  //		fp1=fopen(namefile3,"w");



  // We initialize all bubbles
  for(j=1;j<=NBUBBLES+NWALL+NHOLE;j++){
    // b0 is the bubble at timestep  i-1
    // b1                                       i
    // b2                                       i+1

    b1[j]=b0[j];
    b2[j]=b0[j];

    PL[j]=0;
    AL[j]=0;
    forces[j].x=0.;
    forces[j].z=0.;
    forcesv[j].x=0.;
    forcesv[j].z=0.;
    // and print the first position file
    fprintf(fp2,"%.12f \t %.12f \t %.12f \t %.12f \t %.6f ",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius);
    for(k=0;k<NCONTACTS;k++) fprintf(fp2,"%d \t",b1[j].voisins[k]);
    fprintf(fp2,"\n");
  }


  if(init==-1) fclose(fp2);

  printf("Please fasten your seat belt\n");

  if(init==-1) {
    init2=0;
    switchy=0.;
    Z3=Z2;
  }
  else {
    switchy=1.;
    init2=init;
    Z3=HFINAL/2.;
  }




  // The first iteration has to be integrated with an Euler method
  iter=init2*print_step+1;


  // what's the time ?
  t=iter*dt;

  // For each bubble, we compute the cell number, and adjust the neighbourhood
  for(j=1;j<=NBUBBLES+NWALL+NHOLE;j++){

    b1[j].cell.i=CP((int)((b1[j].position.x-X1)*NX/LX));
    b1[j].cell.k=(int)((b1[j].position.z-Z1)*NZ/LZ);

    PL[j]=HoL[b1[j].cell.i][b1[j].cell.k];
    AL[HoL[b1[j].cell.i][b1[j].cell.k]]=j;
    HoL[b1[j].cell.i][b1[j].cell.k]=j;
  }



  // Now for each bubble except the walls
  for(j=1;j<=NBUBBLES;j++){
    b2[j]=b1[j];
    //Looking for contacts
    contacts(j,switchy);
    // and integrate new positions
    euler(j);
    if(fabs(b2[j].position.x)<.1e-4) printf("zeopuiioi %d \t%f \t%f\t %f\t %f  \n",j,b1[j].position.x,b2[j].position.x,b0[j].velocity.x,b1[j].velocity.x);
  }

  //fclose(fp1);

  // Now for each bubble
  for(j=1;j<=NBUBBLES+NWALL+NHOLE;j++){
    // we update the contact list
    list_update(j);
    // and replace old positions by new ones
    b0[j].position=b1[j].position;
    b0[j].velocity=b1[j].velocity;
    b1[j].position=b2[j].position;
    b1[j].velocity=b2[j].velocity;
    // all forces are reset to zero
    forces[j].x=0.;
    forces[j].z=0.;
    forcesv[j].x=0.;
    forcesv[j].z=0.;
  }



  // NOW WE CAN GO FOR THE REAL THING (Verlet integration)

  printf("You signed up for %d iterations, with U0=%f and phi0= %f \n\n\n",NITER,U0,phi0);

  // switchy=0;
  for(iter=2;iter<=NITER;iter++){

    // 	bubble_area=0.;
    t=iter*dt;



    // For each bubble
    for(j=1;j<=NBUBBLES;j++){
      b2[j]=b1[j];

      //		if(iter>830000) printf("test 1 %d\n",j);

      // bubble_area=bubble_area+M_PI*b1[j].radius*b1[j].radius;

      // If we have solid walls and/or friction along the confining plate and/or gravity
      // printf("switchy %d %.16f\n",switchy,(float)(switchy)*GRAVITY*sinTHETA*b1[j].mass);
      bb=bounce(b1[j]);
      forces[j].x=forces[j].x-switchy*cste_stokes*b1[j].radius/R_B*b1[j].velocity.x+bb.x;
      forces[j].z=forces[j].z-switchy*cste_stokes*b1[j].radius/R_B*b1[j].velocity.z+bb.z;

      if(b1[j].position.x<XP) forces[j].x=forces[j].x+switchy*GRAVITY*sinTHETA*b1[j].mass;

      //		if(iter>830000&&j==822) printf("test 2 %d %f %f %d %d %d %f \n",j,b1[j].position.x/R_B,b1[j].position.z/R_B,b1[j].voisins[0],b1[j].voisins[1],b1[j].voisins[2],Z3/R_B);
      // Let's look for all contacts
      contacts(j,switchy);
      //			if(iter>860000&&j==822) printf("test 3 %d\n",j);
      //if(b1[822].position.z>Z3) break;

      // and integrate over time
      verlet(j);

      //		if(iter>860000) printf("test 4 %d\n",j);
      if(fabs(b1[j].position.x)<.1e-4) printf("weird %d %d \t%f \t%f\t %f\t %f  \n",iter,j,b1[j].position.x,b2[j].position.x,b1[j].velocity.x,b2[j].velocity.x);
    }

    // Computing the forces on the wall bubbles
    // for(j=NBUBBLES+NWALL/2+1;j<=NBUBBLES+NWALL;j++){
    // 	upforce=upforce+forces[j].z;
    // 	xforcev=xforcev+forcesv[j].x;
    // 	xforcee=xforcee+forces[j].x-forcesv[j].x;
    // // bubble_area=bubble_area+M_PI*R_B*R_B/2.;
    // }
    // for(j=NBUBBLES+1;j<=NBUBBLES+NWALL/2;j++){
    // 	doforce=doforce+forces[j].z;
    // 	xforcev=xforcev-forcesv[j].x;
    // 	xforcee=xforcee-forces[j].x+forcesv[j].x;
    // // bubble_area=bubble_area+M_PI*R_B*R_B/2.;
    // }
    //Computing the liquid fraction
    // liquid_fraction=1.-bubble_area/((X2-X1)*(b1[NBUBBLES+NWALL-1].position.z-Z1));

    // We impose the motion of the upper wall bubbles:
    // for(j=NBUBBLES+NWALL/2+1;j<=NBUBBLES+NWALL;j++){
    // // First downward to give the foam the chosen liquid fraction
    // 	if(b1[j].position.z>HFINAL/2.&&switchy==0)	{
    // 	b2[j].position.z=b1[j].position.z-0.1*dt;
    // 	Z3=b2[j].position.z;
    // 	U1=0.;
    // 	b2[j].position.x=b1[j].position.x;
    // 	}
    // // and then with a given x-velocity U0
    // 	else{
    // 	switchy=1;
    // 	b2[j].position.z=b1[j].position.z;
    // 	b2[j].velocity.x=U0;
    // 	b2[j].position.x=XCP(b1[j].position.x+U0*dt);
    // 	}
    // }

    //Vertical motion of the walls if no side bubbles
    // First downward to give the foam the chosen liquid fraction
    if(Z3>HFINAL/2.&&switchy==0)	{
      for(j=NBUBBLES+NWALL+1;j<=NBUBBLES+NWALL+NHOLE/2.;j++){
        b2[j].position.z=b1[j].position.z-VDOWN*dt;
        b2[j].velocity.z=-VDOWN;
        b2[j].position.x=b1[j].position.x;
      }
      Z3=Z3-VDOWN*dt;
      // 	switchy=1;
      // 	U1=0.;
      // 	b2[j].position.x=b1[j].position.x;
    }
    // and then with a given x-velocity U0
    else{
      switchy=1;
      // 	b2[j].position.z=b1[j].position.z;
      // 	b2[j].velocity.x=U0;
      // 	b2[j].position.x=XCP(b1[j].position.x+U0*dt);
    }



    // Every 'print_step' timesteps, we print out a position file, and other useful stuff
    if(iter%print_step==0) {
      sprintf(namefile3,"%s/positions%.6d.dat",directory,(int)(init2+iter/print_step));
      fp3=fopen(namefile3,"w");

      // Print out positions, speeds, forces for each bubble
      for(j=1;j<=NBUBBLES+NWALL+NHOLE;j++){
        // 		fprintf(fp3,"%.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.6f \t %.12f \t %.12f \t %.12f  \n",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius,forces[j].x-forcesv[j].x,forcesv[j].x,forces[j].z-forcesv[j].z,forcesv[j].z);
        fprintf(fp3,"%.12f \t %.12f \t %.12f \t %.12f \t %.12f \t ",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius);
        for(k=0;k<=NCONTACTS-1;k++) fprintf(fp3,"%d \t",b1[j].voisins[k]);
        fprintf(fp3,"\n");
      }

      // Give some news on screen
      printf("hello there i=%d ... gamma= %f     epsilon=%f     U1=%d    Z3=%f \n",iter,t*U0/b1[NWALL+NBUBBLES].position.z,liquid_fraction,switchy,Z3);

      // Print out the stresses on walls
      fp5=fopen(namefile0,"a");
      //	 fprintf(fp5,"%.6f \t %.12f \t %.12f \t %.12f \t %.12f \t %.6f \n",t*U0/b1[NWALL+NBUBBLES].position.z,-xforcev/print_step,-xforcee/print_step,-(xforcev+xforcee)/print_step,(upforce-doforce)/print_step,t);
      fprintf(fp5,"%.16f \t %d \t %.16f \t %.16f \n",t,debit,xforceg/print_step,xforced/print_step);
      fclose(fp5);

      // Reset to zero all global forces
      //upforce=0.;
      //doforce=0.;
      xforceg=0.;
      xforced=0.;

      fclose(fp3);

    }


    // Updating contact lists and bubbles
    for(j=1;j<=NBUBBLES+NWALL+NHOLE;j++){


      list_update(j);
      b0[j]=b1[j];
      b1[j]=b2[j];


      // Reset forces to zero
      forces[j].x=0.;
      forces[j].z=0.;
      forcesv[j].x=0.;
      forcesv[j].z=0.;
      for(k=0;k<=NCONTACTS-1;k++) b1[j].voisins[k]=0;
    }

    switchy0=switchy;


  }

  return;
}

// Time-integration of positions, with 1st-order Euler method
void euler(int j){
  b2[j].position.x=XCP(b1[j].position.x+dt*b1[j].velocity.x);
  b2[j].velocity.x=b1[j].velocity.x+dt*(forces[j].x/b1[j].mass);
  b2[j].position.z=b1[j].position.z+dt*b1[j].velocity.z;
  b2[j].velocity.z=b1[j].velocity.z+dt*(forces[j].z/b1[j].mass);
  // 			if(fabs(b2[j].position.x)<.1e-4) printf("weird %d %f %f %f %f  \n",j,b1[j].position.x,b2[j].position.x,b1[j].position.z,b2[j].position.z);
  return;
}

// Time-integration of positions and speeds, with 2nd-order Verlet method
void verlet(int j){
  double dx;

  // 	if(switchy==1&&b1[j].position.x>X1&&b1[j].position.x<X1+2.*R_B) {
  // 	  b2[j].velocity.x=U0;
  // 	  b2[j].position.x=XCP(b1[j].position.x+dt*U0);
  // 	  b2[j].position.z=b1[j].position.z;
  // 	  b2[j].velocity.z=0.;
  // // 	  printf("happens %d \n",j);
  // 	}
  // 	else{
  b2[j].position.x=XCP(2.*b1[j].position.x-b0[j].position.x+dt2*forces[j].x/b1[j].mass);

  b2[j].position.z=2.*b1[j].position.z-b0[j].position.z+dt2*forces[j].z/b1[j].mass;
  b2[j].velocity.z=0.5*(b2[j].position.z-b0[j].position.z)/dt;

  // Careful if we are on the sides
  dx=b2[j].position.x-b0[j].position.x;
  if(dx<-LX/2.){
    dx=dx+LX;
  }
  else{
    if(dx>LX/2.) {
      dx=dx-LX;
    }
  }

  if(b2[j].position.x>0.&&b1[j].position.x<0.) debit=debit+1;


  b2[j].velocity.x=0.5*dx/dt;

  if(fabs(b2[j].position.x)<.1e-4) printf("plicploc %d \t%f \t%f\t %f\t %f  \n",j,b1[j].position.x,b2[j].position.x,b1[j].velocity.x,b2[j].velocity.x);

  // 	}
  return;
}


