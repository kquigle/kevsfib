//	3D BUBBLE MODEL SERIAL CODE last update 17/03/2014 V. Langlois
//	3D CUDA MODEL PARALLEL CODE last update 22/05/2015 S. Murray
// 3D CUDA GRANULAR CODE V. Langlois
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

#include "fcouette2D.h"
// #include "sphere.h"
#ifndef vector_struct
#define vector_struct
typedef struct{
  double x;
  double y;
} vector;
#endif
#ifndef force_struct
#define force_struct

typedef struct{
  double x;
  double y;
} force;
#endif
#ifndef cell_struct
#define cell_struct

typedef struct{
  int bubin[9];
  int bubout[9];
  int countin;
  int countout;
} cell_up;
#endif
#ifndef vectorint_struct
#define vectorint_struct
// An int 2-vector
typedef struct{
  int i;
  int j;
} vectint;
#endif
#ifndef __contact_list__
#define __contact_list__
typedef struct{
  int contacts[9];
  int count;
} contact_list;
#endif
#ifndef __overlap_list__
#define __overlap_list__
typedef struct{
  double overlap[9];
} overlap_list;
#endif
#include<time.h>


// A sphere
typedef struct{
  vector position;
  double theta;
  vector velocity;
  double omega;
  double radius;
  double mass;
  double moment;
  vectint cell;
  int fiber;
} sphere;

// A beam
typedef struct{
  unsigned int j1;
  unsigned int j2;
  double longueur;
  double tx;
  double ty;
  int actif;
} beam;


//// Must keep WALL BLOCK SIZE AT 256 for reduction purposes
#define BLOCKSIZEXWALL 256
///////////////////////////////////////////////////////////
#define BLOCKSIZEXFIBERS 256
// List of optimum blocksizes on my system 				System size:	38400	4800	300
#define BLOCKSIZEXBULKNEW 288								// They will differ on different systems								NA		92		288
#define BLOCKSIZEXBULKNOSTREAMS 128									// I ran code on a Nvidia K40c										224		NA		NA
#define BLOCKSIZEXBULKPART2 64										//																NA		NA		64
#define BLOCKSIZEXBULKPARED 64										//																64		96		NA
#define BLOCKSIZEXBULKPAREDPart22 96								//																	96		32		NA
#define BLOCKSIZEVERLET 320											//																320		160		32
#define BLOCKSIZEXMOVEWALLS 64										//																64		96		32
#define BLOCKSIZEXPARALLEL 288										//																288		448		32
#define BLOCKSIZEXCELL 32											//																32		32		32
#define BLOCKSIZEXUPDATE 288										//																288		512		64
#define BLOCKNUMBERUPDATE 96


#define BLOCKSIZEX 64
#define BLOCKSIZEXBULK 32


#define part2_Max_threads 352
#define part2_min_blocks 5

#define part1_Max_threads 352
#define part1_min_blocks 5

// #define WITH_HOUSTON

//#define with_error_checking
// #define part1_TLP       // Thread level parallelism, use on small system sizes.
#define part1_ILP     // Instruction level parallelsm, use on large system sizes.

// #define part2_TLP		 //	Thread level parallelism, use on small system sizes.
#define part2_ILP 	 // Instruction level parallelism, use on medium-large system sizes.

//#define launch_bounds
#define prefetch_Max_threads 352
#define prefetch_min_blocks 5

#define shared_diff_Max_threads 352
#define shared_diff_min_blocks 5


#define new_part1_Max_threads 352
#define new_part1_min_blocks 5

//#define max_contacts
//#define long_time_check
// #define WITH_TIMING
//#define With_Events
// #define With_steps



#define LXd (double)(X2-X1)
#define LYd (double)(Z2-Z1)
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	GLOBAL VARIABLES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
int steperror=100;

// whether to run with cuda or not
int cudarun =1;
// Time t
double t;
// Dimensions of the box
double LX=X2-X1;
double LY=Z2-Z1;

// Working directory
char directory[150];

// The lists of bubbles
// CAREFUL: first bubble is j=1
sphere b1[NGRAINS+NFIBERS+NWALL+1],b2[1+NGRAINS+NFIBERS+NWALL],b0[1+NGRAINS+NFIBERS+NWALL];
// The lists of beams
beam b1[(int)(2*NFIBERS+1)];//,b2[NBEAMS],b0[NBEAMS];

// Total force on a bubble
force forces[NGRAINS+NFIBERS+NWALL+1];
double torque[NGRAINS+NFIBERS+NWALL+1];
// Total viscous force on a bubble
// force forcesv[NGRAINS+NWALL+1];

int HoL[(int)NX+1][(int)NY+1];
int AL[1+NGRAINS+NFIBERS+NWALL],PL[1+NGRAINS+NFIBERS+NWALL];

// Total forces on the walls: transverse (upper wall), normal (bottom wall), tangential (viscous part)
double upforce=0.,doforce=0.,xforce=0.,xforcee=0.;

// Liquid fraction and total volume of bubbles
double liquid_fraction=0.;
double bubble_volume=0.;

//  various paramaters used in the programme
double dteff=dt;
double host_U1=U0;								// Speed of wall compression
double tstart;
int iter=2;
double cste_stokes;
double overlap=0.;							// overlaps between two bubbles
double overlap1=0.;
double overlap0=0.;
int i_cell,j_cell,k_cell,iv,jv,kv;
int switchy=0,switchy0;						// switch from moving from compression to shearing
long int NBEAMS=1;

__device__ __constant__ int const_upper_bound=NY-1;		// Redefining the number of cells in the Z in the device constant memory. Is updated after compression is finished.
// is used to decrease the amount of work done, we dont have to search for contacts in cells above this boundary.

// All routines and functions on the CPU
void initialisation();
void evolution();
force gravite(sphere s);
force plate_friction(sphere);
force bounce(sphere, sphere,int);
void collision(int,int,sphere, sphere,double,double);
double norm(vector);
double angle(vector);
double distance(sphere, sphere);
double distanceF(double,double,double,double);
void list_update(int);
void euler(int);
void verlet(int);
double mom(vector,force);
vector vs(sphere sA,sphere sB);
int CPI(int);
double XCP(double);
int CPJ(int);
double YCP(double);
void contacts(int,int);

int print_step3=print_step;



// Kernels used on the GPU, and functions used relating to GPU programme

void device_run(int L, int M, int N, sphere* host_s0, sphere* host_s1, sphere* host_s2, beam* host_b1, force* host_forces,double* host_torque, int* host_AL, int* host_PL, int* host_HoL,double LX, double LY,int NBEAMS,double host_U1);					// Run the code on the GPU instead of the CPU, ititialise all necessary variables, and copy memory over to GPU

int device_evolutionCPU(int L, int M, int N, sphere* device_s0, sphere* device_s1, sphere* device_s2, beam* device_b1, force* forces,double* torque, int* AL, int* PL, int* HoL,double*
    device_bubble_volume, double* device_liquid_fraction, double LX, double LY, int* device_sw, sphere* host_s0, sphere* host_s1,sphere* host_s2, beam* host_b1,force* host_forces,double*
    host_torque, cell_up* cell_list, int* bubble_move_list, int* device_mutex, double* host_doforce, double* host_upforce, double* host_xforce,  double* device_doforce, double*
    device_upforce, double* device_xforce,int* host_AL, int* host_PL, contact_list* host_contact_list,contact_list* device_contact_list,double* device_s0_pos_x,double*
    device_s0_pos_y,double* device_s0_vel_x,double* device_s0_vel_y,double* device_s0_radius, double* device_s0_mass, double* device_s0_the_z,double* device_s0_om_z,double*
    device_s0_moment, int* device_s0_cell_i,int* device_s0_cell_j,int* device_s0_fiber,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_vel_x,double*
    device_s1_vel_y,double* device_s1_radius, double* device_s1_mass,double* device_s1_the_z,double* device_s1_om_z,double* device_s1_moment, int* device_s1_cell_i,int*
    device_s1_cell_j,int* device_s1_fiber,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_radius, double* device_s2_mass,
    double* device_s2_the_z,double* device_s2_om_z,double* device_s2_moment,int* device_s2_cell_i,int* device_s2_cell_j,int* device_s2_fiber,unsigned int* device_b1_j1,unsigned int*
    device_b1_j2,double* device_b1_longueur, double* device_b1_tx,double* device_b1_ty,unsigned int* device_b1_actif,double* device_forces_x,double* device_forces_y,double* device_torque_z, int* device_bubble_mutex, int*
    device_max_contacts,double* device_forces_x_2,double* device_forces_y_2,double* device_torque_z_2,int* device_contact_list_count,int* host_sw, int* device_counter,int* device_nbactive,int *host_nbactive,int NBEAMS,double host_U1,double* device_U1);		// Actually run the foam model on the GPU, calls all kernels to run the model




///////////////////////////////////////////////////////////////// BEAMS KERNELS ////////////////////////////////////////////////////////////////////////////////////////////////

void  __global__ beams_update(double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_radius,int* device_s1_fiber,unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty,unsigned int*
    device_b1_actif,double*
    forces_x_2,double* forces_y_2,int NBACTIVE0,int *device_nbactive);
// Updates beam length

void  __global__ fiber_forces(double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_radius,int* device_s1_fiber,unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty,unsigned int*
    device_b1_actif,double*
    forces_x,double* forces_y,double LX);
// Computes fiber forces

void __global__ arrays_to_beams(unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty,unsigned int* device_b1_actif, beam* device_b1,int XBBeams, int *device_nbactive);
// Writes the arrays of beam structures to arrays of individual struct components

void __global__ beams_to_arrays(unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty,unsigned int* device_b1_actif, beam* device_b1,int XBBeams, int NBEAMS);
// writes arrays of beam struct components back to arrays of structs


//////////////////////////////////////////////////////////////// CONTACTS KERNELS, split into two or three kernels ////////////////////////////////////////////////////////////////

void  __global__ contacts_part1_ILP(int* HoL,int* PL, double* device_s1_pos_x,double* device_s1_pos_y,  double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_radius, int* device_s1_cell_i, int* device_s1_cell_j,int* device_s1_fiber, int i, int *err, contact_list* device_contact_list, int* device_bubble_mutex,int* device_contact_list_count);				// Contacts kernel Part 1, runs one thread per bubble, and extra ILP checks 3 cells simultaneously in the same loop body. Part one checks for all possible contacts and if contact exists between bubbles, it adds it to the contact list

void  __global__ contacts_part1_TLP( int* HoL,int* PL, double* device_s1_pos_x,double* device_s1_pos_y,  double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_radius, int* device_s1_cell_i, int* device_s1_cell_j,int* device_s1_fiber, int i, int *err, contact_list* device_contact_list, int* device_bubble_mutex,int* device_contact_list_count); 									// Contacts kernel Part 1. Runs on large grid, 9 threads per bubble, each thread checking a single neighbouring cell. Checks for contacts and adds to contact lists.

void  __global__ contacts_part2_ILP( double* device_s1_pos_x,double* device_s1_pos_y, double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_om_z,double* device_s1_radius, double* forces_x,double* forces_y,  double* torque_z, contact_list* device_contact_list,double* forces_x_2,double* forces_y_2,  double* torque_z_2,int* device_contact_list_count,double LX);																											// Contacts kernel Part 2. Runs one thread per bubble. Each thread computes all the contacts on the contact list per bubble. Added ILP means two contacts are computed in each collision function.

void __global__ contacts_part2_TLP(double* device_s1_pos_x,double* device_s1_pos_y, double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_radius, double* forces_x,double* forces_y,  double* torque_z, contact_list* device_contact_list, int* device_contact_list_count);							// Contacts part 2 kernel. Runs 9 threads per bubble. Each thread computes one of 9 possible contacts per bubble. Lots of inactive threads, so only use on system sizes up to about 4500.

void __global__ contacts_part3_ILP(double* forces_x,double* forces_y,double* torque_z,double* forces_x_2,double* forces_y_2,double* torque_z_2);             // Contacts part 3 kernel. Used in conjunction with contacts part 2 ...... . kernel. Adds up forces written with atomic adds, and those written without atomics.

void __device__ device_collision_no0_array_streams_no_vectors_no_Forces_no_structs_half_pared (double sA_pos_x, double sA_pos_y, double sA_vel_x, double sA_vel_y,double sA_radius,double sB_pos_x, double sB_pos_y, double sB_vel_x,double sB_vel_y,double sB_radius, double delta , double* forcesa_x,double* forcesa_y,double* torquea_z,double* forcesb_x,double* forcesb_y,   double* torqueb_z); 			  // Collision function used in Contacts kernel part 2 ..... . It computes collision forces between bubbles and writes them to force files

void __device__ device_collision1(int a, int b,double sA_pos_x, double sA_pos_y, double* sA_vel_x, double* sA_vel_y, double* sA_om_z,double sA_radius,double sB_pos_x, double sB_pos_y,double sB_radius, double* forcesa_x,double* forcesa_y, double* torquea_z,double* forces_x_2,double* forces_y_2,double* torque_z_2,double device_s1_pos_wall);									  // Collision function used in Contacts kernel part 2 .... . It computes collision forces and writesto two force arrays, one with Atomic Addds, and one without. Contacts part 3 then adds them together

void __device__ device_collision2 (int a, int b, int c,double sA_pos_x, double sA_pos_y,  double* sA_vel_x, double* sA_vel_y, double* sA_om_z,double sA_radius,double sB_pos_x, double sB_pos_y, double sB_om_z, double sB_radius,double sC_pos_x,double sC_pos_y, double sC_om_z,double sC_radius, double* forcesa_x,double* forcesa_y,  double* torquea_z,double* forces_x_2,double* forces_y_2,double* torque_z_2,double device_s1_pos_wall);       							// Collision function used in Contacts kernel part 2 .... .It operates on two collisions simultaneously, (increasing the ILP). It computes collision forces and writes to two force arrays, one with Atomic Addds, and one without. Contacts part 3 then adds them together

void __global__ fibertension( double* device_s1_pos_x,double* device_s1_pos_y, double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_om_z,double* device_s1_radius,int* device_s1_fiber, double* forces_x_2,double* forces_y_2,  double* torque_z_2, contact_list* device_contact_list,int* device_contact_list_count);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////// Kernels for rest of evolution ///////////////////////////////////////////////////////////////////////////////////


void __global__ verletfunc(double* device_s0_pos_x,double* device_s0_pos_y,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_vel_x,double* device_s1_vel_y,double* device_s1_om_z,double* device_s1_radius, double* device_s1_mass, double* device_s1_moment,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_om_z,double* forces_x,double* forces_y,double* torque_z,double LX, double LY, double* liquid_fraction, double* bubble_volume,int Xblocksi, int i); 					 // run a verlet iteration on the GPU

void __global__ move_walls(double* device_s1_pos_x,double* device_s1_pos_y,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_vel_x,double* device_s2_vel_y, int* sw,double* liquid_fraction,double LX,int Xblocks, int i,double* device_U1);										// Move the wall bubbles, either compressing or shearing.

void __global__ parallel_list_update(int Xblocks,int dimListUpdate,int* move_list,int*  s1_cell_i,int* s1_cell_j, double* s2_pos_x,double* s2_pos_y, int* s2_cell_i, int* s2_cell_j, cell_up* cell_list,  int* device_mutex, double LX, double LY, int i);														// First kerenl in list update step. Checks if a bubble is moving, if so writes it to cell moving list
void __global__ SEAN_parallel_list_update(int Xblocks,int* move_list,int*  s1_cell_i,int* s1_cell_j, double* s2_pos_x,double* s2_pos_y,  int* s2_cell_i, int* s2_cell_j,cell_up* cell_list,  int* device_mutex, double LX, double LY, int i);														// First kerenl in list update step. Checks if a bubble is moving, if so writes it to cell moving list


void __global__ serialupdate(int Xblocks,int*  s1_cell_i,int* s1_cell_j, double* s2_pos_x,double* s2_pos_y,  int* s2_cell_i, int* s2_cell_j,  double LX, double LY, int i, int* AL, int* PL, int* HoL);

void __global__ cell_lists_out_update(int i,cell_up* cell_list,int* AL, int* PL, int* HoL,int Xblocks);
// Second kernel in list update step. Update the cells bubbles are moving out of.

void __global__ cell_lists_in_update(int i,cell_up* cell_list,int* AL, int* PL, int* HoL, int Xblocks);
// Third kerenl in list update step. Updates the cells bubbles are moving into.
void __device__ check_bubble_move(int gid, int* move_list,cell_up* cell_list, int* s1_cell_i,int* s1_cell_j, int* s2_cell_i,int* s2_cell_j,  int* mutex);

void __global__ wall_forces_final_2(double* forces_x,double* forces_y,double* torque_z,double* doforce, double* upforce, double* xforce, int Xblocks, int i);
// Computes the stress and strain forces on the walls

//////////////////////////////////////////  Functions and kernels that are needed called in programmes evolution //////////////////////////////////

void __global__ bubble_volume_update(double* bubble_volume, double* a_radius );		// update bubble volume on the GPU, only needs to be called once

double __device__ device_distance_array_abs_i_pared(double sA_pos_x,double sA_pos_y,double sB_pos_x,double sB_pos_y);	// computes distance between 2 bubbles

double __device__ device_norm(vector v);											// computes the norm of a vector

int __device__ device_CPI (int i);													// Periodic boundary condition for cell x number

double __device__ device_XCP(double x, double LX);									// Peridic boundary condition for bubble x position

int __device__ device_CPJ(int j) ;													// Periodic boundary condition for cell y number

double __device__ device_YCP(double y, double LY); 									// Peridic boundary condition for bubble y position


vector __device__ device_normal_vector_array_pared(double sA_pos_x,double sA_pos_y,double sB_pos_x,double sB_pos_y);	// computes normal vector for distance between two bubbles

void __global__ reset_forces(double* forces_x,double* forces_y,double* torque_z,  int Xblocks);
// function to reset force arrays to zero

void __global__ update_2bubbles_reset_forces(double* s0_pos_x,double* s0_pos_y,double* s0_the_z,double* s0_vel_x,double* s0_vel_y,double* b0_om_z,double* b0_radius, double* b0_mass,double* b0_moment, int* b0_cell_i,int* s0_cell_j,int* device_s0_fiber,double* s1_pos_x,double* s1_pos_y,double* b1_the_z,double* b1_vel_x,double* s1_vel_y,double* s1_om_z,double* s1_radius, double* s1_mass, double* s1_moment,int* s1_cell_i,int* s1_cell_j,int* device_s1_fiber,double* s2_pos_x,double* s2_pos_y,double* b2_the_z,double* b2_vel_x,double* b2_vel_y,double* b2_om_z,double* s2_radius, double* s2_mass,double* b2_moment, int* s2_cell_i,int* s2_cell_j,int* device_s2_fiber,double* forcesx, double* forcesy, double* torquez, int start, int end,int Xblocks,int* device_nbactive);
// Resets the force arrays and updates bubble arrays

void __global__ reset_wall_forces(double* doforce, double* upforce, double* xforce, int Xblocks);
// resets the wall force values

__device__ double double_atomicAdd(double* address, double val);
// Adds value to address in an atomic fashion ( no race conditions );

__device__ int int_atomicAdd(int* address, int val);
// // Adds value to address in an atomic fashion ( no race conditions );

void __global__ zero_cells_old(cell_up* cell_list, int Xblocks);
// zeros cell moving lists

void __global__ zero_contact_lists(int Xblocks, contact_list* device_contact_list);
// Zeros the contact lists

void __global__ zero_bubble_volume(double* device_bubble_volume);
// Zeros device bubble volume


void __global__ arrays_to_bubbles(double* device_s0_pos_x,double* device_s0_pos_y,double* device_s0_the_z,double* device_s0_vel_x,double* device_s0_vel_y,double* device_s0_om_z,double* device_s0_radius, double* device_s0_mass,double* device_s0_moment, int* device_s0_cell_i,int* device_s0_cell_j,int* device_s0_fiber,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_the_z,double* device_s1_vel_x,double* device_s1_vel_y,double* device_s1_om_z,double* device_s1_radius, double* device_s1_mass, double* device_s1_moment,int* device_s1_cell_i,int* device_s1_cell_j,int* device_s1_fiber,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_the_z,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_om_z,double* device_s2_radius, double* device_s2_mass,double* device_s2_moment, int* device_s2_cell_i,int* device_s2_cell_j,int* device_s2_fiber,sphere* device_s0, sphere* device_s1, sphere* device_s2,int Xblocks);
// Writes the arrays of sphere structures to arrays of individual struct components

void __global__ bubbles_to_arrays(double* device_s0_pos_x,double* device_s0_pos_y,double* device_s0_the_z,double* device_s0_vel_x,double* device_s0_vel_y,double* device_s0_om_z,double* device_s0_radius, double* device_s0_mass,double* device_s0_moment, int* device_s0_cell_i,int* device_s0_cell_j,int* device_s0_fiber,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_the_z,double* device_s1_vel_x,double* device_s1_vel_y,double* device_s1_om_z,double* device_s1_radius, double* device_s1_mass, double* device_s1_moment,int* device_s1_cell_i,int* device_s1_cell_j,int* device_s1_fiber,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_the_z,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_om_z,double* device_s2_radius, double* device_s2_mass,double* device_s2_moment, int* device_s2_cell_i,int* device_s2_cell_j,int* device_s2_fiber,sphere* device_s0, sphere* device_s1, sphere* device_s2,int Xblocks);
// writes arrays of sphere struct components back to arrays of structs

void __global__ forces_to_arrays(force* device_forces, double* device_torque, double* device_forces_x,double* device_forces_y,double* device_torque_z);
// Writes the arrays of force structures to arrays of individual struct components

void __global__ arrays_to_forces(force* device_forces, double* device_torque, double* device_forces_x,double* device_forces_y,double* device_torque_z);
// writes arrays of force struct components back to arrays of structs


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////// KERNELS AND FUNCTIONS TO WRITE INFORMATION TO FILES AND SCREEN //////////////////////////////////////////////////////

void __global__ print_liquid_fraction(double* device_liquid_fraction, int* device_sw,double* device_bubble_volume, double* s1_pos_y,int Xblocks, double t, double u0);
// print Liquid fraction, switch state, bubble volume, container volume, and iteration number to screen.

void write_out(sphere* host_s0, sphere* host_s1, sphere* host_s2, beam* host_b1, force* host_forces,double* host_torque,sphere* device_s0, sphere* device_s1, sphere* device_s2, beam* device_b1, force* device_forces,double* device_torque, double* host_doforcept,double* host_upforcept,double* host_xforcept,double* device_doforce,double* device_upforce,double* device_xforce, int* host_AL, int* host_PL, int* device_AL, int* device_PL,contact_list* host_contact_list, contact_list* device_contact_list,int* device_sw, int* host_sw,int *device_nbactive,int *host_nbactive);
// Write out Sphere, force and list info to files


void write_wall_forces_to_file(char* wfname,FILE* wallfile,double* host_doforce,double* host_upforce,double* host_xforce,sphere* host_s1,int i);
// write wall stress and strain forces to files


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////// Kernels for measuring system properties /////////////////////////////////////////////////////////

void __global__ device_max_contacts_func(contact_list* device_contact_list, int* device_max_contacts);
// kernel to find the maximum, and average number of contacts per bubble

void __global__ device_max_contacts_max( int* device_max_contacts);
// finds the maximum number of contacts

void __global__ device_max_contacts_zero( int* device_max_contacts);
// zeros the maximum number of contacts

void __global__ cell_counter(int* HoL, int* PL, int* counter);
// finds the how many cells contain a specfic number of bubbles ( 0-9 possible values for number of bubbles in a cell)

void __global__ cell_counter_print(int* counter);
// print cell occupancy numbers to screen

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //	//	//	//
//																																									//
//																MAIN ROUTINE																						//
//																																									//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //	//	//	//


// Takes as an argument the number of the directory in which results will get stored
int main(int argc, char ** argv)
{

  struct cudaDeviceProp x;
  cudaGetDeviceProperties(&x,0);
  printf("Choosing %s \n",x.name);
  cudaSetDevice(atoi(argv[2]));
  char chaine[150];

  printf("Let's go !\n");
  printf(" Iterations are %d\n",NITER);

  // We create the necessary directories
  sprintf(directory,"./rfc2d_%s",argv[1]);
  sprintf(chaine,"mkdir ./rfc2d_%s",argv[1]);

  printf("We'll be working in the directory%s \n",directory);
  system(chaine);

  sprintf(chaine,"cp ./fcouette2D.h %s",directory);
  system(chaine);

  sprintf(chaine,"cp ./Rfibers2D.cu %s",directory);
  system(chaine);

  liquid_fraction=0.;
  bubble_volume=0.;
  cste_stokes=Facteur*VISCOSITY;

  // We start by building the initial state of the foam
  initialisation();

  // Then we print a few useful numbers before starting
  printf("We have %d bubbles in the bulk and %d on the edges, of mass %.12f kg and moment of inertia %.12f) \n",NGRAINS, NWALL,b0[1].mass,b0[1].moment);
  printf("Les collisions dureront aux alentours de  %e  secondes \n",TCOLL);
  printf("Timestep %e \n",dt);
  printf("Le coefficient de restitution est de %f \n",exp(-TCOLL*GAMMA_G/(2*M_G)));


  printf("La deformation est %e \n",M_G*GRAVITY/(K_G*R_G));
  printf("There are %d x %d cells in the box \n",NX,NY);
  // double TCOLL=PI*sqrt(M_G/K_G);

  // printf("The time of viscous relaxation is %e \n",tau_d);
  // printf("The inertial time is  %e \n",tau_v);
  printf("The desired liquid fraction is %lf \n",phi0);
  // printf("The elastic timescale is  %e   \n",TCOLL);
  //
  printf("The shear timescale %e \n",HFINAL/U0);
  //
  printf("The Deborah number is %e \n",DE);

  // printf("The typical overlap is %e \n",U0/b0[NGRAINS+NWALL].position.z*R_G*friction_coeff/K_G);

  //printf("The dimGridParallel is %d\n",dimGridParallel);

  printf("Let's go, then \n \n \n");

  // Now we let the foam flow
  evolution();

  printf("\n I love it when a plan comes together \n");

  return 0;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	BUILDING THE INITIAL STATE
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void initialisation(){
  char namefile5[150];
  FILE *fp0;
  float ran3(long *idum);
  long graine=150;
  float radius,y,x,vx,vy,drop;
  int i,j,k;
  double y0=Z1+10.5*R_G;
  double DD=((0.09-y0)/(2*R_G))/(NF+4);
  double DY=R_G*(2.5+poly);

  // HERE j is the index of a bubble
  // If we start from scratch, we put the bubbles on a rectangular grid, with some noise
  if(init==-1) {


    // Now let's create horizontal fibers
    for(j=NGRAINS+1;j<=NGRAINS+NFIBERS;j++){
      i=(int)((j-NGRAINS)/LFIBERS)+1;
      b0[j].velocity.x=0.;
      b0[j].velocity.z=0.;
      b0[j].radius=R_F;
      b0[j].fiber=(int)((j-NGRAINS-1)/LFIBERS)+1;
      if((j-NGRAINS-1)%LFIBERS==0){
        /*if(i%2==1)*/  b0[j].position.x=XCP(X1+(X2-X1)*ran3(&graine));
        // else b0[j].position.x=XCP(b0[j-1].position.x+8*R_B);
        b0[j].position.z=y0+(i+2)*DD*DY;//DD+delta/2.+(i-1.)*(DD+delta)+1.5*R_F*ran3(&graine);
      }
      else	{
        b0[j].position.x=XCP(b0[j-1].position.x+1.8*R_F);
        b0[j].position.z=b0[j-1].position.z+R_F*0.1*(ran3(&graine)-0.5);
      }
      b0[j].mass=M_F;
      bubble_volume=bubble_volume+PI*R_F*R_F;
    }

    // starting by putting the first bubble
    b0[1].velocity.x=0.005*(ran3(&graine)-0.5);
    b0[1].velocity.z=0.005*(ran3(&graine)-0.5);
    b0[1].radius=R_G*(1.+poly*2.*(ran3(&graine)-0.5));
    b0[1].mass=pow(b0[1].radius,3)*4./3.*PI*DENSITY;
    // 		b0[1].moment=0.4*b0[1].mass*pow(b0[1].radius,2);
    b0[1].position.x=X1+2.5*b0[1].radius;
    // 	b0[1].position.z=Z1+LY/5.+b0[1].radius;
    b0[1].position.z=y0;
    // 	b0[1].theta=0.;
    // 	b0[1].omega=0.;
    b0[1].fiber=0;
    bubble_volume=bubble_volume+PI*pow(b0[1].radius,2.);

    // then the other bubbles one by one
    for(j=2;j<=NGRAINS;j++){

      // We pack them by x-layers
      b0[j].velocity.x=0.005*(ran3(&graine)-0.5);
      b0[j].velocity.z=0.002*(ran3(&graine)-0.5);
      b0[j].radius=R_G*(1.+poly*2.*(ran3(&graine)-0.5));

      b0[j].position.x=b0[j-1].position.x+b0[j-1].radius+b0[j].radius*1.5;
      b0[j].position.z=b0[j-1].position.z;
      //		b0[j].position.z=b0[j-1].position.z;
      // 	b0[1].theta=0.;
      // 	b0[1].omega=0.;
      b0[j].fiber=0;


      // Go to the next line in y
      if(b0[j].position.x>X2-R_G*2.5){
        b0[j].position.x=X1+R_G+b0[j].radius*(1+ran3(&graine));
        b0[j].position.z=b0[j].position.z+DY;
      }
      for(i=0;i<NF;i++){
        for(k=1;k<=LFIBERS;k++){
          if(distance(b0[j],b0[NGRAINS+i*LFIBERS+k])<b0[j].radius+R_F*1.8){
            printf("oui voila %d %d %d %f %f \n",j,NGRAINS+i*LFIBERS+k,i,b0[j].position.x,b0[NGRAINS+i*LFIBERS+k].position.x);
            b0[j].position.x=b0[j].position.x+LFIBERS*R_F*1.8+3*R_G;
          }
          if(b0[j].position.x>X2-R_G*2.5){
            b0[j].position.x=X1+R_G+b0[j].radius*(1+ran3(&graine));
            b0[j].position.z=b0[j].position.z+DY;
          }

        }}

      //	if(b0[j].position.x>X2-R_G*2.5){//
      //	b0[j].position.x=X1+R_G+b0[j].radius*(1+ran3(&graine));
      //	b0[j].position.z=b0[j].position.z+DY;
      //	}


      b0[j].mass=pow(b0[j].radius,3)*PIC*DENSITY;
      // 			b0[j].moment=0.4*b0[j].mass*pow(b0[j].radius,2);
      bubble_volume=bubble_volume+PI*pow(b0[1].radius,2.);
    }


    // Now we stack bubbles on the walls, symmetrically
    // int nbott=(int)((X2-X1)/(2*R_G));
    for(j=NGRAINS+NFIBERS+1;j<=NGRAINS+NFIBERS+NWALL/2;j++){
      //   fscanf(fp0,"%f %f %f  \n",&x,&y,&radius);
      if(j==NGRAINS+NFIBERS+1){
        b0[j].position.x=X1;
        b0[j+NWALL/2].position.x=X1;
        b0[j].position.z=Z1;
        b0[j+NWALL/2].position.z=b0[NGRAINS].position.z+5*R_G;
        b0[j].radius=R_G;
        b0[j+NWALL/2].radius=R_G;
      }
      else
      {
        if(j==NGRAINS+NFIBERS+NWALL/2) {
          b0[j].radius=((X2-R_G)-(b0[j-1].position.x+b0[j-1].radius))/2.;
          b0[j+NWALL/2].radius=((X2-R_G)-(b0[j-1+NWALL/2].position.x+b0[j-1+NWALL/2].radius))/2.;
        }
        else{
          b0[j].radius=R_G*(1.+poly*2.*(ran3(&graine)-0.5));
          b0[j+NWALL/2].radius=R_G*(1.+poly*2.*(ran3(&graine)-0.5));
        }
        b0[j].position.x=b0[j-1].position.x+b0[j-1].radius+b0[j].radius;
        b0[j].position.z=b0[j-1].position.z;
        b0[j+NWALL/2].position.x=b0[j+NWALL/2-1].position.x+b0[j-1].radius+b0[j].radius;
        b0[j+NWALL/2].position.z=b0[j+NWALL/2-1].position.z;
      }

      b0[j].velocity.x=0.;
      b0[j].velocity.z=0.;
      b0[j].mass=pow(b0[j].radius,3)*4./3.*PI*DENSITY;
      b0[j+NWALL/2].velocity.x=0.;
      b0[j+NWALL/2].velocity.z=0.;
      b0[j+NWALL/2].mass=pow(b0[j+NWALL/2].radius,3)*4./3.*PI*DENSITY;
      bubble_volume=bubble_volume+PI*pow(b0[1].radius,2.);
    }
    // int nwalls=(int)((Z2-Z1)/(2*R_G));
    // for(j=NGRAINS+nbott+1;j<=NGRAINS+nbott+nwalls;j++){
    // //   fscanf(fp0,"%f %f %f  \n",&x,&y,&radius);
    // if(j==NGRAINS+nbott+1){
    //   b0[j].position.z=Z1;
    //   b0[j+nwalls].position.z=Z1;
    // }
    // else
    // {
    //
    //   b0[j].position.z=b0[j-1].position.z+2*R_G;
    //     b0[j+nwalls].position.z=b0[j+nwalls-1].position.z+2*R_G;
    // }
    //   b0[j].position.x=X1;
    //     b0[j+nwalls].position.x=X2;
    //   b0[j].radius=R_G;
    // 	b0[j+nwalls].velocity.x=0.;
    // 	b0[j+nwalls].velocity.z=0.;
    // 	b0[j+nwalls].mass=pow(b0[j+nwalls].radius,3)*4./3.*PI*DENSITY;
    // 	  b0[j+nwalls].radius=R_G;
    // 	b0[j+nwalls].velocity.x=0.;
    // 	b0[j+nwalls].velocity.z=0.;
    // 	b0[j+nwalls].mass=pow(b0[+nwalls].radius,3)*4./3.*PI*DENSITY;
    // 	bubble_volume=bubble_volume+PI*4./3.*pow(R_G,3.);
    // }

    // We initialize the beams
    for(j=1;j<=NBEAMS;j++){
      b1[j].actif=0;
    }

  }

  // Otherwise, we start by reading a position file, and we take the initial positions from there
  //
  else{

    sprintf(namefile5,"./ii_F0.dat",dossier);
    printf("IM READING THE FILE %s \n",namefile5);
    fp0=fopen(namefile5,"r");

    for(j=1;j<=NGRAINS+NFIBERS+NWALL;j++){

      fscanf(fp0,"%f \t %f \t %f \t %f \t %f %f %f\n",&x,&y,&vx,&vy,&radius,&drop,&drop);

      b0[j].position.x=(double)(x);
      b0[j].position.z=(double)(y);
      b0[j].velocity.x=0.;//(double)(vx);
      b0[j].velocity.z=0.;//(double)(vy);
      // 		b0[j].omega=0.;
      b0[j].radius=(double)(radius);
      b0[j].mass=pow(b0[j].radius,2)*PI*DENSITY;
      // 		b0[j].moment=0.4*b0[j].mass*pow(b0[j].radius,2);
      if(j<=NGRAINS||j>NGRAINS+NFIBERS) b0[j].fiber=0;
      if(j>NGRAINS&&j<=NGRAINS+NFIBERS) b0[j].fiber=(int)((j-NGRAINS-1)/LFIBERS)+1;

    }
    for(j=1;j<=NBEAMS;j++){
      b1[j].actif=0;
    }

    fclose(fp0);
    // 	sprintf(namefile5,"./walls.dat");
    // 	printf("IM PRINTING THE FILE %s \n",namefile5);
    // 	fp0=fopen(namefile5,"w");
    // // Now we stack bubbles on the walls, symmetrically
    // int nbott=(int)((X2-X1)/(2*R_G));
    // for(j=NGRAINS+1;j<=NGRAINS+nbott;j++){
    // //   fscanf(fp0,"%f %f %f  \n",&x,&y,&radius);
    // if(j==NGRAINS+1){
    //   b0[j].position.x=X1;
    // //   b0[j+NWALL/2].position.x=X1;
    //   b0[j].position.z=Z1;
    // //   b0[j+NWALL/2].position.z=Z1;
    // }
    // else
    // {
    //   b0[j].position.x=b0[j-1].position.x+2*R_G;
    //   b0[j].position.z=b0[j-1].position.z;
    // }
    //   b0[j].radius=R_G;
    // 	b0[j].velocity.x=0.;
    // 	b0[j].velocity.z=0.;
    // 	b0[j].mass=pow(b0[j].radius,3)*4./3.*PI*DENSITY;
    // 	bubble_volume=bubble_volume+PI*4./3.*pow(b0[1].radius,3.);
    // }
    // int nwalls=(int)((Z2-Z1)/(2*R_G));
    // for(j=NGRAINS+nbott+1;j<=NGRAINS+nbott+nwalls;j++){
    // //   fscanf(fp0,"%f %f %f  \n",&x,&y,&radius);
    // if(j==NGRAINS+nbott+1){
    //   b0[j].position.z=Z1;
    //   b0[j+nwalls].position.z=Z1;
    // }
    // else
    // {
    //
    //   b0[j].position.z=b0[j-1].position.z+2*R_G;
    //     b0[j+nwalls].position.z=b0[j+nwalls-1].position.z+2*R_G;
    // }
    //   b0[j].position.x=X1;
    //     b0[j+nwalls].position.x=X2;
    //   b0[j].radius=R_G;
    // 	b0[j+nwalls].velocity.x=0.;
    // 	b0[j+nwalls].velocity.z=0.;
    // 	b0[j+nwalls].mass=pow(b0[j+nwalls].radius,3)*4./3.*PI*DENSITY;
    // 	  b0[j+nwalls].radius=R_G;
    // 	b0[j+nwalls].velocity.x=0.;
    // 	b0[j+nwalls].velocity.z=0.;
    // 	b0[j+nwalls].mass=pow(b0[+nwalls].radius,3)*4./3.*PI*DENSITY;
    // 	bubble_volume=bubble_volume+PI*4./3.*pow(R_G,3.);
    // }


    // fclose(fp0);
    // 	sprintf(namefile5,"./wallii%.3d.dat",dossier);
    // 	printf("IM READING THE FILE %s \n",namefile5);
    // 	fp0=fopen(namefile5,"r");
    // 	int nwalls=(int)((Z2-Z1)/(2*R_G));
    // 	int nbott=(int)((X2-X1)/(2*R_G));
    // 	for(j=NGRAINS+1;j<=NGRAINS+NWALL;j++){
    //
    // 		fscanf(fp0,"%f \t %f  \t %f  \n",&x,&y,&radius);
    // 	if(j>NGRAINS+nwalls+nbott) {
    // b0[j].position.x=X2;
    // //printf("youps %d  \t %f\n",j,b0[j].position.x);
    // }
    // 	  else b0[j].position.x=(double)(x);
    // 		b0[j].position.z=(double)(y);
    // 		b0[j].velocity.x=0.;
    // 		b0[j].velocity.z=0.;
    // 		b0[j].omega=0.;
    // 		b0[j].radius=(double)(radius);
    // 	b0[j].mass=pow(b0[j].radius,3)*4./3.*PI*DENSITY;
    // 		b0[j].moment=0.4*b0[j].mass*pow(b0[j].radius,2);
    //
    // 	}
    // 	fclose(fp0);

    // 	for(j=1;j<=NBEAMS;j++){
    // 	  b1[j].actif=0;
    // 	}



  }




  printf("The bubble assembly has been successfully built !\n");
}


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	EVOLUTION ROUTINE
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void evolution() {
  char namefile3[150],namefile0[150];
  char namefileb0[150],namefileb2[150];
  FILE* fpb0;
  FILE* fpb2;
  FILE *fp5,*fp3=0;
  int init2,ic,jc,kc,i,j,k;
  float ran3(long *idum);
  long graine=1;
  // section to create proper HoL to send over to device
  int HoL2[(NX+1)*(NY+1)];
  int percision=25;
  // Print out initial positions-----------------------------------------//
  sprintf(namefiles0,"%s/positions000000.dat",directory);
  //          sprintf(namefile3,"%s/ppositions0-000000.dat",directory);
  //          sprintf(namefileb2,"%s/ppositions2-000000.dat",directory);
  //          fp3=fopen(namefile3,"w");
  fpb0=fopen(namefiles0,"w");
  //          fpb2=fopen(namefileb2,"w");

  for(j=1;j<=NGRAINS+NFIBERS+NWALL;j++){
    //          fprintf(fp3,"%.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \n",percision,b1[j].position.x,percision,b1[j].position.z,percision,b1[j].position.z,percision,b1[j].velocity.x,percision,b1[j].velocity.z,percision,b1[j].velocity.z,percision,b1[j].radius,percision,forces[j].x,percision,forcesv[j].x,percision,forces[j].z,percision,forcesv[j].z,percision,forces[j].z,percision,forcesv[j].z);
    fprintf(fpb0,"%.16f \t %.16f \t%.16f \t%.16f \t%.16f 0. \t 0. \n",b0[j].position.x,b0[j].position.z,b0[j].velocity.x,b0[j].velocity.z,b0[j].radius);
    //          fprintf(fpb2,"%.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \t %.*e \n",percision,b2[j].position.x,percision,b2[j].position.z,percision,b2[j].position.z,percision,b2[j].velocity.x,percision,b2[j].velocity.z,percision,b2[j].velocity.z,percision,b2[j].radius,percision,forces[j].x,percision,forcesv[j].x,percision,forces[j].z,percision,forcesv[j].z,percision,forces[j].z,percision,forcesv[j].z);
  }

  //  fclose(fp3);
  fclose(fpb0);
  //  fclose(fpb2);
  //----------------------------------------------------------------------//

  // We initialize all bubbles
  for(j=1;j<=NGRAINS+NFIBERS+NWALL;j++){
    // b0 is the bubble at timestep  iter-1
    // b1                                       iter
    // b2                                       iter+1

    b1[j]=b0[j];
    b2[j]=b0[j];

    PL[j]=0;
    AL[j]=0;
    forces[j].x=0.;
    forces[j].z=0.;
    torque[j]=0.;
    /// need to uncomment, but for the moment do no printing
    /*
    // and print the first position file
    fprintf(fp2,"%.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.6f 0. 0. 0. 0. \n",b1[j].position.x,b1[j].position.z,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].velocity.z,b1[j].radius);
     */
  }

  // The contact lists are set to zero
  for(ic=0;ic<=NX-1;ic++){
    for(jc=0;jc<=NY-1;jc++){
      HoL[ic][jc]=0;
    }}
  PL[0]=0;
  AL[0]=0;

  printf("Contact list is ready to be built !\n");

  //if(init==-1) fclose(fp2);

  printf("Please fasten your seat belt\n");

  if(init==-1) {
    init2=0;
  }
  else {
    init2=init;
  }


  // The first iteration has to be integrated with an Euler method
  iter=init2*print_step+1;
  // what's the time ?
  t=iter*dt;
  // For each bubble, we compute the cell number, and adjust the neighbourhood
  for(j=1;j<=NGRAINS+NFIBERS+NWALL;j++){

    b1[j].cell.i=CPI((int)((b1[j].position.x-X1)*NX/LX));
    b1[j].cell.j=(int)((b1[j].position.z-Z1)*NY/LY);

    PL[j]=HoL[b1[j].cell.i][b1[j].cell.j];
    AL[HoL[b1[j].cell.i][b1[j].cell.j]]=j;
    HoL[b1[j].cell.i][b1[j].cell.j]=j;
  }


  // Now for each bubble except the walls
  for(j=1;j<=NGRAINS;j++){
    b2[j]=b1[j];
    //Looking for contacts
    contacts(j,switchy);
    // and integrate new positions
    euler(j);
  }
  //	Creation of beams
  for(j=NGRAINS+1;j<NGRAINS+NFIBERS;j++){
    if(b1[j+1].fiber==b1[j].fiber){
      b1[NBEAMS].j1=j;
      b1[NBEAMS].j2=j+1;
      b1[NBEAMS].actif=1;
      b1[NBEAMS].longueur=RM;//sqrt(pow(b1[j+1].position.x-b1[j].position.x,2.)+pow(b1[j+1].position.z-b1[j].position.z,2.));
      // 		  		  printf("%ld \t %d %d \t %f \n",NBEAMS,b1[NBEAMS].j1,b1[NBEAMS].j2,b1[NBEAMS].longueur);
      b1[NBEAMS].tx=(b1[b1[NBEAMS].j2].position.x-b1[b1[NBEAMS].j1].position.x)/sqrt(pow((b1[b1[NBEAMS].j2].position.x-b1[b1[NBEAMS].j1].position.x),2.)+pow(b1[b1[NBEAMS].j2].position.z-b1[b1[NBEAMS].j1].position.z,2.));
      b1[NBEAMS].ty=(b1[b1[NBEAMS].j2].position.z-b1[b1[NBEAMS].j1].position.z)/sqrt(pow((b1[b1[NBEAMS].j2].position.x-b1[b1[NBEAMS].j1].position.x),2.)+pow(b1[b1[NBEAMS].j2].position.z-b1[b1[NBEAMS].j1].position.z,2.));
      while(b1[NBEAMS].actif==1) NBEAMS=NBEAMS+1;
    }}


  // 	         sprintf(namefiles0,"%s/beams000000.dat",directory);
  //          fpb0=fopen(namefiles0,"w");
  //  for(j=1;j<NBEAMS;j++){
  //          fprintf(fpb0,"%d \t %d \t %d \t %f \t %d \n",j,b1[j].j1,b1[j].j2,b1[j].longueur/(2*R_G),b1[j].actif);
  // 		}
  //   fclose(fpb0);


  // Now for each bubble
  for(j=1;j<=NGRAINS+NFIBERS+NWALL;j++){
    // we update the contact list
    list_update(j);
    // and replace old positions by new ones
    b0[j].position=b1[j].position;
    b0[j].velocity=b1[j].velocity;
    b1[j].position=b2[j].position;
    b1[j].velocity=b2[j].velocity;
    // all forces are reset to zero
    forces[j].x=0.;
    forces[j].z=0.;
    torque[j]=0.;
  }


  // NOW WE CAN GO FOR THE REAL THING (Verlet integration)
  printf("You signed up for %d iterations, with U0=%f\n\n\n",NITER,U0);

  if(init==-1) switchy=0;
  else switchy=1;

  // Section to do a run on cuda ///////////
  /////////////////////////////////////////
  //////////////////////////////////////////
  if(cudarun==1){

    for (i=0;i<(NX+1);i++){
      for(j=0;j<(NY+1);j++){
        HoL2[i*(NY+1)+j]=HoL[i][j];
      }
    }


    device_run(24,20,40,s0,s1,s2,b1,forces,torque,AL,PL,HoL2,LX,LY,NBEAMS,host_U1);
  }

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	IMPORTANT ROUTINES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


// Time-integration of positions, with 1st-order Euler method
void euler(int j){
  b2[j].position.x=XCP(b1[j].position.x+dt*b1[j].velocity.x);
  b2[j].velocity.x=b1[j].velocity.x+dt*(forces[j].x/b1[j].mass);
  b2[j].position.z=b1[j].position.z+dt*b1[j].velocity.z;
  b2[j].velocity.z=b1[j].velocity.z+dt*(forces[j].z/b1[j].mass);
  return;
}

// Time-integration of positions and speeds, with 4th-order Verlet method
void verlet(int j){
  double dx,dy;

  b2[j].position.x=XCP(2.*b1[j].position.x-b0[j].position.x+dt2*forces[j].x/b1[j].mass);
  b2[j].position.z=2.*b1[j].position.z-b0[j].position.z+dt2*forces[j].z/b1[j].mass;

  // Careful if we are on the sides
  dx=b2[j].position.x-b0[j].position.x;
  if(dx<-LX/2.){
    dx=dx+LX;
  }
  else{
    if(dx>LX/2.) {
      dx=dx-LX;
    }
  }
  b2[j].velocity.x=0.5*dx/dt;
  dy=b2[j].position.z-b0[j].position.z;
  // 	if(dy<-LY/2.){
  // 		 dy=dy+LY;
  // 	}
  // 	else{
  // 	if(dy>LY/2.) {
  // 		dy=dy-LY;
  // 	}
  // 	}
  b2[j].velocity.z=0.5*dy/dt;

  return;
}


// Updating the contact lists without rebuilding everything
void list_update(int j)
{

  b2[j].cell.i=CPI((int)((b2[j].position.x-X1)*NX/LX));
  b2[j].cell.j=(int)((b2[j].position.z-Z1)*NY/LY);

  // If bubble j has moved
  if(b1[j].cell.i!=b2[j].cell.i||b1[j].cell.j!=b2[j].cell.j) {

    // then we rearrange its former cell
    if(AL[j]>0){	PL[AL[j]]=PL[j];}
    else		{HoL[b1[j].cell.i][b1[j].cell.j]=PL[j];	}
    AL[PL[j]]=AL[j];

    // and then we rearrange its new cell

    AL[HoL[b2[j].cell.i][b2[j].cell.j]]=j;
    AL[j]=0;
    PL[j]=HoL[b2[j].cell.i][b2[j].cell.j];
    HoL[b2[j].cell.i][b2[j].cell.j]=j;

    // and we store the new cell
    b1[j].cell.i=b2[j].cell.i;
    b1[j].cell.j=b2[j].cell.j;
  }
  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	LOOKING FOR NEIGHBOURS
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void contacts(int j,int critere){

  int epsi,delta,zeta,lim1,lim2,lim3,lim4,j_nb;
  int condition;
  double rip,dd;
  long int ib;

  // We scan all neighbouring cells in search of a collision

  for(epsi=-1;epsi<=1;epsi++){
    for(zeta=-1;zeta<=1;zeta++){


      iv=CPI(b1[j].cell.i+epsi);
      jv=(b1[j].cell.j+zeta);


      // If the cell is not empty
      if(HoL[iv][jv]>0) {

        // Then let's see if the head of list is a colliding neighbour
        j_nb=HoL[iv][jv];

        // No need to do twice the same contact: if j_nb<j, we've seen it already
        if(j_nb>j){

          overlap1=(b1[j].radius+b1[j_nb].radius)-distance(b1[j],b1[j_nb]);
#ifdef WITH_HOUSTON
          // Just check if the overlap is not too big (remove when sure the program runs correctly)
          if(overlap1/b1[j].radius>0.75) {
            printf("HOUSTON WE HAVE A PROBLEM INIT 1  %f, %d %d %f\n",overlap1/b1[j].radius,j,j_nb,b1[j].position.z);
            exit(0);
          }
#endif
          // If overlap is small enough, we create a beam
          if(overlap1>-BRIDGE&&j_nb<=NGRAINS+NFIBERS){
            dd=distance(b0[j],b0[j_nb]);
            // 		  b1[NBEAMS].actif=1;
            // 		  b1[NBEAMS].longueur=dd;
            // 		  b1[NBEAMS].j1=j;
            // 		  b1[NBEAMS].j2=j_nb;
            // // 		  		  printf("%ld \t %d %d \t %f \n",NBEAMS,b1[NBEAMS].j1,b1[NBEAMS].j2,b1[NBEAMS].longueur);
            // 		  while(b1[NBEAMS].actif==1) NBEAMS=NBEAMS+1;

            // If overlap is positive, then we have a collision.
            if(overlap1>0.){
              // We'll need to know if it's a new or an old collision
              overlap0=(b0[j].radius+b0[j_nb].radius)-dd;
              collision(j,j_nb,b1[j],b1[j_nb],overlap1,overlap0);
            }
          }

        }


        // Once it's done with the Head of List, we can unfold the list for the chosen cell, and do the same stuff
        while (PL[j_nb]>0) {
          j_nb=PL[j_nb];
          if(j_nb>j){
            overlap1=(b1[j].radius+b1[j_nb].radius)-distance(b1[j],b1[j_nb]);

#ifdef WITH_HOUSTON
            if(overlap1/b1[j].radius>0.75) {
              printf("HOUSTON WE HAVE A PROBLEM INIT 2 %f, %d %d %f %f \n",overlap1/b1[j].radius,j,j_nb,(b1[j].radius+b1[j_nb].radius),distance(b1[j],b1[j_nb]));
              exit(0);
            }
#endif
            if(overlap1>-BRIDGE&&j_nb<=NGRAINS+NFIBERS){
              dd=distance(b0[j],b0[j_nb]);
              // 		  b1[NBEAMS].actif=1;
              // 		  b1[NBEAMS].longueur=dd;
              // 		  b1[NBEAMS].j1=j;
              // 		  b1[NBEAMS].j2=j_nb;
              // // 		  		  printf("%ld \t %d %d \t %f \n",NBEAMS,b1[NBEAMS].j1,b1[NBEAMS].j2,b1[NBEAMS].longueur);
              // 		  while(b1[NBEAMS].actif==1) NBEAMS=NBEAMS+1;
              if(overlap1>0.){
                overlap0=(b0[j].radius+b0[j_nb].radius)-dd;
                collision(j,j_nb,b1[j],b1[j_nb],overlap1,overlap0);

              }
            }}

        }

      }

    }}

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMPUTE WHAT HAPPENS DURING A COLLISION
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
void collision(int ja,int jb,sphere sA,sphere sB,double delta,double delta0){
  vector n12;
  double N;
  force repulsive_force;
  force friction;
  force dissipation={0.,0.};
  vector v_n,vr;
  vector v_t,u_t;
  force tors;

  double VRN;

  // Let's compute the normal vector (oriented sA -> sB)
  n12.x=sB.position.x-sA.position.x;
  n12.z=sB.position.z-sA.position.z;
  if(n12.x<=-LX/2.) n12.x=n12.x+LX;
  else{
    if(n12.x>=LX/2.) n12.x=n12.x-LX;
  }
  if(n12.z<=-LY/2.) n12.z=n12.z+LY;
  else{
    if(n12.z>=LY/2.) n12.z=n12.z-LY;
  }

  N=norm(n12);
  n12.x=n12.x/N;
  n12.z=n12.z/N;

  // Relative velocity of sA with respect to sB
  vr.x=sA.velocity.x-sB.velocity.x;
  vr.z=sA.velocity.z-sB.velocity.z;

  VRN=norm(vr);

  // Normal velocity of sA, with respect to sB
  double vn=vr.x*n12.x+vr.z*n12.z;
  v_n.x=vn*n12.x;
  v_n.z=vn*n12.z;


  // Elastic force
  repulsive_force.x=-K_G*2.*R_G/(sA.radius+sB.radius)*delta*n12.x;
  repulsive_force.z=-K_G*2.*R_G/(sA.radius+sB.radius)*delta*n12.z;


  // Dissipative (viscous) force
  friction.x=-friction_coeff*vr.x;
  friction.z=-friction_coeff*vr.z;

  if(sA.radius>sB.radius){
    friction.x=friction.x*sB.radius/R_G;
    friction.z=friction.z*sB.radius/R_G;
  }
  else{
    friction.x=friction.x*sA.radius/R_G;
    friction.z=friction.z*sA.radius/R_G;
  }


  forces[ja].x=forces[ja].x+repulsive_force.x+friction.x;
  forces[ja].z=forces[ja].z+repulsive_force.z+friction.z;

  forces[jb].x=forces[jb].x-repulsive_force.x-friction.x;
  forces[jb].z=forces[jb].z-repulsive_force.z-friction.z;

  // 	forcesv[ja].x=forcesv[ja].x+friction.x;
  // 	forcesv[ja].z=forcesv[ja].z+friction.z;
  // 	forcesv[ja].z=forcesv[ja].z+friction.z;
  //
  // 	forcesv[jb].x=forcesv[jb].x-friction.x;
  // 	forcesv[jb].z=forcesv[jb].z-friction.z;
  // 	forcesv[jb].z=forcesv[jb].z-friction.z;


  //  	vector AM={(sA.radius-delta)*n12.x,(sA.radius-delta)*n12.z};

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	BOUNCE IF THERE IS A SOLID BOUNDARY
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
force bounce(sphere s,sphere s0, int critere){
  force hooke;
  vector n12;
  int condition;
  double N,friction,F_n;

  force tors2;
  force dissipation={0.,0.};

  tors2.x=0.;
  tors2.z=0.;



  if(s.position.z+s.radius>Z2)
  {
    double delta=s.position.z+s.radius-b1[NGRAINS+NFIBERS+NWALL].position.z;


    tors2.z=-K_G*delta*R_G/s.radius;
    tors2.x=10.*friction_coeff*host_U1;

  }

  if(s.position.z-s.radius<Z1)
  {
    double delta=b1[NGRAINS+1].position.z-(s.position.z-s.radius);

    tors2.z=K_G*delta*R_G/s.radius;


  }

  return tors2;
}


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMPUTE THE FRICTION ALONG THE CONFINING PLATES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
force plate_friction(sphere s){

  force stokes;


  stokes.x=-cste_stokes*s.radius/R_G*s.velocity.x;
  stokes.z=-cste_stokes*s.radius/R_G*s.velocity.z;


  return stokes;
}




// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMPUTE THE WEIGHT (usually useless)
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

force gravite(sphere s){
  force poids={0.,GRAVITY*s.mass};

  return poids;
}





// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMMON OPERATIONS ON VECTORS, SPHERES, etc
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


double mom(vector v,force f){
  return f.x*v.z-f.z*v.x;
}

/* Computes the distance between two bubbles*/
double distance(sphere sA,sphere sB) {
  double d=0.,dx,dy;

  dx=sB.position.x-sA.position.x;
  if(dx<-LX/2.){
    dx=dx+LX;
  }
  else{
    if(dx>LX/2.) {
      dx=dx-LX;
    }
  }
  dy=sB.position.z-sA.position.z;

  //	if(dy<-LY/2.){
  //		 dy=dy+LY;
  //	}
  //	else{
  //	if(dy>LY/2.) {
  //		dy=dy-LY;
  //	}

  //	}
  d=sqrt(pow(dx,2.)+pow(dy,2.));

  // if(fabs(sB.position.x-sA.position.x)<LX/2.&&fabs(sB.position.z-sA.position.z)<LY/2.)
  // d=sqrt(pow(sA.position.x-sB.position.x,2)+pow(sA.position.z-sB.position.z,2)+pow(sA.position.z-sB.position.z,2));
  // else {
  // if(sB.position.x-sA.position.x>=LX/2.)
  // d=sqrt(pow(LX+sA.position.x-sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
  // /*if(sA.position.x-sB.position.x>LX/2.)*/
  // else d=sqrt(pow(LX-sA.position.x+sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
  // }
  return d;
}
double distanceF(double x1,double x2,double Z1,double Z2) {
  double d=0.,dx,dy;

  dx=x2-x1;
  if(dx<-LX/2.){
    dx=dx+LX;
  }
  else{
    if(dx>LX/2.) {
      dx=dx-LX;
    }
  }
  dy=Z2-Z1;
  //	if(dy<-LY/2.){
  //		 dy=dy+LY;
  //	}
  //	else{
  //	if(dy>LY/2.) {
  //		dy=dy-LY;
  //	}
  //	}
  d=sqrt(pow(dx,2.)+pow(dy,2.));

  return d;
}

/* Computes the modulus of any vector*/
double norm(vector v)
{
  double n=sqrt(v.x*v.x+v.z*v.z);
  return n;
}


/* Periodic Boundary Conditions*/
int CPI(int i) {
  int icp;
  if(i<=NX-1){
    if(i>=0) icp=i;
    else icp=i+NX;
  }
  else icp=i-NX;
  return icp;
}

double XCP(double x) {
  double xcp=x;
  if(x>=X2){
    while(xcp>=X2)
      xcp =xcp-LX;
  }

  else if(x<X1){
    while(xcp<X1)
      xcp =xcp+LX;
  }
  return xcp;
}

int CPJ(int j) {
  int jcp;
  if(j<=NY-1){
    if(j>=0) jcp=j;
    else jcp=j+NY;
  }
  else jcp=j-NY;
  return jcp;
}

double YCP(double y) {
  double ycp=y;
  if(y>=Z2){
    while(ycp>=Z2)
      ycp =ycp-LY;
  }
  else if(y<Z1){
    while(ycp<Z1)
      ycp =ycp+LY;
  }
  return ycp;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	RANDOM NUMBER GENERATOR
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

float ran3(long *idum)
{
  static int inext,inextp;
  static long ma[56];
  static int iff=0;
  long mj,mk;
  int i,ii,k;

  if (*idum < 0 || iff == 0) {
    iff=1;
    mj=MSEED-(*idum < 0 ? -*idum : *idum);
    mj %= MBIG;
    ma[55]=mj;
    mk=1;
    for (i=1;i<=54;i++) {
      ii=(21*i) % 55;
      ma[ii]=mk;
      mk=mj-mk;
      if (mk < MZ) mk += MBIG;
      mj=ma[ii];
    }
    for (k=1;k<=4;k++)
      for (i=1;i<=55;i++) {
        ma[i] -= ma[1+(i+30) % 55];
        if (ma[i] < MZ) ma[i] += MBIG;
      }
    inext=0;
    inextp=31;
    *idum=1;
  }
  if (++inext == 56) inext=1;
  if (++inextp == 56) inextp=1;
  mj=ma[inext]-ma[inextp];
  if (mj < MZ) mj += MBIG;
  ma[inext]=mj;
  return mj*FAC;
}

//------------------------------------------ FINISHED SERIAL C FUNCTIONS, everything from here on in is related to cuda programme ------------------------------------//


//////////////////////////////////////// CUDA RUN FUNCTIONS //////////////////////////////////////////////



//---------------- FUNCTION TO INITIAIALISE ALL VARIABLES AND ARRAYS ON THE DEVICE, AND TO COPY DATA FROM THE HOST INTO THEM ---------------------------//
void device_run(int L, int M, int N, sphere* host_s0, sphere* host_s1, sphere* host_s2, beam* host_b1, force* host_forces,double* host_torque, int* host_AL, int* host_PL, int*
    host_HoL,double LX, double LY,int NBEAMS,double host_U1){
  // Arrays of sphere structures	GPU
  sphere* device_s0;
  sphere* device_s1;
  sphere* device_s2;

  beam* device_b1;

  // arrays of sphere structure components   GPU
  double* device_s0_pos_x;
  double* device_s0_pos_y;
  double* device_s0_vel_x;
  double* device_s0_vel_y;
  double* device_s0_the_z;
  double* device_s0_om_z;
  double* device_s0_radius;
  double* device_s0_mass;
  double* device_s0_moment;
  int* device_s0_cell_i;
  int* device_s0_cell_j;
  int* device_s0_fiber;


  double* device_s1_pos_x;
  double* device_s1_pos_y;
  double* device_s1_vel_x;
  double* device_s1_vel_y;
  double* device_s1_the_z;
  double* device_s1_om_z;
  double* device_s1_radius;
  double* device_s1_mass;
  double* device_s1_moment;
  int* device_s1_cell_i;
  int* device_s1_cell_j;
  int* device_s1_fiber;

  double* device_s2_pos_x;
  double* device_s2_pos_y;
  double* device_s2_vel_x;
  double* device_s2_vel_y;
  double* device_s2_the_z;
  double* device_s2_om_z;
  double* device_s2_radius;
  double* device_s2_mass;
  double* device_s2_moment;
  int* device_s2_cell_i;
  int* device_s2_cell_j;
  int* device_s2_fiber;



  // arrays of beam structure components   GPU
  unsigned int* device_b1_j1;
  unsigned int* device_b1_j2;
  double* device_b1_longueur;
  double* device_b1_tx;
  double* device_b1_ty;
  unsigned int* device_b1_actif;

  int* device_nbactive;
  int host_nbactive=NBEAMS;

  // Arrays for force strutures GPU
  force* device_forces;				// total forces
  double* device_torque;				// viscous forces


  // arrays of force structure components GPU
  double* device_forces_x;
  double* device_forces_y;

  double* device_torque_z;

  // second force structures used for atomic adds in some contact kernels GPU ( when using contacts part 3 )
  double* device_forces_x_2;
  double* device_forces_y_2;

  double* device_torque_z_2;

  //double U1;

  // List arrays for the GPU
  int*   device_HoL;
  int*   device_PL;
  int*   device_AL;

  // Lock for cells on the CPU
  int*   bubble_move_list;

  // Structure containing bubble ids moving in and out of cells GPU
  cell_up* 	cell_list;

  // Lock and moving bubbles struct on the CPU, not nessesary, but use them to initialise the corresponding structures on the GPU
  int* host_bubble_move_list;
  cell_up* host_cell_list;

  // Liquid fraction and bubble volume on the GPU, again hosts are used to initialise the corresponding values on the GPU
  double* device_liquid_fraction;
  double* device_bubble_volume;
  double host_liquid_fraction;
  double host_bubble_volume;

  double* device_U1;


  // Switch for device, switch from compressing to shearing
  int* device_sw;
  int sw=0;
  if(init==-1) sw=0;
  else sw=1;

  // Locks on bubble Ids and Cell Ids
  int* device_mutex;
  int* device_bubble_mutex;
  int cpu_mutexes[(NX+1)*(NY+1)];
  int cpu_bubble_mutexes[NGRAINS+NFIBERS+1];


  // contact list structures
  contact_list* device_contact_list;					// contact lists on the GPU
  contact_list* host_contact_list;
  int* device_contact_list_count;						// seperate array for the count in the contact list structure
  int* host_contact_list_count;

  int* device_max_contacts;							// variable to find maximum number of contacts per bubble

  // Total forces on the walls: transverse (upper wall), normal (bottom wall), tangential (viscous part)
  double* device_upforce;
  double* device_doforce;
  double* device_xforce;
  // double* device_xforcee;

  double* host_upforce;
  double* host_doforce;
  double* host_xforce;
  // double* host_xforcee;


  host_bubble_move_list=(int*)malloc((NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  host_cell_list=(cell_up*)malloc((NX+1)*(NY+1)*sizeof(cell_up));



  int l=0;
  int m=0;
  for(l=0;l<(NGRAINS+NFIBERS+NWALL+1);l++)
    host_bubble_move_list[l]=0;

  for(l=0;l<(NX+1)*(NY+1);l++){
    host_cell_list[l].countin=0;
    host_cell_list[l].countout=0;
    for(m=0;m<9;m++){
      host_cell_list[l].bubin[m]=0;
      host_cell_list[l].bubout[m]=0;
    }
  }


  host_liquid_fraction=liquid_fraction;
  host_bubble_volume=bubble_volume;


  int i=0;
  int j=0;
  // initialise the locks to be copied over
  for(i=0;i<((NX+1)*(NY+1));i++)
    cpu_mutexes[i]=0;

  for(i=0;i<(NGRAINS+NFIBERS+1);i++)
    cpu_bubble_mutexes[i]=0;


  host_contact_list=(contact_list*)malloc((NGRAINS+NFIBERS+NWALL+1)*sizeof(contact_list));
  for(i=0;i<=(NGRAINS+NFIBERS+NWALL);i++){
    host_contact_list[i].count=0;
    for(j=0;j<9;j++){
      host_contact_list[i].contacts[j]=0;
    }
  }
  host_contact_list_count=(int*)malloc((NGRAINS+NFIBERS+NWALL+1)*sizeof(int));

  for(i=0;i<=(NGRAINS+NFIBERS+NWALL);i++){
    host_contact_list_count[i]=0;
  }


  // global varialbes to be copied over




  host_upforce=(double*)malloc(sizeof(double));
  host_doforce=(double*)malloc(sizeof(double));
  host_xforce=(double*)malloc(sizeof(double));
  // host_xforcee=(double*)malloc(sizeof(double));



  host_upforce[0]=0.;
  host_doforce[0]=0.;
  host_xforce[0]=0.;
  // host_xforcee[0]=0.;
  printf(" Started Device Run on CPU\n");


  // Various things
  int iter=2;
  double cste_stokes;

  // counters for cells
  int* device_counter;
  int* host_counter;
  host_counter=(int*)malloc(9*sizeof(int));
  for(i=0;i<9;i++){
    host_counter[i]=0;
  }
  //ARRAYS OF, FORCES, BUBBLES, LISTS //

  cudaError_t ret;
  ret=cudaMalloc((void**)&device_forces,(NGRAINS+NFIBERS+NWALL+1)*sizeof(force));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_forces\n");
  ret=cudaMalloc((void**)&device_torque,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_torque\n");
  ret=cudaMalloc((void**)&device_s0,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0\n");
  ret=cudaMalloc((void**)&device_s1,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1\n");
  ret=cudaMalloc((void**)&device_s2,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2\n");
  ret=cudaMalloc((void**)&device_b1,(NBEAMS+1)*sizeof(beam));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1\n");
  ret=cudaMalloc((void**)&device_HoL,((NX+1)*(NY+1))*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_HOL\n");
  ret=cudaMalloc((void**)&device_PL,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_PL\n");
  ret=cudaMalloc((void**)&device_AL,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_AL\n");



  ret=cudaMalloc((void**)&device_s0_pos_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_pos_x\n");
  ret=cudaMalloc((void**)&device_s0_pos_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_pos_y\n");
  ret=cudaMalloc((void**)&device_s0_vel_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_vel_x\n");
  ret=cudaMalloc((void**)&device_s0_vel_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_vel_y\n");
  ret=cudaMalloc((void**)&device_s0_the_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_the_z\n");
  ret=cudaMalloc((void**)&device_s0_om_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_om_z\n");
  ret=cudaMalloc((void**)&device_s0_moment,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_moment\n");
  ret=cudaMalloc((void**)&device_s0_radius,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_radius\n");
  ret=cudaMalloc((void**)&device_s0_mass,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_mass\n");
  ret=cudaMalloc((void**)&device_s0_cell_i,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_cell_i\n");
  ret=cudaMalloc((void**)&device_s0_cell_j,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_cell_j\n");
  ret=cudaMalloc((void**)&device_s0_fiber,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s0_fiber\n");

  ret=cudaMalloc((void**)&device_s1_pos_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_pos_x\n");
  ret=cudaMalloc((void**)&device_s1_pos_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_pos_y\n");
  ret=cudaMalloc((void**)&device_s1_vel_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_vel_x\n");
  ret=cudaMalloc((void**)&device_s1_vel_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_vel_y\n");
  ret=cudaMalloc((void**)&device_s1_moment,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_moment\n");
  ret=cudaMalloc((void**)&device_s1_the_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_the_z\n");
  ret=cudaMalloc((void**)&device_s1_om_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_om_z\n");
  ret=cudaMalloc((void**)&device_s1_radius,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_radius\n");
  ret=cudaMalloc((void**)&device_s1_mass,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_mass\n");
  ret=cudaMalloc((void**)&device_s1_cell_i,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_cell_i\n");
  ret=cudaMalloc((void**)&device_s1_cell_j,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_cell_j\n");
  ret=cudaMalloc((void**)&device_s1_fiber,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s1_fiber\n");

  ret=cudaMalloc((void**)&device_b1_actif,(NBEAMS+1)*sizeof(unsigned int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1_actif\n");
  ret=cudaMalloc((void**)&device_b1_longueur,(NBEAMS+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1_longueur\n");
  ret=cudaMalloc((void**)&device_b1_tx,(NBEAMS+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1_tx\n");
  ret=cudaMalloc((void**)&device_b1_ty,(NBEAMS+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1_ty\n");
  ret=cudaMalloc((void**)&device_b1_j1,(NBEAMS+1)*sizeof(unsigned int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1_j1\n");
  ret=cudaMalloc((void**)&device_b1_j2,(NBEAMS+1)*sizeof(unsigned int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_b1_j2\n");



  ret=cudaMalloc((void**)&device_s2_pos_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_pos_x\n");
  ret=cudaMalloc((void**)&device_s2_pos_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_pos_y\n");
  ret=cudaMalloc((void**)&device_s2_vel_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_vel_x\n");
  ret=cudaMalloc((void**)&device_s2_vel_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_vel_y\n");
  ret=cudaMalloc((void**)&device_s2_moment,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_moment\n");
  ret=cudaMalloc((void**)&device_s2_the_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_the_z\n");
  ret=cudaMalloc((void**)&device_s2_om_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_om_z\n");
  ret=cudaMalloc((void**)&device_s2_radius,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_radius\n");
  ret=cudaMalloc((void**)&device_s2_mass,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_mass\n");
  ret=cudaMalloc((void**)&device_s2_cell_i,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_cell_i\n");
  ret=cudaMalloc((void**)&device_s2_cell_j,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_cell_j\n");
  ret=cudaMalloc((void**)&device_s2_fiber,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_s2_fiber\n");

  ret=cudaMalloc((void**)&device_forces_x,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_forces_x\n");
  ret=cudaMalloc((void**)&device_forces_y,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_forces_y\n");
  ret=cudaMalloc((void**)&device_torque_z,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_torque_z\n");

  ret=cudaMalloc((void**)&device_forces_x_2,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_forces_x2\n");
  ret=cudaMalloc((void**)&device_forces_y_2,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_forces_Z2\n");
  ret=cudaMalloc((void**)&device_torque_z_2,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_torque_z2\n");


  ret=cudaMalloc((void**)&bubble_move_list,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with bubble_move_list\n");


  ret=cudaMalloc((void**)&cell_list,((NX+1)*(NY+1))*sizeof(cell_up));
  if(ret!=cudaSuccess) printf("Cuda malloc error with cell_list\n");



  // SINGLE VARIABLES

  printf(" sending out liquid fraction of %lf, bubble volume of %f\n",host_liquid_fraction,host_bubble_volume);


  ret=cudaMalloc((void**)&device_liquid_fraction,sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_liquid_fraction\n");
  ret=cudaMalloc((void**)&device_bubble_volume,sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_bubble_volume\n");

  ret=cudaMalloc((void**)&device_sw,sizeof(int));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_sw\n");

  ret=cudaMalloc((void**)&device_U1,sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_U1\n");

  ret=cudaMalloc((void**)&device_upforce,sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_upforce\n");

  ret=cudaMalloc((void**)&device_doforce,sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_doforce\n");

  ret=cudaMalloc((void**)&device_xforce,sizeof(double));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_xforce\n");
  //
  // ret=cudaMalloc((void**)&device_xforcee,sizeof(double));
  // if(ret!=cudaSuccess) printf("Cuda malloc error with device_xforcee\n");
  // locks

  ret=cudaMalloc((void**)&device_mutex,(NX+1)*(NY+1)*(sizeof(int)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with mutexes\n");
  ret=cudaMalloc((void**)&device_bubble_mutex,(NGRAINS+NFIBERS+NWALL+1)*(sizeof(int)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with bubble_mutexes\n");

  ret=cudaMalloc((void**)&device_contact_list,(NGRAINS+NFIBERS+NWALL+1)*(sizeof(contact_list)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with contact_list\n");
  ret=cudaMalloc((void**)&device_contact_list_count,(NGRAINS+NFIBERS+NWALL+1)*(sizeof(int)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with contact_list_count\n");
  ret=cudaMalloc((void**)&device_max_contacts,(NGRAINS+NFIBERS+NWALL+1)*(sizeof(int)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with max_contacts\n");


  ret=cudaMalloc((void**)&device_counter,(9)*(sizeof(int)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_counter\n");

  ret=cudaMalloc((void**)&device_nbactive,(sizeof(int)));
  if(ret!=cudaSuccess) printf("Cuda malloc error with device_counter\n");

  // CUDA MEM CPOIES //

  ret=cudaMemcpy(device_s0,host_s0,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_s0\n");
  ret=cudaMemcpy(device_s1,host_s1,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_s1\n");
  ret=cudaMemcpy(device_s2,host_s2,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_s2\n");

  ret=cudaMemcpy(device_b1,host_b1,(NBEAMS+1)*sizeof(beam),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_b1\n");

  ret=cudaMemcpy(device_AL,host_AL,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_AL\n");
  ret=cudaMemcpy(device_PL,host_PL,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_PL\n");
  ret=cudaMemcpy(device_HoL,host_HoL,(NX+1)*(NY+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_HoL\n");


  ret=cudaMemcpy(device_forces,host_forces,(NGRAINS+NFIBERS+NWALL+1)*sizeof(force),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_forces\n");

  ret=cudaMemcpy(device_torque,host_torque,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_torque\n");

  ret=cudaMemcpy(device_sw,&sw,sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_sw\n");


  ret=cudaMemcpy(device_U1,&host_U1,sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_U1\n");


  ret=cudaMemcpy(device_liquid_fraction,&host_liquid_fraction,sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with dvice_liquid_fraction\n");
  ret=cudaMemcpy(device_bubble_volume,&host_bubble_volume,sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_bubble_volume\n");


  ret=cudaMemcpy(device_mutex,&cpu_mutexes,(NX+1)*(NY+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with mutexes\n");
  ret=cudaMemcpy(device_bubble_mutex,&cpu_bubble_mutexes,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with bubble mutexes\n");


  ret=cudaMemcpy(bubble_move_list,host_bubble_move_list,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with bubble move list\n");


  ret=cudaMemcpy(device_doforce,host_doforce,sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_doforce\n");

  ret=cudaMemcpy(device_upforce,host_upforce,sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_upforce\n");

  ret=cudaMemcpy(device_xforce,host_xforce,sizeof(double),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_xforce\n");
  //
  // ret=cudaMemcpy(device_xforcee,host_xforcee,sizeof(double),cudaMemcpyHostToDevice);
  // if(ret!=cudaSuccess)printf("Cuda memcpy error with device_xforcee\n");

  ret=cudaMemcpy(cell_list,host_cell_list,((NY+1)*(NX+1))*sizeof(cell_up),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with cell_list\n");

  ret=cudaMemcpy(device_contact_list,host_contact_list,(NGRAINS+NFIBERS+NWALL+1)*sizeof(contact_list),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_xforcee\n");
  ret=cudaMemcpy(device_contact_list_count,host_contact_list_count,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_contact_list_count\n");

  ret=cudaMemcpy(device_counter,host_counter,(9)*sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_counter\n");

  ret=cudaMemcpy(device_nbactive,&host_nbactive,sizeof(int),cudaMemcpyHostToDevice);
  if(ret!=cudaSuccess)printf("Cuda memcpy error with device_nbactive\n");



  ///////////////////////////////////////////////////////////////////

  int err=0;

  // note am changoing HoL to a single pointer, not a trio of pointers

  printf(" pushing out to the GPU now \n");
  err=device_evolutionCPU(L, M, N,  device_s0, device_s1,  device_s2,  device_b1, device_forces, device_torque,  device_AL,   device_PL,   device_HoL,  device_bubble_volume,
      device_liquid_fraction,  LX,  LY,    device_sw,  host_s0, host_s1, host_s2, host_b1, host_forces, host_torque,  cell_list,   bubble_move_list,
      device_mutex,host_doforce,host_upforce,host_xforce,device_doforce,device_upforce,device_xforce, host_AL,   host_PL,  host_contact_list, device_contact_list,  device_s0_pos_x,
      device_s0_pos_y,  device_s0_vel_x,  device_s0_vel_y,  device_s0_radius,   device_s0_mass,   device_s0_the_z,  device_s0_om_z,  device_s0_moment,   device_s0_cell_i,
      device_s0_cell_j,device_s0_fiber,   device_s1_pos_x,  device_s1_pos_y,  device_s1_vel_x,  device_s1_vel_y,    device_s1_radius,   device_s1_mass,   device_s1_the_z,    device_s1_om_z,
      device_s1_moment,   device_s1_cell_i,  device_s1_cell_j,device_s1_fiber,   device_s2_pos_x,  device_s2_pos_y,   device_s2_vel_x,  device_s2_vel_y,   device_s2_radius,   device_s2_mass,
      device_s2_the_z,    device_s2_om_z,  device_s2_moment,  device_s2_cell_i,  device_s2_cell_j, device_s2_fiber,  device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,
      device_b1_actif,device_forces_x,  device_forces_y,   device_torque_z,   device_bubble_mutex,   device_max_contacts,  device_forces_x_2,  device_forces_y_2,   device_torque_z_2,  device_contact_list_count, &sw, device_counter,device_nbactive,&host_nbactive,NBEAMS,host_U1,device_U1);


  //  ( L,  M,  N,  device_s0, device_s1, device_s2, device_forces, device_forcesv,  device_AL,  device_PL,  device_HoL,  device_bubble_volume,  device_liquid_fraction, LX,  LY, LZ,device_sw, host_s0, host_s1, host_s2, host_forces,host_forcesv, cell_list,  bubble_move_list, device_mutex,host_doforce,host_upforce,host_xforcev,host_xforcee,device_doforce,device_upforce,device_xforcev,device_xforcee,host_AL,host_PL,host_contact_list,device_contact_list,device_s0_pos_x, device_s0_pos_y, device_s0_pos_z, device_s0_vel_x, device_s0_vel_y, device_s0_vel_z, device_s0_radius,  device_s0_mass,  device_s0_cell_i, device_s0_cell_j, device_s0_cell_k, device_s1_pos_x, device_s1_pos_y, device_s1_pos_z, device_s1_vel_x, device_s1_vel_y, device_s1_vel_z, device_s1_radius,  device_s1_mass,  device_s1_cell_i, device_s1_cell_j, device_s1_cell_k, device_s2_pos_x, device_s2_pos_y, device_s2_pos_z, device_s2_vel_x, device_s2_vel_y, device_s2_vel_z, device_s2_radius,  device_s2_mass,  device_s2_cell_i, device_s2_cell_j, device_s2_cell_k,device_forces_x,device_forces_y,device_forces_z,device_forcesv_x,device_forcesv_y,device_forcesv_z, device_bubble_mutex, device_max_contacts,device_forces_x_2,device_forces_y_2,device_forces_z_2,device_forcesv_x_2,device_forcesv_y_2,device_forcesv_z_2,device_contact_list_count,&sw,device_counter);
  //
  cudaDeviceSynchronize();

  //freeing Cuda procedures
  cudaFree(device_forces);
  cudaFree(device_torque);
  cudaFree(device_s0);
  cudaFree(device_s1);
  cudaFree(device_s2);
  cudaFree(device_b1);
  cudaFree(device_HoL);
  cudaFree(device_AL);
  cudaFree(device_PL);
  cudaFree(device_liquid_fraction);
  cudaFree(device_U1);
  cudaFree(device_bubble_volume);
}

//////////////////////////////////////////////////////////////// CUDA_FUNCS FUNCTIONS //////////////////////////////////////////

////////////////// have to turn this into a CPU function, and call other kernels on the GPU ////////////////////////
int device_evolutionCPU(int L, int M, int N, sphere* device_s0, sphere* device_s1, sphere* device_s2, beam* device_b1, force* forces,double* torque, int* AL, int* PL, int* HoL,double*
    device_bubble_volume, double* device_liquid_fraction, double LX, double LY, int* device_sw, sphere* host_s0, sphere* host_s1,sphere* host_s2, beam* host_b1, force* host_forces,double*
    host_torque, cell_up* cell_list, int* bubble_move_list, int* device_mutex,double* host_doforce, double* host_upforce, double* host_xforce,  double* device_doforce, double*
    device_upforce, double* device_xforce, int* host_AL, int* host_PL, contact_list* host_contact_list,contact_list* device_contact_list,double* device_s0_pos_x,double*
    device_s0_pos_y,double* device_s0_vel_x,double* device_s0_vel_y,double* device_s0_radius, double* device_s0_mass, double* device_s0_the_z,double* device_s0_om_z,double*
    device_s0_moment, int* device_s0_cell_i,int* device_s0_cell_j,int* device_s0_fiber,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_vel_x,double*
    device_s1_vel_y,double* device_s1_radius, double* device_s1_mass,double* device_s1_the_z,double* device_s1_om_z,double* device_s1_moment, int* device_s1_cell_i,int*
    device_s1_cell_j,int* device_s1_fiber,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_radius, double*
    device_s2_mass,double* device_s2_the_z,double* device_s2_om_z,double* device_s2_moment,int* device_s2_cell_i,int* device_s2_cell_j,int* device_s2_fiber,unsigned int*
    device_b1_j1,unsigned int* device_b1_j2, double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty, unsigned int* device_b1_actif,double* device_forces_x,double* device_forces_y,double* device_torque_z, int*
    device_bubble_mutex, int* device_max_contacts,double* device_forces_x_2,double* device_forces_y_2,double* device_torque_z_2,int* device_contact_list_count,int* host_sw, int* device_counter,int* device_nbactive, int* host_nbactive,int NBEAMS,double host_U1,double* device_U1){


  // Timers to be used with clock timing functions
  clock_t start = clock(), diff;
  clock_t start_contacts,stop_contacts,total_contacts;
  clock_t start_verlet,stop_verlet,total_verlet;
  clock_t start_list_update,stop_list_update,total_list_update;
  clock_t start_move_walls,stop_move_walls,total_move_walls;
  clock_t start_wall_forces,stop_wall_forces,total_wall_forces;

  // Times for kernels
  int contacts_sec,contacts_milsec;
  int verlet_sec,verlet_milsec;
  int list_update_sec,list_update_milsec;
  int move_walls_sec,move_walls_milsec;
  int wall_forces_sec,wall_forces_milsec;
  int sec;
  int milsec;
  // int NBEAMS2=NBEAMS;
  // int NBACTIVE[2];
  // NBACTIVE[0]=NBEAMS2;
  // double* NBACTIVE_double=NULL;
  // double NBACTIVE_double=1.*NBACTIVE;
  host_nbactive[0]=NBEAMS;
  int NBACTIVE0=*host_nbactive;
  // printf("YOUHOUUUUUUUUUUUUUU %d\t  %d\t\n",NBEAMS,*host_nbactive);

#ifdef With_Events
  // Timers to be used with CUDA Events
  cudaEvent_t event_start, finish;
  float elapsedTime;
  cudaEventCreate(&event_start);
  cudaEventCreate(&finish);

  float contacts_event_time;
  float verlet_event_time;
  float list_update_event_time;
  float move_walls_event_time;
  float wall_forces_event_time;
#endif


  int percision = 25;				// precision for some of the print out functions

  total_contacts=0;				// Initialise all the times to zero
  total_verlet=0;
  total_move_walls=0;
  total_list_update=0;
  total_wall_forces=0;


  ///////////////////////// Setting Up all the Blocks //////////////////////////

  dim3 dimBlock(BLOCKSIZEX);							// Blocksize for grid which covers all bubbles including wall bubbles
  dim3 dimBlockUpdate(BLOCKSIZEXUPDATE);				// Blocksize for the Update bubbles, reset forces kernel
  dim3 dimBlockParallel(BLOCKSIZEXPARALLEL);			// Blocksize used in parallel update
  dim3 dimBlockMoveWalls(BLOCKSIZEXMOVEWALLS);		// Blocksize for the move walls kernel
  dim3 dimBlockVerlet(BLOCKSIZEVERLET);				// Blocksize for the verlet kernel
  dim3 dimBlockcell(BLOCKSIZEXCELL);					// Blocksize which puts a thread per cell, used for Cell in and out update kernels
  dim3 dimBlockbulk(BLOCKSIZEXBULK);					// Blocksize which gives one thread per bulk bubble
  dim3 dimBlockbulknostreams(BLOCKSIZEXBULKNOSTREAMS);//
  dim3 dimBlockbulkpared(BLOCKSIZEXBULKPARED);		//
  dim3 dimBlockbulkparedpart22(BLOCKSIZEXBULKPAREDPart22);// BLocksize for contacts part 2 part 2
  dim3 dimBlockbulknew(BLOCKSIZEXBULKNEW);			//
  dim3 dimBlockbulkpart2(BLOCKSIZEXBULKPART2);		//
  dim3 dimBlockWALL(BLOCKSIZEXWALL); 	// Blocksize for getting the wall forces
  dim3 dimBlockFIBERS(BLOCKSIZEXFIBERS);

  dim3 dimBlockListUpdate(BLOCKNUMBERUPDATE);

  /// Set the appropriate number of blocks with the given blocksize for the number of threads needed
  int Xblocks = (((int)(NGRAINS+NFIBERS+NWALL+1)/dimBlock.x) + (!((int)(NGRAINS+NFIBERS+NWALL+1)%dimBlock.x)?0:1));           // One thread per Bubble ( NWALL and Bulk Bub
  int XBBeams= (((int)(NBEAMS+1)/dimBlock.x) + (!((int)(NBEAMS+1)%dimBlock.x)?0:1));
  int XblocksUpdate = (((int)(NGRAINS+NFIBERS+NWALL+1)/dimBlockUpdate.x) + (!((int)(NGRAINS+NFIBERS+NWALL+1)%dimBlockUpdate.x)?0:1));    // One thread per bubble
  int XblocksMoveWalls = (((int)(NWALL/2+1)/dimBlockMoveWalls.x) + (!((int)(NWALL/2+1)%dimBlockMoveWalls.x)?0:1));         // ONe thread per bubble on top wall
  int XblocksParallel =(((int)(NGRAINS+NFIBERS+NWALL+1)/dimBlockParallel.x) + (!((int)(NGRAINS+NFIBERS+NWALL+1)%dimBlockParallel.x)?0:1));	// One thred per bubble
  int XCellBlocks = (((int)((NX+1)*(NY+1))/dimBlockcell.x)+(!((int)((NX+1)*(NY+1))%dimBlockcell.x)?0:1));		// One thread per cell
  int XBulkBlocks = (((int)(NGRAINS+NFIBERS+1)/dimBlockbulk.x) + (!((int)(NGRAINS+NFIBERS+1)%dimBlockbulk.x)?0:1));  						// one thread per bulk bubble
  int XBulkBlocksNoStreams = (((int)(NGRAINS+NFIBERS+1)/dimBlockbulknostreams.x) + (!((int)(NGRAINS+NFIBERS+1)%dimBlockbulknostreams.x)?0:1));  // One thread per bulk bubble
  int XBulkBlocksPared = (((int)(NGRAINS+NFIBERS+1)/dimBlockbulkpared.x) + (!((int)(NGRAINS+NFIBERS+1)%dimBlockbulkpared.x)?0:1)); 		// One thread per bulk bubble
  int XBulkBlocksParedPart22 = (((int)(NGRAINS+NFIBERS+NWALL+1)/dimBlockbulkparedpart22.x) + (!((int)(NGRAINS+NFIBERS+NWALL+1)%dimBlockbulkparedpart22.x)?0:1)); 		// One thread per bulk bubble
  int XBeams = (((int)(NBEAMS+1)/dimBlockbulknostreams.x) + (!((int)(NBEAMS+1)%dimBlockbulknostreams.x)?0:1));  // One thread per beam
  int XFibers = (((int)(NFIBERS+1)/dimBlockbulknostreams.x) + (!((int)(NFIBERS+1)%dimBlockbulknostreams.x)?0:1));  // One thread per fiber bead

  int XVerletBlocks = (((int)(NGRAINS+NFIBERS+1)/dimBlockVerlet.x) + (!((int)(NGRAINS+NFIBERS+1)%dimBlockVerlet.x)?0:1)); 					// One thread per bulk bubble
  int XBulkBlocksnew = (((int)(9*(NGRAINS+NFIBERS)+1)/dimBlockbulknew.x) + (!((int)(9*(NGRAINS+NFIBERS)+1)%dimBlockbulknew.x)?0:1)); 	// One thread per bulk bubble
  int XBulkBlockspart2 = (((int)(9*(NGRAINS+NFIBERS)+1)/dimBlockbulkpart2.x) + (!((int)(9*(NGRAINS+NFIBERS)+1)%dimBlockbulkpart2.x)?0:1));  // One thread per bulk bubble
  int XWallBlocks = (((int)(NWALL+1)/dimBlockWALL.x)+(!((int)(NWALL+1)%dimBlockWALL.x)?0:1));
  int XFibersBlocks = 2;
  //(((int)(NFIBERS+1)/dimBlockFIBERS.x)+(!((int)(NFIBERS+1)%dimBlockFIBERS.x)?0:1));// one thread per wall bubble
  int XListUpdate=(((int)(NGRAINS+NFIBERS+NWALL+1)/(dimBlockListUpdate.x)) + (!((int)(NGRAINS+NFIBERS+NWALL+1)%(dimBlockListUpdate.x))?0:1));   // One thred per bubble
  //int XListUpdate=(((int)(NGRAINS+NWALL+1)/(dimBlockListUpdate.x*dimBlockListUpdate.z)) + (!((int)(NGRAINS+NWALL+1)%(dimBlockListUpdate.x*dimBlockListUpdate.z))?0:1));	// One thred per bubble

  // Set up grids for the defined blocks
  dim3 dimGrid (Xblocks);
  dim3 dimGBeams (XBBeams);
  dim3 dimGridUpdate (XblocksUpdate);
  dim3 dimGridParallel (XblocksParallel);
  dim3 dimGridMoveWalls (XblocksMoveWalls);
  dim3 dimCellGrid (XCellBlocks);
  dim3 dimBulkGrid (XBulkBlocks);
  dim3 dimBulkGridNoStreams (XBulkBlocksNoStreams);
  dim3 dimBeams (XBeams);
  dim3 dimFibers (XFibers);
  dim3 dimBulkGridPared (XBulkBlocksPared);
  dim3 dimBulkGridParedPart22 (XBulkBlocksParedPart22);
  dim3 dimVerletGrid (XVerletBlocks);
  dim3 dimBulkGridnew(XBulkBlocksnew);
  dim3 dimBulkGridpart2(XBulkBlockspart2);
  dim3 dimGridWALL(XWallBlocks);
  dim3 dimGridFIBERS(XFibersBlocks);
  int dimListUpdate=XListUpdate;

  int host_const_upper_bound;					// A CPU variable for the upper bound in z cell when system is compressed

#ifdef With_steps
  printf("hello -1 \n");
#endif

  cudaError_t ret;							// Cuda error function to return and check for errors


  int i=0;									// iteration number
  int sw2=0;									// switch
  int init2;									// iteration number that includes starting point
  int j;										// counters
  int k;
  int l;
  double t=0.;								// time of the system
  char wfname[250];
  FILE* wallfile;
  sprintf(wfname,"%s/wallforces.dat",directory,directory);// File the wall forces are written to
  wallfile=fopen(wfname,"w");
  fclose(wallfile);


  // The initial configuration files, shows the initial configuration of the system, in b0 b1 and b2 bubble arrays
  FILE* inf1;

#ifdef With_steps
  printf("bonjour 1\n");
#endif


  char inf1file[150];
  sprintf(inf1file,"%s/walls.dat",directory);
  inf1=fopen(inf1file,"w");


  // This file writes the time taken between print steps to a file, used to analyse the time taken per print step iterations over a long run. This is useful as the programme
  // slows down over long runs
#ifdef long_time_check
  FILE* longf;
  char longffile[150];
  sprintf(longffile,"%s/long_times_38400_recent.dat",directory);
  longf=fopen(longffile,"w");
#endif

  // These files are for the bubble data files ( position, velocity, radius and force) that get printed out every print-step.
  char fileb1[150];
  char fileb2[150];
  FILE* fpb1;
  FILE* fpb2;

#ifdef With_steps
  printf("hello 0 \n");
#endif

  // If starting from scratch, init2 is set to zero, otherwise it is set to the file number we start from.
  if(init==-1) {
    init2=0;
  }
  else {
    init2=init;
  }


  cudaError_t error;		// another cuda error variable, used to check the last error that comes up ( used to exit the programme if there is an error)

  // Write out the initial configurations to the first bubble files
  write_out(host_s0,host_s1,host_s2,host_b1,host_forces,host_torque,device_s0,device_s1,device_s2,device_b1,forces,torque,host_doforce,host_upforce,host_xforce,device_doforce,device_upforce,device_xforce,host_AL,host_PL,AL,PL,host_contact_list,device_contact_list,device_sw,host_sw,device_nbactive,host_nbactive); // write data from GPU to CPU
  for(j=1;j<=NGRAINS+NFIBERS;j++){
    fprintf(inf1,"%.12f \t %.12f \t %.6f 0. \t 0.\n",host_b1[j].position.x,host_b1[j].position.z,host_b1[j].radius);
  }
  fclose(inf1);

#ifdef With_steps
  printf("hello 1 \n");
#endif

  // move the bubble structures to arrays of bubble structure components
  bubbles_to_arrays<<<dimGrid,dimBlock>>>(  device_s0_pos_x,  device_s0_pos_y,  device_s0_the_z,  device_s0_vel_x,  device_s0_vel_y, device_s0_om_z,  device_s0_radius,   device_s0_mass,  device_s0_moment,  device_s0_cell_i, device_s0_cell_j,device_s0_fiber, device_s1_pos_x,  device_s1_pos_y, device_s1_the_z,  device_s1_vel_x,  device_s1_vel_y,   device_s1_om_z,  device_s1_radius,   device_s1_mass,   device_s1_moment, device_s1_cell_i, device_s1_cell_j,device_s1_fiber, device_s2_pos_x,  device_s2_pos_y,  device_s2_the_z,  device_s2_vel_x,  device_s2_vel_y,    device_s2_om_z,  device_s2_radius,   device_s2_mass,  device_s2_moment, device_s2_cell_i,device_s2_cell_j,device_s2_fiber, device_s0,  device_s1, device_s2, Xblocks);
  // beams_to_arrays<<<dimGBeams,dimBlock>>>( device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,device_b1_actif, device_b1, XBBeams,NBEAMS);

  // zero the bubble volume
  zero_bubble_volume<<<dimGrid,dimBlock>>>(device_bubble_volume);
  cudaDeviceSynchronize();
  // calculate the bubble volume, using an atomic add, it needs to be zeroed first.
  bubble_volume_update<<<dimGrid,dimBlock>>>(device_bubble_volume,device_s1_radius);
  cudaDeviceSynchronize();
  // print some information to the screen, liquid fraction, switch state, bubble volume, container volume, and system time
  print_liquid_fraction<<<dimGrid,dimBlock>>>(device_liquid_fraction,device_sw,device_bubble_volume,device_s1_pos_y,Xblocks ,t,U0);

  // if looking for max contacts, we zero it here
#ifdef max_contacts
  device_max_contacts_zero<<<dimBulkGrid,dimBlockbulk>>>(device_max_contacts);
#endif

  cudaDeviceSynchronize();

  // move the forces from structures to arrays of structure components
  forces_to_arrays<<<dimGrid,dimBlock>>>(forces,torque,device_forces_x,device_forces_y,device_torque_z);

  cudaDeviceSynchronize();
  //  	print_liquid_fraction<<<dimGrid,dimBlock>>>(device_liquid_fraction,device_sw,device_bubble_volume,device_s1_pos_y,Xblocks ,t,U0);
  //
  // cudaDeviceSynchronize();
  // zero the cell moving lists
  zero_cells_old<<<dimCellGrid,dimBlockcell>>>(cell_list,Xblocks );

  cudaDeviceSynchronize();
  // zero the wall forces variables
  reset_wall_forces<<<dimGrid,dimBlock>>>(device_doforce,device_upforce,device_xforce,Xblocks );

  cudaDeviceSynchronize();
  // zero the bubble contact lists
  zero_contact_lists<<<dimGrid,dimBlock>>>(Xblocks,device_contact_list);

  cudaDeviceSynchronize();
  // reset all the force arrays to zero
  reset_forces<<<dimGrid,dimBlock>>>(device_forces_x_2,device_forces_y_2,device_torque_z_2,Xblocks );
  cudaDeviceSynchronize();
  print_liquid_fraction<<<dimGrid,dimBlock>>>(device_liquid_fraction,device_sw,device_bubble_volume,device_s1_pos_y,Xblocks ,t,U0);


  printf("Main loop is about to start... %d \n\n",host_sw[0]);
  //---------------------------------------------------------------- BEGIN THE MAIN LOOP ------------------------------------------------------------//

  for(i=1;i<NITER;i++){
    t=i*dt;
#ifdef With_steps
    if(i>1000) printf("Contact step is about to start...\n");
#endif
    //------------------------ Contacts Step (computes forces between bubbles) -------------------------------


    // If using the right contacts part 1 function we update the const upper bound ( number of  cells in the z direction containing bubbles)
    // #ifdef no_streams_ldg_half_const_bound_seperate_count_prefetch_ILP_ldg
    // if(host_sw[0]==1 && sw2==0){					// if it is finished compressing and hasnt already been updated ( sw2)
    // ret=cudaMemcpy(&host_const_upper_bound,&device_s1_cell_k[NGRAINS+NWALL],sizeof(int),cudaMemcpyDeviceToHost);
    // if(ret!=cudaSuccess)printf("Cuda memcpy error with const_upper_bound\n");
    // cudaMemcpyToSymbol(const_upper_bound,&host_const_upper_bound,sizeof(host_const_upper_bound));		// copy const upper bound into devce contant memory
    // sw2=1;											// set switch 2 = 1, this means we will never update upper bound again ( initial if stratement)
    // }
    // #endif


    // If using Cuda Events timing fuctions
#ifdef With_Events
    cudaEventRecord(event_start,0);
#endif
    // If using clock timing functions
#ifdef WITH_TIMING
    start_contacts=clock();
#endif
    // 		  XBeams = (((int)(NBACTIVE+1)/dimBlockbulknostreams.x) + (!((int)(NBACTIVE+1)%dimBlockbulknostreams.x)?0:1));
    // 		dim3 dimBeams(XBeams);
    // 		NBACTIVE0=NBACTIVE[0];
    // 		NBACTIVE_double=0;
    // 		host_nbactive[0]=0;


    // Updating the beams
#ifdef beams_on
    // 	  beams_update<<<dimBeams,dimBlockbulknostreams>>>(device_s1_pos_x,device_s1_pos_y,device_s1_radius,device_s1_fiber,device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,device_b1_actif,device_forces_x_2,device_forces_y_2,NBACTIVE0,device_nbactive);

    // 		NBACTIVE=(int)(NBACTIVE_double);
    // 	cudaDeviceSynchronize();

#endif

    // fibertension<<<dimGrid,1>>>(device_s1_pos_x,device_s1_pos_y,device_s1_vel_x,device_s1_vel_y,device_s1_om_z,device_s1_radius,device_s1_fiber,device_forces_x_2,device_forces_y_2,device_torque_z_2, device_contact_list,device_contact_list_count);
    // 	cudaDeviceSynchronize();


    // Use the Contacts Part 1 function with multiple threads per bulk bubble ( I call It streams, but its not using actual cuda streams, just a large grid )
#ifdef part1_TLP
    contacts_part1_TLP<<<dimBulkGridnew,dimBlockbulknew>>>(HoL,PL,device_s1_pos_x,device_s1_pos_y, device_s1_vel_x, device_s1_vel_y, device_s1_radius,  device_s1_cell_i,  device_s1_cell_j,device_s1_fiber, i, 0, device_contact_list,device_bubble_mutex,device_contact_list_count);
#endif
    // Use the Contacts part 1 with only one thread per bulk bubble
#ifdef part1_ILP
    contacts_part1_ILP<<<dimBulkGridNoStreams,dimBlockbulknostreams>>>(HoL,PL,device_s1_pos_x,device_s1_pos_y, device_s1_vel_x, device_s1_vel_y,   device_s1_radius,  device_s1_cell_i,  device_s1_cell_j,device_s1_fiber,  i, 0, device_contact_list,device_bubble_mutex,device_contact_list_count);
#endif

    // If want to check max contacts
#ifdef max_contacts
    device_max_contacts_func<<<dimBulkGrid,dimBlockbulk>>>(device_contact_list,device_max_contacts);
#endif
#ifdef With_steps
    if(i>1000) printf("contact2 step is about to start...\n");
#endif
    // Using Contacts part 2 with one thread per bulk bubble
#ifdef part2_ILP
    contacts_part2_ILP<<<dimBulkGridPared,dimBlockbulkpared>>> (device_s1_pos_x,device_s1_pos_y,  device_s1_vel_x, device_s1_vel_y,device_s1_om_z,device_s1_radius,device_forces_x,device_forces_y,device_torque_z,device_contact_list,device_forces_x_2,device_forces_y_2,device_torque_z_2,device_contact_list_count,LX);

    // 	  fiber_forces<<<dimFibers,dimBlockbulknostreams>>>(device_s1_pos_x,device_s1_pos_y,device_s1_radius,device_s1_fiber,device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,device_b1_actif,device_forces_x,device_forces_y,LX);
    //
    cudaDeviceSynchronize();
#ifdef With_steps
    if(i>1000) printf("contact3 step is about to start...\n");
#endif
    // Contacts Part 3, adds together forces from two arrays ( this is used to minimise the number of atomic adds using the previous contacts Part 2 function)
    contacts_part3_ILP<<<dimBulkGridParedPart22,dimBlockbulkparedpart22>>>(device_forces_x,device_forces_y,device_torque_z,device_forces_x_2,device_forces_y_2,device_torque_z_2);
#endif
    // Using Contacts Part 2 with multiple threads per bulk bubble ( note doesnt need a contacts part 3 kernel )
#ifdef part2_TLP
    contacts_part2_TLP<<<dimBulkGridpart2,dimBlockbulkpart2>>> (device_s1_pos_x,device_s1_pos_y,  device_s1_vel_x, device_s1_vel_y,   device_s1_radius,device_forces_x,device_forces_y,device_torque_z,device_contact_list,device_contact_list_count);
#endif

#ifdef With_steps
    if(i>0) printf("about to compute fiber forces... %f %f \n",*(device_s1_radius+340),*(device_s1_radius+888));
#endif



    // Get the end time using timing functions ( if defined )
#ifdef WITH_TIMING
    cudaDeviceSynchronize();
#endif

#ifdef WITH_TIMING
    stop_contacts=clock();
    total_contacts+=stop_contacts-start_contacts;
#endif

#ifdef With_Events
    cudaEventRecord(finish,0);
    cudaEventSynchronize(finish);
    cudaEventElapsedTime(&elapsedTime,event_start,finish);
    contacts_event_time+=elapsedTime;
#endif
#ifdef With_steps
    if(i>1000) printf("Verlet step is about to start...\n");
#endif
    // --------------------- Verlet step (integrate positions using the forces computed in contacts step ) ------------------------------------

    // start timing functions ( if defined )
#ifdef With_Events
    cudaEventRecord(event_start,0);
#endif

#ifdef WITH_TIMING
    start_verlet=clock();
#endif
    // Integrate positions using the verlet function
    verletfunc<<<dimVerletGrid,dimBlockVerlet>>>(device_s0_pos_x,  device_s0_pos_y,  device_s1_pos_x,  device_s1_pos_y, device_s1_vel_x,  device_s1_vel_y,   device_s1_om_z,  device_s1_radius,   device_s1_mass,   device_s1_moment, device_s2_pos_x,  device_s2_pos_y,  device_s2_vel_x,  device_s2_vel_y,    device_s2_om_z, device_forces_x,device_forces_y,device_torque_z,LX,LY,device_liquid_fraction,device_bubble_volume,Xblocks,i );

    // finish timing functions ( if defined)
#ifdef WITH_TIMING
    cudaDeviceSynchronize();
#endif

#ifdef WITH_TIMING
    stop_verlet=clock();
    total_verlet+=stop_verlet-start_verlet;
#endif

#ifdef With_Events
    cudaEventRecord(finish,0);
    cudaEventSynchronize(finish);
    cudaEventElapsedTime(&elapsedTime,event_start,finish);
    verlet_event_time+=elapsedTime;
#endif
#ifdef With_steps
    if(i>1000) printf("Wall step is about to start...\n");
#endif
    // --------------------- Move Walls Step ( either compress top wall, or shear top wall ---------------------------------

    // start timing functions ( if defined)
#ifdef With_Events
    cudaEventRecord(event_start,0);
#endif
#ifdef WITH_TIMING
    start_move_walls=clock();
#endif
    // Move walls, device switch defines wether or not to compress or to shear

    move_walls<<<dimGridMoveWalls,dimBlockMoveWalls>>>(device_s1_pos_x,device_s1_pos_y,device_s2_pos_x,device_s2_pos_y,device_s2_vel_x,device_s2_vel_y,device_sw,device_liquid_fraction,LX,Xblocks,i,device_U1);

    // end timing functions ( if defined )
#ifdef WITH_TIMING
    cudaDeviceSynchronize();
#endif

#ifdef WITH_TIMING
    stop_move_walls=clock();
    total_move_walls+=stop_move_walls-start_move_walls;
#endif
#ifdef With_Events
    cudaEventRecord(finish,0);
    cudaEventSynchronize(finish);
    cudaEventElapsedTime(&elapsedTime,event_start,finish);
    move_walls_event_time+=elapsedTime;
#endif
#ifdef With_steps
    if(i>1000) printf("Update step is about to start...\n");
#endif
    // ------------------------ Section to compute wall forces and update bubbbles -----

    // start timing functions ( if defined )
#ifdef With_Events
    cudaEventRecord(event_start,0);
#endif
#ifdef WITH_TIMING
    start_wall_forces=clock();
#endif
    // Calculate the forces on the walls, tangental and perpendicular ( Shearing and compressing stresses )
    wall_forces_final_2<<<dimGridWALL,dimBlockWALL>>>(device_forces_x,device_forces_y,device_torque_z,device_doforce,device_upforce, device_xforce, Xblocks,i );
#ifdef WITH_TIMING
    cudaDeviceSynchronize();
#endif
#ifdef WITH_TIMING
    stop_wall_forces=clock();
    total_wall_forces+=stop_wall_forces-start_wall_forces;
#endif

#ifdef With_Events
    cudaEventRecord(finish,0);
    cudaEventSynchronize(finish);
    cudaEventElapsedTime(&elapsedTime,event_start,finish);
    wall_forces_event_time+=elapsedTime;
#endif
#ifdef With_steps
    if(i>1000) printf("Printing step is about to start... %d \n",i);
#endif
    //--------------------------------------------------- Section to print out intermediate results to some files ------------------------------------//

    // if(i==73000) print_step3=1;
    //		if(i%print_step3==0||(i>650000&&i%100==0)){
    if(i%print_step3==0){
      double  ymax=Z1;
      diff=clock()-start;										// total time
      sec = diff / CLOCKS_PER_SEC;							// total time seconds
      milsec=diff*1000/CLOCKS_PER_SEC;						// total time miliseconds

      contacts_milsec=total_contacts*1000/CLOCKS_PER_SEC;		// time for individual kernels
      verlet_milsec=total_verlet*1000/CLOCKS_PER_SEC;
      move_walls_milsec=total_move_walls*1000/CLOCKS_PER_SEC;
      list_update_milsec=total_list_update*1000/CLOCKS_PER_SEC;
      wall_forces_milsec=total_wall_forces*1000/CLOCKS_PER_SEC;

#ifdef long_time_check												// print time taken per print step iterations to a file
      fprintf(longf,"%d \n",sec);
      fflush(longf);
#endif
      // Print details to the screen
      printf("\n iteration is %d  \t",i);
      printf(" Time taken so far is %d mins, or  %d seconds %d milliseconds\n",sec/60, sec,milsec%1000);
      // 			for(j=NGRAINS+NWALL/2;j<=NGRAINS+NWALL;j+=1){
      // 			  if(b1[j].cell.k!=b1[NGRAINS+NWALL].cell.k) printf("%d %f %f \n",j,b1[j].position.z,b1[NGRAINS+NWALL].position.z);
      // 			}
      // 			printf("\n");
#ifdef WITH_TIMING
      printf(" Contacts time so far is %d seconds, %d miliseconds \n",contacts_milsec/1000,contacts_milsec%1000);
      printf(" Verlet time so far is  %d seconds, %d miliseconds \n",verlet_milsec/1000,verlet_milsec%1000);
      printf(" Move_Walls time so far is  %d seconds, %d miliseconds \n",move_walls_milsec/1000,move_walls_milsec%1000);
      printf(" List_update time so far is  %d seconds, %d miliseconds \n",list_update_milsec/1000,list_update_milsec%1000);
      printf(" Wall_Forces time so far is  %d seconds, %d miliseconds \n",wall_forces_milsec/1000,wall_forces_milsec%1000);
#endif
#ifdef With_Events
      printf(" Contacts time so far is %f seconds, \n",contacts_event_time/1000);
      printf(" Verlet time so far is  %f seconds \n",verlet_event_time/1000);
      printf(" Move_Walls time so far is  %f seconds, \n",move_walls_event_time/1000);
      printf(" List_update time so far is  %f seconds, \n",list_update_event_time/1000);
      printf(" Wall_Forces time so far is  %f seconds,\n",wall_forces_event_time/1000);
#endif

      // 			cell_counter<<<dimCellGrid,dimBlockcell>>>(HoL,PL,device_counter);
      // 			cell_counter_print<<<dimCellGrid,dimBlockcell>>>(device_counter);

      // print liquid fraction and switch info to the screen
      //			printf("about to print liquid fraction %d\n",i);
      print_liquid_fraction<<<dimGrid,dimBlock>>>(device_liquid_fraction,device_sw,device_bubble_volume,device_s1_pos_y,Xblocks ,t,U0);

      // write the structure component arrays back to structures that can be copied over to the host to print to files
      arrays_to_bubbles<<<dimGrid,dimBlock>>>( device_s0_pos_x,  device_s0_pos_y,  device_s0_the_z,  device_s0_vel_x,  device_s0_vel_y,    device_s0_om_z,  device_s0_radius,   device_s0_mass,  device_s0_moment,  device_s0_cell_i, device_s0_cell_j,device_s0_fiber, device_s1_pos_x,  device_s1_pos_y,  device_s1_the_z,  device_s1_vel_x,  device_s1_vel_y,   device_s1_om_z,  device_s1_radius,   device_s1_mass,   device_s1_moment, device_s1_cell_i, device_s1_cell_j,device_s1_fiber, device_s2_pos_x,  device_s2_pos_y, device_s2_the_z,  device_s2_vel_x,  device_s2_vel_y,   device_s2_om_z,  device_s2_radius,   device_s2_mass,  device_s2_moment, device_s2_cell_i,device_s2_cell_j,device_s2_fiber, device_s0,  device_s1, device_s2, Xblocks);
      // 			arrays_to_beams<<<dimGBeams,dimBlock>>>( device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,device_b1_actif,device_b1, XBBeams,device_nbactive);
      arrays_to_forces<<<dimGrid,dimBlock>>>(forces,torque,device_forces_x,device_forces_y,device_torque_z);
      cudaDeviceSynchronize();
      // write the GPU date to the CPU
      write_out(host_s0,host_s1,host_s2,host_b1,host_forces,host_torque,device_s0,device_s1,device_s2,device_b1,forces,torque,host_doforce,host_upforce,host_xforce,device_doforce,device_upforce,device_xforce,host_AL,host_PL,AL,PL,host_contact_list,device_contact_list,device_sw,host_sw,device_nbactive,host_nbactive);
      // 									printf("there, it's done %d\n",i);

      // label the filenames for bubble data ( name contains bubble array 0,1 or 2 and the file  number ( how many print steps it is )
      sprintf(files1,"%s/positions%.6d.dat",directory,(int)(init2+i/print_step));
      fpb1=fopen(files1,"w");
      // write bubble data to files
      ymax=0.;
      //
      // 			  			  fwrite(&host_s1,sizeof(sphere),NGRAINS,fpb1);
      // 				if(j==3)		printf("%d %.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.6f  \n",i,host_b1[j].position.x,host_b1[j].position.z,host_b1[j].position.z,host_b1[j].velocity.x,host_b1[j].velocity.z,host_b1[j].velocity.z,host_b1[j].theta.x,host_b1[j].theta.z,host_b1[j].theta.z,host_b1[j].radius);
      for(j=+1;j<=NGRAINS+NFIBERS+NWALL;j++){
        fprintf(fpb1," %.12f \t %.12f \t %.12f \t %.12f \t %.6f \t %.12f \t %.12f  \n",host_b1[j].position.x,host_b1[j].position.z,host_b1[j].velocity.x,host_b1[j].velocity.z,host_b1[j].radius,host_forces[j].x,host_forces[j].z);
        if(host_b1[j].position.z>ymax) ymax=host_b1[j].position.z;
        // 							if(j==15) ymax=ymax+.5*host_b1[j].mass*(host_b1[j].velocity.x*host_b1[j].velocity.x+host_b1[j].velocity.z*host_b1[j].velocity.z+host_b1[j].velocity.z*host_b1[j].velocity.z)+host_b1[j].mass*GRAVITY*host_b1[j].position.z;
      }

#ifdef beams_on
      //                        sprintf(files2,"%s/beams%.6d.dat",directory,(int)(init2+i/print_step));
      //                      fpb2=fopen(files2,"w");

      //			for(j=1;j<NBEAMS;j++){
      //			 fprintf(fpb2,"%ld \t %ld \t %ld \t %f \t %d\n",j,b1[j].j1,b1[j].j2,b1[j].longueur,b1[j].actif);
      //			}
      //fclose(fpb2);
#endif
      //	printf("\n iteration %d highest grain is at z=%f active beams are %d \t %d \n",i,ymax,*host_nbactive,device_nbactive);
      printf("\n iteration %d highest grain is at z=%f fiber goes at vx= %f \n",i,ymax,host_b1[NGRAINS+1].velocity.x);
      fclose(fpb1);

      ////////////////////////////////////////////////////


#ifdef max_contacts					//calculate maximum contacts for a bubble and print info to screen
      device_max_contacts_max<<<dimBulkGrid,dimBlockbulk>>>(device_max_contacts);
#endif
      // write wall forces to a file, and then reset wall forces. Done in here as wall forces are averaged over print-step iterations
      write_wall_forces_to_file(wfname,wallfile,host_doforce,host_upforce,host_xforce,host_s1,i);
      reset_wall_forces<<<dimGrid,dimBlock>>>(device_doforce,device_upforce,device_xforce,Xblocks );
      cudaDeviceSynchronize();
    }
#ifdef With_steps
    if(i>1000) printf("End of printing %d \n",i);
#endif

    error=cudaGetLastError();			// check the last error thrown by CUDA
    if(error!=cudaSuccess){				// if a function didnt return Success,  print info to screen and files and shut down programme
      printf("Exiting because of some error \n");
      //  			print_liquid_fraction<<<dimGrid,dimBlock>>>(device_liquid_fraction,device_sw,device_bubble_volume,device_s1_pos_y,Xblocks ,t,U0);
      // 			arrays_to_bubbles<<<dimGrid,dimBlock>>>(  device_s0_pos_x,  device_s0_pos_y,  device_s0_the_z,  device_s0_vel_x,  device_s0_vel_y,   device_s0_om_z,  device_s0_radius,   device_s0_mass,  device_s0_moment,  device_s0_cell_i, device_s0_cell_j,device_s0_fiber ,  device_s1_pos_x,  device_s1_pos_y,  device_s1_the_z,  device_s1_vel_x,  device_s1_vel_y,    device_s1_om_z,  device_s1_radius,   device_s1_mass,   device_s1_moment, device_s1_cell_i, device_s1_cell_j,device_s1_fiber, device_s2_pos_x,  device_s2_pos_y,    device_s2_the_z,  device_s2_vel_x,  device_s2_vel_y,   device_s2_om_z,  device_s2_radius,   device_s2_mass,  device_s2_moment, device_s2_cell_i,device_s2_cell_j,device_s2_fiber,  device_s0,  device_s1, device_s2, Xblocks);
      // 			arrays_to_beams<<<dimGBeams,dimBlock>>>(device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,device_b1_actif,device_b1,XBBeams,device_nbactive);
      // 			arrays_to_forces<<<dimGrid,dimBlock>>>(forces,torque,device_forces_x,device_forces_y,device_torque_z);
      // 			write_out(host_s0,host_s1,host_s2,host_b1,host_forces,host_torque,device_s0,device_s1,device_s2,device_b1,forces,torque,host_doforce,host_upforce,host_xforce,device_doforce,device_upforce,device_xforce,host_AL,host_PL,AL,PL,host_contact_list,device_contact_list,device_sw,host_sw,device_nbactive,host_nbactive);
      printf(" one of the kernels had an error %d so im exiting on iteration no %d \t:%s \n",error,i,cudaGetErrorString(cudaGetLastError()));
      // 			printf(" one of the kernels had an error %d so im exiting on iteration no %d\n",error,i);
      return(-1);
    }
    // 			#ifdef With_steps
    // 			if(i>40000&&i%10==0) printf("List update about to start... %d \t%.12f \t %d \n",i,device_b1[NGRAINS+NWALL-5].position.z,device_b1[NGRAINS+NWALL-5].cell.k);
    // #endif
    //------------------------------------------ List Update section -------------------------------------------//

    // start timings( if defined)
#ifdef With_Events
    cudaEventRecord(event_start,0);
#endif
#ifdef WITH_TIMING
    start_list_update=clock();
#endif
    // 		if(i>47000) printf("just before parallel %d \n",i);
    // 			cudaDeviceSynchronize();
    // check which bubbles are moving cells

    parallel_list_update<<<dimBlockListUpdate,1>>>(Xblocks,dimListUpdate,bubble_move_list,device_s1_cell_i,device_s1_cell_j,device_s2_pos_x,device_s2_pos_y,device_s2_cell_i,device_s2_cell_j,cell_list,device_mutex,LX,LY,i);
    //		SEAN_parallel_list_update<<<dimGridParallel,dimBlockParallel>>>(Xblocks,bubble_move_list,device_s1_cell_i,device_s1_cell_j,device_s2_pos_x,device_s2_pos_y,device_s2_cell_i,device_s2_cell_j,cell_list,device_mutex, LX,LY,i);
    // 			cudaDeviceSynchronize();
#ifdef With_steps
    if(i>1000) printf("just after parallel %d %d %d %d %d\n",i,cell_list,AL,PL,HoL,Xblocks);
#endif
    // update the cells that bubbles are moving out of
    cell_lists_out_update<<<dimCellGrid,dimBlockcell>>>(i,cell_list,AL,PL,HoL,Xblocks );
#ifdef With_steps
    if(i>1000) printf("cell lists %d \n",i);
#endif
    // update the cells that bubbles are moving into
    cell_lists_in_update<<<dimCellGrid,dimBlockcell>>>(i,cell_list,AL,PL,HoL,Xblocks );


    // serialupdate<<<1,1>>>(Xblocks,device_s1_cell_i,device_s1_cell_j,device_s1_cell_k,device_s2_pos_x,device_s2_pos_y,device_s2_pos_z,device_s2_cell_i,device_s2_cell_j,device_s2_cell_k, LX,LY,LZ,i,AL,PL,HoL);


#ifdef With_steps
    if(i>1000) printf("just before ifdef with_timing %d \n",i);
#endif
    // stop timing( if defined )
#ifdef WITH_TIMING
    cudaDeviceSynchronize();
#endif

#ifdef WITH_TIMING
    stop_list_update=clock();
    total_list_update+=stop_list_update-start_list_update;
#endif

#ifdef With_Events
    cudaEventRecord(finish,0);
    cudaEventSynchronize(finish);
    cudaEventElapsedTime(&elapsedTime,event_start,finish);
    list_update_event_time+=elapsedTime;
#endif

    //------------------------------------------------- Update Structures and reset forces
    // Start timing functions ( if defined)
#ifdef With_Events
    cudaEventRecord(event_start,0);
#endif
    //updates bubble arrays ( b0 goes to b1 and b1 goes to b2 ) and sets force components to zero.
    update_2bubbles_reset_forces<<<dimGridUpdate,dimBlockUpdate>>>(device_s0_pos_x,  device_s0_pos_y,  device_s0_the_z,  device_s0_vel_x,  device_s0_vel_y,    device_s0_om_z,  device_s0_radius,   device_s0_mass,  device_s0_moment,  device_s0_cell_i, device_s0_cell_j,device_s0_fiber,  device_s1_pos_x,  device_s1_pos_y,    device_s1_the_z,  device_s1_vel_x,  device_s1_vel_y,   device_s1_om_z,  device_s1_radius,   device_s1_mass,   device_s1_moment, device_s1_cell_i, device_s1_cell_j, device_s1_fiber, device_s2_pos_x,  device_s2_pos_y,   device_s2_the_z,  device_s2_vel_x,  device_s2_vel_y,  device_s2_om_z,  device_s2_radius,   device_s2_mass,  device_s2_moment, device_s2_cell_i,device_s2_cell_j, device_s2_fiber, device_forces_x,device_forces_y,device_torque_z,0,NGRAINS+NFIBERS+NWALL,Xblocks,device_nbactive);
    // stop timing functions( if defined)
    // 		 cudaDeviceSynchronize();
#ifdef With_Events
    cudaEventRecord(finish,0);
    cudaEventSynchronize(finish);
    cudaEventElapsedTime(&elapsedTime,event_start,finish);
    copy_event_time+=elapsedTime;
#endif
#ifdef With_steps
    if(i>1000) printf("Error about to start... %d \n",i);
#endif
    // ------------------------- Section to catch errors ----------------------------
    error=cudaGetLastError();					// if there was an error, print liquid fraction and data to files and stop the programme.
    if(error!=cudaSuccess){
      printf("Exiting because of some error \n");
      print_liquid_fraction<<<dimGrid,dimBlock>>>(device_liquid_fraction,device_sw,device_bubble_volume,device_s1_pos_y,Xblocks ,t,U0);
      arrays_to_bubbles<<<dimGrid,dimBlock>>>(  device_s0_pos_x,  device_s0_pos_y,   device_s0_the_z,  device_s0_vel_x,  device_s0_vel_y,  device_s0_om_z,  device_s0_radius,   device_s0_mass,  device_s0_moment,  device_s0_cell_i, device_s0_cell_j,device_s0_fiber,   device_s1_pos_x,  device_s1_pos_y, device_s1_the_z,  device_s1_vel_x,  device_s1_vel_y,  device_s1_om_z,  device_s1_radius,   device_s1_mass,   device_s1_moment, device_s1_cell_i, device_s1_cell_j, device_s1_fiber,  device_s2_pos_x,  device_s2_pos_y,   device_s2_the_z,  device_s2_vel_x,  device_s2_vel_y,  device_s2_om_z,  device_s2_radius,   device_s2_mass,  device_s2_moment, device_s2_cell_i,device_s2_cell_j, device_s2_fiber,  device_s0,  device_s1, device_s2, Xblocks);
      // 			arrays_to_beams<<<dimGBeams,dimBlock>>>(device_b1_j1,device_b1_j2,device_b1_longueur,device_b1_tx,device_b1_ty,device_b1_actif,device_b1, XBBeams,device_nbactive);
      arrays_to_forces<<<dimGrid,dimBlock>>>(forces,torque,device_forces_x,device_forces_y,device_torque_z);
      write_out(host_s0,host_s1,host_s2,host_b1,host_forces,host_torque,device_s0,device_s1,device_s2,device_b1,forces,torque,host_doforce,host_upforce,host_xforce,device_doforce,device_upforce,device_xforce,host_AL,host_PL,AL,PL,host_contact_list,device_contact_list,device_sw,host_sw,device_nbactive,host_nbactive);

#ifdef With_steps
      if(i>1000) printf("Error about to start... %d \n",i);
#endif
      printf(" one of the kernels had an error %d so im exiting on iteration %d \n",error,i);
      return(-1);
    }
  }

  //-------------------------------------------------------------------- Finish the main loop ------------------------------------//

  printf("The GPU has finished its job after %d iterations. Have a good day.\n",i);

  fclose(wallfile);
#ifdef long_time_check
  fclose(longf);
#endif
  return 0;
  }


  ///////////////////////////////////////////////////////////////////////// FINISHED THE MAIN CODE /////////////////////////////////////////////////////////////////////////

  /////////////////////////																									//////////////////////////////////////////
  ////////////////////////																									/////////////////////////////////////////






  ////////////////////////////////////////////////////////				 Kernel Definitions 				////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////// FIBER KERNELS ///////////////////////////////////////////////////////////////////////////////

  void __global__ fiber_forces(double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_radius,int* device_s1_fiber,unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty,unsigned int* device_b1_actif,double* forces_x_2,double* forces_y_2, double LX){
    double dx,dy,delta,truc;
    int idx=blockIdx.x*blockDim.x + threadIdx.x;
    int gid=NGRAINS+idx+1;										// global Id
    double ff_x,ff_y;
    vector n12,n10,t10,t12;
    double x0,y0,x1,x2,Z1,Z2,signe12,signe10,ps,angle,nforce,dd;
    double xp1,yp1,xp2,yp2,xm1,ym1,xm2,ym2;
    double txm1,tym1,tx,ty,txp1,typ1,txp2,typ2,SC0,SC1,SC2;

    if(gid <= NFIBERS+NGRAINS){

      x1=device_s1_pos_x[gid];
      Z1=device_s1_pos_y[gid];
      x2=device_s1_pos_x[gid+1];
      Z2=device_s1_pos_y[gid+1];

      dd=device_distance_array_abs_i_pared(x1,Z1,x2,Z2);
      n12=device_normal_vector_array_pared(x1,Z1,x2,Z2);

      ff_x=K_G*(dd-RM)*n12.x;
      ff_y=K_G*(dd-RM)*n12.z;

      x1=x1;
      Z1=Z1;

      if(gid<NFIBERS+NGRAINS){
        double_atomicAdd(&forces_x_2[gid],ff_x);
        double_atomicAdd(&forces_y_2[gid],ff_y);
        double_atomicAdd(&forces_x_2[gid+1],-ff_x);
        double_atomicAdd(&forces_y_2[gid+1],-ff_y);
      }



      xm1=device_s1_pos_x[gid-1];
      ym1=device_s1_pos_y[gid-1];
      xm2=device_s1_pos_x[gid-2];
      ym2=device_s1_pos_y[gid-2];
      xp1=device_s1_pos_x[gid+1];
      yp1=device_s1_pos_y[gid+1];
      // if(gid<=NBACTIVE0-2){
      xp2=device_s1_pos_x[gid+2];
      yp2=device_s1_pos_y[gid+2];
      // }
      // else{
      // xp2=device_s1_pos_x[device_b1_j2[gid]+1]/RM;
      // yp2=device_s1_pos_y[device_b1_j2[gid]+1]/RM;
      // }
      SC0=(device_XCP(xm1-xm2,LX)*device_XCP(x1-xm1,LX)+(ym1-ym2)*(Z1-ym1))/(RM*RM);
      SC1=(device_XCP(x1-xm1,LX)*device_XCP(xp1-x1,LX)+(Z1-ym1)*(yp1-Z1))/(RM*RM);
      SC2=(device_XCP(xp1-x1,LX)*device_XCP(xp2-xp1,LX)+(yp1-Z1)*(yp2-yp1))/(RM*RM);


      if(gid==NGRAINS+1){
        double_atomicAdd(&forces_x_2[gid],STIFFNESS/RM*(SC2*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)));
        double_atomicAdd(&forces_y_2[gid],STIFFNESS/RM*(SC2*(yp1-Z1)-(yp2-yp1)));
      }
      else if(gid==NGRAINS+2){
        double_atomicAdd(&forces_x_2[gid],STIFFNESS/RM*(-(1+SC1)*device_XCP(x1-xm1,LX)+(SC1+1+SC2)*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)));
        double_atomicAdd(&forces_y_2[gid],STIFFNESS/RM*(-(1+SC1)*(Z1-ym1)+(SC1+1+SC2)*(yp1-Z1)-(yp2-yp1)));
      }
      else if(gid==NGRAINS+NFIBERS-1)
      {
        double_atomicAdd(&forces_x_2[gid],STIFFNESS/RM*(device_XCP(xm1-xm2,LX)-(SC0+1+SC1)*device_XCP(x1-xm1,LX)+(SC1+1)*device_XCP(xp1-x1,LX)));
        double_atomicAdd(&forces_y_2[gid],STIFFNESS/RM*((ym1-ym2)-(SC0+1+SC1)*(Z1-ym1)+(SC1+1)*(yp1-Z1)));
      }
      else if(gid==NGRAINS+NFIBERS){
        double_atomicAdd(&forces_x_2[gid],STIFFNESS/RM*(device_XCP(xm1-xm2,LX)-SC0*device_XCP(x1-xm1,LX)));
        double_atomicAdd(&forces_y_2[gid],STIFFNESS/RM*((ym1-ym2)-SC0*(Z1-ym1)));
      }
      else {
        double_atomicAdd(&forces_x_2[gid],STIFFNESS/RM*(device_XCP(xm1-xm2,LX)-(SC0+1+SC1)*device_XCP(x1-xm1,LX)+(SC1+1+SC2)*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)));
        double_atomicAdd(&forces_y_2[gid],STIFFNESS/RM*((ym1-ym2)-(SC0+1+SC1)*(Z1-ym1)+(SC1+1+SC2)*(yp1-Z1)-(yp2-yp1)));
      }




    }
    return;
  }


  //////////////////////////////////////////////////////////////////////// BEAMS KERNELS ///////////////////////////////////////////////////////////////////////////////

  void __global__ beams_update(double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_radius,int* device_s1_fiber,unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur,double* device_b1_tx,double* device_b1_ty,unsigned int*
      device_b1_actif,double*
      forces_x_2,double* forces_y_2,int
      NBACTIVE0, int *device_nbactive){
    double dx,dy,delta,truc;
    int idx=blockIdx.x*blockDim.x + threadIdx.x;
    int gid=idx+1;										// global Id
    // double friction_x=0.,friction_y=0.,friction_z,VS=0.,vtx,vty,vtz,vsx,vsy,vsz,VT=0.;
    // double beta=0.*PI/180.;
    double ff_x,ff_y;
    vector n12,n10,t10,t12;
    double x0,y0,x1,x2,Z1,Z2,signe12,signe10,ps,angle,nforce,dd;
    double xp1,yp1,xp2,yp2,xm1,ym1,xm2,ym2;
    double txm1,tym1,tx,ty,txp1,typ1,txp2,typ2;

    if(gid < NBACTIVE0){
      if(device_b1_actif[gid]==1){

        x1=device_s1_pos_x[device_b1_j1[gid]];
        Z1=device_s1_pos_y[device_b1_j1[gid]];
        x2=device_s1_pos_x[device_b1_j2[gid]];
        Z2=device_s1_pos_y[device_b1_j2[gid]];
        dd=device_distance_array_abs_i_pared(x1,Z1,x2,Z2);


        // if(device_b1_longueur[gid]-(device_s1_radius[device_b1_j1[gid]]+device_s1_radius[device_b1_j2[gid]])>BRIDGE) device_b1_actif[gid]=0;
        // else {
        n12=device_normal_vector_array_pared(device_s1_pos_x[device_b1_j1[gid]],device_s1_pos_y[device_b1_j1[gid]],device_s1_pos_x[device_b1_j2[gid]],device_s1_pos_y[device_b1_j2[gid]]);
        ff_x=K_G*(dd-RM)*n12.x;
        ff_y=K_G*(dd-RM)*n12.z;

        x1=x1/RM;
        Z1=Z1/RM;

        double_atomicAdd(&forces_x_2[device_b1_j1[gid]],ff_x);
        double_atomicAdd(&forces_y_2[device_b1_j1[gid]],ff_y);
        double_atomicAdd(&forces_x_2[device_b1_j2[gid]],-ff_x);
        double_atomicAdd(&forces_y_2[device_b1_j2[gid]],-ff_y);



        xm1=device_s1_pos_x[device_b1_j1[gid]-1]/RM;
        ym1=device_s1_pos_y[device_b1_j1[gid]-1]/RM;
        xm2=device_s1_pos_x[device_b1_j1[gid]-2]/RM;
        ym2=device_s1_pos_y[device_b1_j1[gid]-2]/RM;
        xp1=device_s1_pos_x[device_b1_j1[gid]+1]/RM;
        yp1=device_s1_pos_y[device_b1_j1[gid]+1]/RM;
        if(gid<=NBACTIVE0-2){
          xp2=device_s1_pos_x[device_b1_j1[gid]+2]/RM;
          yp2=device_s1_pos_y[device_b1_j1[gid]+2]/RM;
        }
        else{
          xp2=device_s1_pos_x[device_b1_j2[gid]+1]/RM;
          yp2=device_s1_pos_y[device_b1_j2[gid]+1]/RM;
        }




        if(gid==1){
          double_atomicAdd(&forces_x_2[device_b1_j1[gid]],STIFFNESS*((1+(xp1-x1)*(xp2-xp1)+(yp1-Z1)*(yp2-yp1))*(xp1-x1)-(xp2-xp1)));
          double_atomicAdd(&forces_y_2[device_b1_j1[gid]],STIFFNESS*((1+(xp1-x1)*(xp2-xp1)+(yp1-Z1)*(yp2-yp1))*(yp1-Z1)-(yp2-yp1)));
        }
        else if(gid==2){
          double_atomicAdd(&forces_x_2[device_b1_j1[gid]],STIFFNESS*(-(1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(x1-xm1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1+(xp1-x1)*(xp2-xp1)+(yp1-Z1)*(yp2-yp1))*(xp1-x1)-(xp2-xp1)));
          double_atomicAdd(&forces_y_2[device_b1_j1[gid]],STIFFNESS*(-(1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(Z1-ym1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1+(xp1-x1)*(xp2-xp1)+(yp1-Z1)*(yp2-yp1))*(yp1-Z1)-(yp2-yp1)));
        }
        else if(gid==NBACTIVE0-1)
        {
          double_atomicAdd(&forces_x_2[device_b1_j1[gid]],STIFFNESS*((xm1-xm2)-((xm1-xm2)*(x1-xm1)+(ym1-ym2)*(Z1-ym1)+1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(x1-xm1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1)*(xp1-x1)));
          double_atomicAdd(&forces_x_2[device_b1_j2[gid]],-STIFFNESS*((xm1-xm2)-((xm1-xm2)*(x1-xm1)+(ym1-ym2)*(Z1-ym1)+1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(x1-xm1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1)*(xp1-x1)));
          double_atomicAdd(&forces_y_2[device_b1_j1[gid]],STIFFNESS*((ym1-ym2)-((xm1-xm2)*(x1-xm1)+(ym1-ym2)*(Z1-ym1)+1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(Z1-ym1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1)*(yp1-Z1)));
          double_atomicAdd(&forces_y_2[device_b1_j2[gid]],-STIFFNESS*((ym1-ym2)-((xm1-xm2)*(x1-xm1)+(ym1-ym2)*(Z1-ym1)+1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(Z1-ym1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1)*(yp1-Z1)));

        }
        else {
          double_atomicAdd(&forces_x_2[device_b1_j1[gid]],STIFFNESS*((xm1-xm2)-((xm1-xm2)*(x1-xm1)+(ym1-ym2)*(Z1-ym1)+1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(x1-xm1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1+(xp1-x1)*(xp2-xp1)+(yp1-Z1)*(yp2-yp1))*(xp1-x1)-(xp2-xp1)));
          double_atomicAdd(&forces_y_2[device_b1_j1[gid]],STIFFNESS*((ym1-ym2)-((xm1-xm2)*(x1-xm1)+(ym1-ym2)*(Z1-ym1)+1+(x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1))*(Z1-ym1)+((x1-xm1)*(xp1-x1)+(Z1-ym1)*(yp1-Z1)+1+(xp1-x1)*(xp2-xp1)+(yp1-Z1)*(yp2-yp1))*(yp1-Z1)-(yp2-yp1)));


        }


        //if(device_s1_fiber[device_b1_j1[gid]-1]==device_s1_fiber[device_b1_j1[gid]]) {
        /*
           xm1=device_s1_pos_x[device_b1_j1[gid]-1];
           ym1=device_s1_pos_y[device_b1_j1[gid]-1];
           xm2=device_s1_pos_x[device_b1_j1[gid]-2];
           ym2=device_s1_pos_y[device_b1_j1[gid]-2];
           xp1=device_s1_pos_x[device_b1_j1[gid]+1];
           yp1=device_s1_pos_y[device_b1_j1[gid]+1];
           xp2=device_s1_pos_x[device_b1_j1[gid]+2];
           yp2=device_s1_pos_y[device_b1_j1[gid]+2];

           txm1=xm1-xm2;
           tym1=ym1-ym2;
           tx1=x1-xm1;
           tZ1=Z1-ym1;
           txp1=xp1-x1;
           typ1=yp1-Z1;
           txp2=xp2-xp1;
           typ2=yp2-yp1;*/

        //x0=device_s1_pos_x[device_b1_j1[gid]-1];
        //y0=device_s1_pos_y[device_b1_j1[gid]-1];
        //n10=device_normal_vector_array_pared(x1,Z1,x0,y0);
        //t10.x=n10.z;
        //t10.z=-n10.x;
        //t12.x=n12.z;
        //t12.z=-n12.x;
        //if(t12.x*n10.x+t12.z*n10.z<0.) signe12=1.;
        //else signe12=-1.;
        //if(t10.x*n12.x+t10.z*n12.z<0.) signe10=1.;
        //else signe10=-1.;
        //ps=n12.x*n10.x+n12.z*n10.z;
        //if(ps<-1) {
        //     printf("C QUOI CE BORDEL %.12f %d \n",ps,j);
        //  ps=-1.;
        //}
        //angle=acos(ps);
        // Force sur le barreau j/j+1

        // nforce=-K_G;
        // double_atomicAdd(&forces_x_2[device_b1_j1[gid]],nforce*6*x1);
        // double_atomicAdd(&forces_y_2[device_b1_j1[gid]],nforce*6*Z1);
        //
        // if(device_s1_fiber[device_b1_j1[gid]-1]==device_s1_fiber[device_b1_j1[gid]]) {
        // double_atomicAdd(&forces_x_2[device_b1_j1[gid]],-nforce*4*xm1);
        // double_atomicAdd(&forces_y_2[device_b1_j1[gid]],-nforce*4*ym1);
        // }
        // if(device_s1_fiber[device_b1_j1[gid]-2]==device_s1_fiber[device_b1_j1[gid]]) {
        // double_atomicAdd(&forces_x_2[device_b1_j1[gid]],nforce*xm2);
        // double_atomicAdd(&forces_y_2[device_b1_j1[gid]],nforce*ym2);
        // }
        // if(device_s1_fiber[device_b1_j1[gid]+1]==device_s1_fiber[device_b1_j1[gid]]) {
        // double_atomicAdd(&forces_x_2[device_b1_j1[gid]],-nforce*4*xp1);
        // double_atomicAdd(&forces_y_2[device_b1_j1[gid]],-nforce*4*yp1);
        // }
        // if(device_s1_fiber[device_b1_j1[gid]+2]==device_s1_fiber[device_b1_j1[gid]]) {
        // double_atomicAdd(&forces_x_2[device_b1_j1[gid]],nforce*xp2);
        // double_atomicAdd(&forces_y_2[device_b1_j1[gid]],nforce*yp2);
        // }
        /*
           if(device_s1_fiber[device_b1_j1[gid]+2]!=device_s1_fiber[device_b1_j1[gid]]) {
           double_atomicAdd(&forces_x_2[device_b1_j2[gid]],nforce*(6*x1+xm2-4*xm1));
           double_atomicAdd(&forces_y_2[device_b1_j2[gid]],nforce*(6*Z1+ym2-4*ym1));
           }
         */


        /*
           nforce=signe12*(PI-angle)*K_G*R_G*RIGIDITY;
           double_atomicAdd(&forces_x_2[device_b1_j1[gid]],-nforce*n12.z);
           double_atomicAdd(&forces_y_2[device_b1_j1[gid]],nforce*n12.x);
           double_atomicAdd(&forces_x_2[device_b1_j2[gid]],nforce*n12.z);
           double_atomicAdd(&forces_y_2[device_b1_j2[gid]],-nforce*n12.x);

        // Force sur le barreau j-1/j
        nforce=signe10*(PI-angle)*K_G*R_G*RIGIDITY;
        double_atomicAdd(&forces_x_2[device_b1_j1[gid]],-nforce*n10.z);
        double_atomicAdd(&forces_y_2[device_b1_j1[gid]],nforce*n10.x);
        double_atomicAdd(&forces_x_2[device_b1_j1[gid]-1],nforce*n10.z);
        double_atomicAdd(&forces_y_2[device_b1_j1[gid]-1],-nforce*n10.x);

         */
        //}
        //    atomicAdd(device_nbactive,1);
        //     &NBACTIVE_double=2000.;
        // }
      }
    }

    return;
  }


  //////////////////////////////////////////////////////////////////////// FIBER KERNEL ///////////////////////////////////////////////////////////////////////////////
  void __global__ fibertension( double* device_s1_pos_x,double* device_s1_pos_y, double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_om_z,double* device_s1_radius,int* device_s1_fiber, double* forces_x_2,double* forces_y_2,  double* torque_z_2, contact_list* device_contact_list,int* device_contact_list_count) {

    int gid=blockIdx.x*blockDim.x + threadIdx.x;			// global Id
    int i,j,jv;
    double dd,nforce;
    vector n12,ff;

    if(gid>=0&&gid<NF){

      for(i=1;i<LFIBERS;i++){
        j=NGRAINS+gid*LFIBERS+i;
        jv=j+1;
        dd=device_distance_array_abs_i_pared(device_s1_pos_x[j],device_s1_pos_y[j],device_s1_pos_x[jv],device_s1_pos_y[jv]);
        nforce=K_G*(dd-RM);
        n12=device_normal_vector_array_pared(device_s1_pos_x[j],device_s1_pos_y[j],device_s1_pos_x[jv],device_s1_pos_y[jv]);
        ff.x=nforce*n12.x;
        ff.z=nforce*n12.z;
        double_atomicAdd(&forces_x_2[j],ff.x);
        //     forces_x[j]+=ff.x;
        //     double_atomicAdd(&forces_y[j],ff.x);
        //     forces_y[j]+=nforce*n12.z;
        //     forces_x[jv]+=-nforce*n12.x;
        //     forces_y[jv]+=-nforce*n12.z;
        //               device_collisionF(gid,j_nb,shared_s1_pos_x[lid],shared_s1_pos_y[lid],device_s1_vel_x,device_s1_vel_y,device_s1_om_z,shared_s1_radius[lid],J_nb_pos_x,J_nb_pos_y,J_nb_radius,forces_x,forces_y,torque_z,forces_x_2,forces_y_2,torque_z_2,device_s1_pos_y[NGRAINS+NFIBERS+NWALL]);

      }


    }
    return;
  }


  //////////////////////////////////////////////////////////////////////// CONTACTS KERNELS ///////////////////////////////////////////////////////////////////////////////


  ////  CONTACTS PART 1 WITH 1 LARGE GRID, RUNNING THREAD LEVEL PARALLELISM OF 9 THREADS PER BUBBLE, EACH THREAD CHECKING ONE OF THE 9 NEIGHBOURIING CELLS FOR EACH BUBBLE

  void  __global__ contacts_part1_TLP
#ifdef launch_bounds
    __launch_bounds__(new_part1_Max_threads,new_part1_min_blocks)
#endif
    (int* HoL,int* PL, double* device_s1_pos_x,double* device_s1_pos_y,  double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_radius, int* device_s1_cell_i, int* device_s1_cell_j, int i, int *err, contact_list* device_contact_list, int* device_bubble_mutex,int* device_contact_list_count){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;			// global Id
      int lid = threadIdx.x;									// local thread Id
      double overlap1;										// overlap between bubbles
      int strid=(gid-1)%9;									// string Id ( 9 strings per bubble, each bubble has 9 threads )
      int bubid=(int)((gid-1)/9)+1;							// bubble Id ( each bubble has 9 threads, so 9 threads have the same bubble Id)
      int iv,jv;											// cell ID (x,y,z co-ords) we check for contacts in
      int cellj;												// cell in the z direction
      int j_nb;												// bubble Id loaded from lists
      int j_nb2;												// second bubble Id loaded from lists ( used for prefetching)

      bool leaveloop=false;									// wheter or not to leave loop when we look for a lock ( initialised to false)
      int sidex,sidey,sidez;									// the neighbouring cell we check for contacts, it will be the current cell with either (-1,0,1) added in
      //	each of the cell dimensions

      sidex=(strid/9)-1;										// -1,0,1 for cell id in the x direction
      sidey=((strid%9)/3)-1;									// -1,0,1 for cell id in the y direction								// -1,0,1 for cell id in the z direction

      double J_nb_pos_x;										// Position of J_nb bubble in x,y and z dimesions
      double J_nb_pos_y;
      double J_nb_radius;										// radius of J_nb bubble

      __shared__ double shared_s1_pos_x[BLOCKSIZEXBULKNEW];	// Position and radius of the threads own bubble ( called per thread local bubble )
      __shared__ double shared_s1_pos_y[BLOCKSIZEXBULKNEW];
      __shared__ double shared_s1_radius[BLOCKSIZEXBULKNEW];



      if(bubid <= NGRAINS){
        shared_s1_pos_x[lid]=device_s1_pos_x[bubid];			// load per thread local bubble data into shared memory
        shared_s1_pos_y[lid]=device_s1_pos_y[bubid];
        shared_s1_radius[lid]=device_s1_radius[bubid];
        cellj=device_s1_cell_j[bubid];							// load cell k co-ordinate from global memory into a register

        if(cellj!=0 || (cellj ==0 && sidey!=-1)){				// dont check cell below bottom wall boundary
          if(cellj!=(NY-1) || (cellj==(NY-1) && sidey != 1)){	// dont check cell above top wall boundary ( should have const upper bound here)
            iv=device_CPI(device_s1_cell_i[bubid]+sidex);	// cell Id X,Y,Z co-ords to check for contacts ( use periodic boundary conditions on X and Y co-ords )
            jv=device_s1_cell_j[bubid]+sidey;
            // 		kv=cellk+sidez;


            // check Head of list to make sure cell not empty //
            if(HoL[iv*(NY+1)+jv]>0){

              j_nb=HoL[iv*(NY+1)+jv];	// load bubble ID from head of cell

              j_nb2=PL[j_nb];								// prefetch the next bubble ID on the list ( from child of current bubble ID)

              if(j_nb>bubid){								// Only check for collisions once ( without if statement would have thread thats processing J_nb's ID checking
                // the very same collision, it also gets rid of the problem of computing overlap with itself

                J_nb_pos_x=__ldg(&device_s1_pos_x[j_nb]);		// load J-nb bubble data in through texture cache
                J_nb_pos_y=__ldg(&device_s1_pos_y[j_nb]);
                J_nb_radius=__ldg(&device_s1_radius[j_nb]);

                overlap1=(shared_s1_radius[lid] + J_nb_radius)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x,J_nb_pos_y);
                // compute overlap between per thread local bubble and j_nb

                // check to see if something has gone wrong, if the overlap is too large there is likely a problem in the system. Can turn off this checking as the programme seems to
                // run correctly. This also can flag problems with the euler function step, or initialisation step, so is best left off
#ifdef WITH_HOUSTON
                //  					if(overlap1/shared_s1_radius[lid]>0.75) {
                //           				printf("HOUSTON WE HAVE A PROBLEM 1 overlap over radius is %G \nj buble number %8d at %4lf %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf %4lf with radius %4lf \nforces on %d are %lf %lf %lf overlap is %4lf,iteration %d \n\n",overlap1/device_s1_radius[bubid],bubid,device_s1_pos_x[bubid],device_s1_pos_y[bubid],device_s1_pos_z[bubid],device_s1_radius[bubid],     j_nb,device_s1_pos_x[j_nb],device_s1_pos_y[j_nb],device_s1_pos_z[j_nb],device_s1_radius[j_nb],bubid,forces[bubid].x,forces[bubid].z,forces[bubid].z,overlap1,i);
                // 					*err=7;
                //           			}
#endif

                // If overlap is positive, then we have a collision.
                if(overlap1>0.){
                  while (!leaveloop){						// leaveloop is initialisaed to false, can only exit the loop if we grab the lock and update contact lists.
                    // has to be done with lock inside this loop as thread divergence can cause problems if we used a classic
                    // spin lock when asking for the lock. Also we have to use atomic functions here to assure cache coherence
                    // we could also use a threadfence, but I think atomics are cheaper.

                    if(atomicCAS(&(device_bubble_mutex[bubid]),0,1)==0){		// get a lock on the bubble ID
                      atomicExch(&device_contact_list[bubid].contacts[device_contact_list_count[bubid]],j_nb); // Add J_nb to local Bubbles contact list
                      atomicAdd(&device_contact_list_count[bubid],1);			// add 1 to number of contacts
                      leaveloop=true;											// allow to leave loop
                      atomicExch(&(device_bubble_mutex[bubid]),0);			// release the lock
                    }
                  }

                  leaveloop=false;							// reset leave-loop boolean for next lock ask


                }
              }

              while (j_nb2>0){								// while there are still bubbles in the cell to check
                j_nb=j_nb2;									// prefetch next ID in cell list
                j_nb2=PL[j_nb];
                if(j_nb>bubid){


                  J_nb_pos_x=__ldg(&device_s1_pos_x[j_nb]);						// load j_nb bubble data through texture cache
                  J_nb_pos_y=__ldg(&device_s1_pos_y[j_nb]);
                  J_nb_radius=__ldg(&device_s1_radius[j_nb]);
                  // same check and carry on as before if there is a collision

                  overlap1=(shared_s1_radius[lid] + J_nb_radius)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x,J_nb_pos_y);


#ifdef WITH_HOUSTON
                  // 						if(overlap1/shared_s1_radius[lid]>0.75) {
                  //           				printf("HOUSTON WE HAVE A PROBLEM 2 overlap over radius is %G \nj buble number %8d at %4lf %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf %4lf with radius %4lf \nforces on %d are %lf %lf %lf overlap is %4lf,iteration %d \n\n",overlap1/device_s1_radius[bubid],bubid,device_s1_pos_x[bubid],device_s1_pos_y[bubid],device_s1_pos_z[bubid],device_s1_radius[bubid],     j_nb,device_s1_pos_x[j_nb],device_s1_pos_y[j_nb],device_s1_pos_z[j_nb],device_s1_radius[j_nb],bubid,forces[bubid].x,forces[bubid].z,forces[bubid].z,overlap1,i);
                  // 						*err=7;
                  //           				}
#endif
                  if(overlap1>0.){
                    while (!leaveloop){
                      if(atomicCAS(&(device_bubble_mutex[bubid]),0,1)==0){
                        atomicExch(&device_contact_list[bubid].contacts[device_contact_list_count[bubid]],j_nb);
                        atomicAdd(&device_contact_list_count[bubid],1);
                        leaveloop=true;
                        atomicExch(&(device_bubble_mutex[bubid]),0);
                      }
                    }

                    leaveloop=false;

                  }
                }

              }
            }
          }
        }
      }
    }




  // Alternate contacts part 1 function. This one only uses one thread per bulk bubble. However each thread checks three neighbouring cells for collisions simultaneously.
  // this is done by checking all neighbouring y cells (-1,0 1) for each value of X and Z cell at the same time. This increases instruction level parallelism and
  // increases performance. Since one thread is used per bulk bubble we dont have to use locks or atomics when we find overlaps. However the stopping conditions
  // for checking a cell now only stops if all y cells (-1,0,1) are empty not just one, this increases thread divergence. But increased ILP is more beneficial
  // than thread divergence detrimental.
  void  __global__ contacts_part1_ILP
#ifdef launch_bounds
    __launch_bounds__(no_streams_Max_threads,no_streams_min_blocks)
#endif
    ( int* HoL,int* PL, double* device_s1_pos_x,double* device_s1_pos_y,  double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_radius, int* device_s1_cell_i, int* device_s1_cell_j,int* device_s1_fiber, int i, int *err, contact_list* device_contact_list, int* device_bubble_mutex,int* device_contact_list_count){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;
      int lid = threadIdx.x;
      double overlap1;
      double overlap2;
      double overlap3;
      int jv,iv1,iv2,iv3;
      int cellj;

      int l;

      int count=0;

      int j_nb11;		// j_nb and prefetch for each y cell
      int j_nb12;
      int j_nb21;
      int j_nb22;
      int j_nb31;
      int j_nb32;

      int sidex,sidey;

      __shared__ double shared_s1_pos_x[BLOCKSIZEXBULKNOSTREAMS];
      __shared__ double shared_s1_pos_y[BLOCKSIZEXBULKNOSTREAMS];
      __shared__ double shared_s1_radius[BLOCKSIZEXBULKNOSTREAMS];


      double J_nb_pos_x1;					// j_nb bubble data for each y cell
      double J_nb_pos_Z1;
      double J_nb_radius1;
      double J_nb_pos_x2;
      double J_nb_pos_Z2;
      double J_nb_radius2;
      double J_nb_pos_x3;
      double J_nb_pos_y3;
      double J_nb_radius3;

      if(gid <= NGRAINS+NFIBERS){
        shared_s1_pos_x[lid]=device_s1_pos_x[gid];
        shared_s1_pos_y[lid]=device_s1_pos_y[gid];
        shared_s1_radius[lid]=device_s1_radius[gid];
        cellj=device_s1_cell_j[gid];

        for(sidey=-1;sidey<2;sidey++){
          if(cellj!=0 || (cellj ==0 && sidey!=-1)){
            if(cellj!=const_upper_bound || (cellj==const_upper_bound && sidey != 1)){
              jv=device_s1_cell_j[gid]+sidey;
              iv1=device_CPI(device_s1_cell_i[gid]-1);
              iv2=device_CPI(device_s1_cell_i[gid]);
              iv3=device_CPI(device_s1_cell_i[gid]+1);
              // 		kv=cellj+sidey;

              j_nb11=__ldg(&HoL[iv1*(NY+1)+jv]);			// load all three j_nb values
              j_nb21=__ldg(&HoL[iv2*(NY+1)+jv]);
              j_nb31=__ldg(&HoL[iv3*(NY+1)+jv]);

              // check Head of list to make sure cell not empty //
              if(j_nb11>0 | j_nb21>0 | j_nb31>0){						// have to check all three heads, if one is non-empty we continue

                j_nb12=__ldg(&PL[j_nb11]);
                j_nb22=__ldg(&PL[j_nb21]);
                j_nb32=__ldg(&PL[j_nb31]);
                if(j_nb11>gid&&(gid<=NGRAINS||(j_nb11>gid+1))){										// same as before, only check collisions once

                  J_nb_pos_x1=__ldg(&device_s1_pos_x[j_nb11]);
                  J_nb_pos_Z1=__ldg(&device_s1_pos_y[j_nb11]);
                  J_nb_radius1=__ldg(&device_s1_radius[j_nb11]);
                }
                if(j_nb21>gid&&(gid<=NGRAINS||(j_nb21>gid+1))){

                  J_nb_pos_x2=__ldg(&device_s1_pos_x[j_nb21]);
                  J_nb_pos_Z2=__ldg(&device_s1_pos_y[j_nb21]);
                  J_nb_radius2=__ldg(&device_s1_radius[j_nb21]);
                }
                if(j_nb31>gid&&(gid<=NGRAINS||(j_nb31>gid+1))){

                  J_nb_pos_x3=__ldg(&device_s1_pos_x[j_nb31]);
                  J_nb_pos_y3=__ldg(&device_s1_pos_y[j_nb31]);
                  J_nb_radius3=__ldg(&device_s1_radius[j_nb31]);
                }

                // check all overlaps
                if(j_nb11 > gid&&(gid<=NGRAINS||(j_nb11>gid+1))){
                  overlap1=(shared_s1_radius[lid] + J_nb_radius1)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x1,J_nb_pos_Z1);
                }
                if(j_nb21 > gid&&(gid<=NGRAINS||(j_nb21>gid+1))){
                  overlap2=(shared_s1_radius[lid] + J_nb_radius2)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x2,J_nb_pos_Z2);
                }
                if(j_nb31 > gid&&(gid<=NGRAINS||(j_nb31>gid+1))){
                  overlap3=(shared_s1_radius[lid] + J_nb_radius3)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x3,J_nb_pos_y3);
                }

                // If overlap is positive, then we have a collision. Dont need locks or atomics as only one thread per bubble ( so race conditions and thread coherence
                // arent a concern

                if(overlap1>0. && j_nb11 > gid&&(gid<=NGRAINS||(j_nb11>gid+1))){
                  device_contact_list[gid].contacts[count]=j_nb11;
                  count+=1;
                }
                if(overlap2>0. && j_nb21 > gid&&(gid<=NGRAINS||(j_nb21>gid+1))){
                  device_contact_list[gid].contacts[count]=j_nb21;
                  count+=1;
                }
                if(overlap3>0. && j_nb31 > gid&&(gid<=NGRAINS||(j_nb31>gid+1))){
                  device_contact_list[gid].contacts[count]=j_nb31;
                  count+=1;
                }



                while (j_nb12>0  | j_nb22>0 | j_nb32>0 ){			// while any of the cells are non-empty
                  j_nb11=j_nb12;
                  j_nb21=j_nb22;
                  j_nb31=j_nb32;
                  j_nb12=__ldg(&PL[j_nb12]);
                  j_nb22=__ldg(&PL[j_nb22]);
                  j_nb32=__ldg(&PL[j_nb32]);

                  if(j_nb11>gid&&(gid<=NGRAINS||(j_nb11>gid+1))){
                    J_nb_pos_x1=__ldg(&device_s1_pos_x[j_nb11]);
                    J_nb_pos_Z1=__ldg(&device_s1_pos_y[j_nb11]);
                    J_nb_radius1=__ldg(&device_s1_radius[j_nb11]);
                  }
                  if(j_nb21>gid&&(gid<=NGRAINS||(j_nb21>gid+1))){

                    J_nb_pos_x2=__ldg(&device_s1_pos_x[j_nb21]);
                    J_nb_pos_Z2=__ldg(&device_s1_pos_y[j_nb21]);
                    J_nb_radius2=__ldg(&device_s1_radius[j_nb21]);
                  }
                  if(j_nb31>gid&&(gid<=NGRAINS||(j_nb31>gid+1))){

                    J_nb_pos_x3=__ldg(&device_s1_pos_x[j_nb31]);
                    J_nb_pos_y3=__ldg(&device_s1_pos_y[j_nb31]);
                    J_nb_radius3=__ldg(&device_s1_radius[j_nb31]);
                  }
                  if(j_nb11 > gid&&(gid<=NGRAINS||(j_nb11>gid+1))){
                    overlap1=(shared_s1_radius[lid] + J_nb_radius1)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x1,J_nb_pos_Z1);
                    // if(i%1000==0&&gid==1383&&j_nb11>NGRAINS&&overlap1>0.) printf("OOUUUUUPS ! %d %d %f \n",gid,j_nb11,overlap1/R_G);
#ifdef WITH_HOUSTON

                    if(/*(gid==1383&&j_nb11>NGRAINS)||*/overlap1/shared_s1_radius[lid]>0.75) {
                      printf("HOUSTON WE HAVE A PROBLEM 1 overlap over radius is %f \n j buble number %8d at %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf  with radius %4lf \nforces on %d are  overlap is %4lf,iteration %d \n\n",overlap1/device_s1_radius[gid],gid,device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_radius[gid],     j_nb11,device_s1_pos_x[j_nb11],device_s1_pos_y[j_nb11],device_s1_radius[j_nb11],lid,overlap1,i);
                      *err=7;
                    }
#endif
                  }
                  if(j_nb21 > gid&&(gid<=NGRAINS||(j_nb21>gid+1))){
                    overlap2=(shared_s1_radius[lid] + J_nb_radius2)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x2,J_nb_pos_Z2);
                    // if(i%1000==0&&gid==1383&&j_nb21>NGRAINS&&overlap2>0.) printf("OOUUUUUPS ! %d %d %f \n",gid,j_nb21,overlap2/R_G);
#ifdef WITH_HOUSTON
                    if(/*(gid==1383&&j_nb21>NGRAINS)||*/overlap2/shared_s1_radius[lid]>0.75) {
                      printf("HOUSTON WE HAVE A PROBLEM 2 overlap over radius is %f \n j buble number %8d at %4lf %4lf  with radius %4lf \n j_nb bubble number %d at %4lf %4lf  with radius %4lf \nforces on %d are overlap is %4lf,iteration %d \n\n",overlap2/device_s1_radius[gid],gid,device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_radius[gid],     j_nb21,device_s1_pos_x[j_nb21],device_s1_pos_y[j_nb21],device_s1_radius[j_nb21],lid,overlap2,i);
                      *err=7;
                    }
#endif
                  }
                  if(j_nb31 > gid&&(gid<=NGRAINS||(j_nb31>gid+1))){
                    overlap3=(shared_s1_radius[lid] + J_nb_radius3)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x3,J_nb_pos_y3);
                    // if(i%1000==0&&gid==1383&&j_nb31>NGRAINS&&overlap3>0.) printf("OOUUUUUPS ! %d %d %f \n",gid,j_nb31,overlap3/R_G);
#ifdef With_HOUSTON
                    if(/*(gid==1383&&j_nb31>NGRAINS)||*/overlap3/shared_s1_radius[lid]>0.75) {
                      printf("HOUSTON WE HAVE A PROBLEM 3 overlap over radius is %G \n j buble number %8d at %4lf %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf %4lf with radius %4lf \nforces on %d are  overlap is %4lf,iteration %d \n\n",overlap3/device_s1_radius[lid],gid,device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_pos_z[gid],device_s1_radius[gid],     j_nb31,device_s1_pos_x[j_nb31],device_s1_pos_y[j_nb31],device_s1_pos_z[j_nb31],device_s1_radius[j_nb31],lid,overlap3,i);
                      *err=7;
                    }
#endif

                  }


                  // #ifdef WITH_HOUSTON
                  // 						if(overlap1/shared_s1_radius[lid]>0.75) {
                  //           				printf("HOUSTON WE HAVE A PROBLEM 1 overlap over radius is %f \n j buble number %8d at %4lf %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf %4lf with radius %4lf \nforces on %d are  overlap is %4lf,iteration %d \n\n",overlap1/device_s1_radius[gid],gid,device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_pos_z[gid],device_s1_radius[gid],     j_nb11,device_s1_pos_x[j_nb11],device_s1_pos_y[j_nb11],device_s1_pos_z[j_nb11],device_s1_radius[j_nb11],lid,overlap1,i);
                  // 						*err=7;
                  //           				}
                  //           										if(overlap2/shared_s1_radius[lid]>0.75) {
                  //           				printf("HOUSTON WE HAVE A PROBLEM 2 overlap over radius is %f \n j buble number %8d at %4lf %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf %4lf with radius %4lf \nforces on %d are overlap is %4lf,iteration %d \n\n",overlap2/device_s1_radius[gid],gid,device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_pos_z[gid],device_s1_radius[gid],     j_nb21,device_s1_pos_x[j_nb21],device_s1_pos_y[j_nb21],device_s1_pos_z[j_nb21],device_s1_radius[j_nb21],lid,overlap2,i);
                  // // 						*err=7;
                  //           				}
                  //           										if(overlap3/shared_s1_radius[lid]>0.75) {
                  // //           				printf("HOUSTON WE HAVE A PROBLEM 3 overlap over radius is %G \n j buble number %8d at %4lf %4lf %4lf with radius %4lf \n j_nb bubble number %d at %4lf %4lf %4lf with radius %4lf \nforces on %d are  overlap is %4lf,iteration %d \n\n",overlap3/device_s1_radius[lid],gid,device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_pos_z[gid],device_s1_radius[gid],     j_nb31,device_s1_pos_x[j_nb31],device_s1_pos_y[j_nb31],device_s1_pos_z[j_nb31],device_s1_radius[j_nb31],lid,overlap3,i);
                  // // 						*err=7;
                  //           				}
                  // #endif
                  if(overlap1>0. && j_nb11 > gid&&(gid<=NGRAINS||(j_nb11>gid+1)) ){
                    device_contact_list[gid].contacts[count]=j_nb11;
                    count+=1;
                  }
                  if(overlap2>0. && j_nb21 > gid&&(gid<=NGRAINS||(j_nb21>gid+1))){
                    device_contact_list[gid].contacts[count]=j_nb21;
                    count+=1;
                  }
                  if(overlap3>0. && j_nb31 > gid&&(gid<=NGRAINS||(j_nb31>gid+1))){
                    device_contact_list[gid].contacts[count]=j_nb31;
                    count+=1;
                  }


                }
              }
            }
          }
        }

        device_contact_list_count[gid]=count;						// only need to update global memory number of collisions at the end.
      }
    }


  //------------------ Contacts Part 2 functions ------------------//



  // this function computes the forces between bubbles and bubbles on their collsion lists. It runs 9 threads per bubble, as we have defined a maximum number
  // contacts per bubble to be less than 9 ( have found to be about 16). It computes all possible collisions, and hence has a lot of inactive threads.
  // It is only really effective for small system sizes.
  void  __global__
#ifdef launch_bounds
    __launch_bounds__(part2_Max_threads,part2_min_blocks)		// this function is very register heavy, can use launch bounds to restrict register usage and increase occupancy
    // however I normally restrict register usage in the compiling stage as this is the only kernel that
    // really benefits from being restricted, and no other kernel. No other kernel uses as many registers so
    // the compile time restrict only restricts this kernel( 80 registers per thread is the current limit )
#endif
    contacts_part2_TLP(double* device_s1_pos_x,double* device_s1_pos_y, double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_radius, double* forces_x,double* forces_y, double* torque_z, contact_list* device_contact_list,int* device_contact_list_count){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;				// global Id
      int lid = threadIdx.x;										// local Id
      int bubid=(int)((gid-1)/9)+1;								// bubble Id ( each bubble has 9 threads)
      int strid=(gid-1)%9;										// String Id ( there are 9 threads per bubble)
      double J_nb_pos_x;											// Bubbule position and radius for j_nb ( bubble Id per local thread bubble computes collision with)
      double J_nb_pos_y;
      double J_nb_radius;

      __shared__ double shared_s1_pos_x[BLOCKSIZEXBULKPART2];		// per thread local bubble data is stored in shared memory
      __shared__ double shared_s1_pos_y[BLOCKSIZEXBULKPART2];
      __shared__ double shared_s1_radius[BLOCKSIZEXBULKPART2];

      double overlap1;											// compute the overlap of bubbles ( cheaper to recompute then to loadit in from Contacts part 1 kernel)
      int j_nb;
      if(bubid <= NGRAINS){										// only operate on bulk bubbles

        device_contact_list_count[gid]=0;					// set contact list count = 0
        //		shared_s1_pos_x[lid]=device_s1_pos_x[bubid];
        //		shared_s1_pos_y[lid]=device_s1_pos_y[bubid];
        //		shared_s1_pos_z[lid]=device_s1_pos_z[bubid];
        //		shared_s1_radius[lid]=device_s1_radius[bubid];
        shared_s1_pos_x[lid]=__ldg(&device_s1_pos_x[bubid]);	// load per thread Local bubble data into shared memory through texture cache
        shared_s1_pos_y[lid]=__ldg(&device_s1_pos_y[bubid]);
        shared_s1_radius[lid]=__ldg(&device_s1_radius[bubid]);
        j_nb=device_contact_list[bubid].contacts[strid];	// find the bubble Id to compute collision with, this is the string Id, each bubble computes collision with
        // all 9 possible contacts
        device_contact_list[bubid].contacts[strid]=0;		// reset bubble contact list
        if(j_nb> 0){										// if it is a real collision ( if the string ID doesnt corresond to a real contact, it reads a zero)

          J_nb_pos_x=__ldg(&device_s1_pos_x[j_nb]);					// load contact bubbles data through the texture cache
          J_nb_pos_y=__ldg(&device_s1_pos_y[j_nb]);
          J_nb_radius=__ldg(&device_s1_radius[j_nb]);

          overlap1=(shared_s1_radius[lid] + J_nb_radius)-device_distance_array_abs_i_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],J_nb_pos_x,J_nb_pos_y);											// compute the overlap

          // compute the forces that arise from the collision.
          device_collision_no0_array_streams_no_vectors_no_Forces_no_structs_half_pared(shared_s1_pos_x[lid],shared_s1_pos_y[lid],device_s1_vel_x[bubid],device_s1_vel_y[bubid],shared_s1_radius[lid],J_nb_pos_x,J_nb_pos_y,device_s1_vel_x[j_nb],device_s1_vel_y[j_nb],J_nb_radius,overlap1, &forces_x[bubid],&forces_y[bubid],&torque_z[bubid],&forces_x[j_nb],&forces_y[j_nb],&torque_z[j_nb]);
        }
      }
    }

  // This is anther Contacts Part 2 kernel. It only uses one thread per bulk bubble ( no string IDs). It is more efficient at larger system sizes (rouglhy >4000 bulk bubbles)
  // It computes 2 collisions per iteration though. This increased ILP and lead to more efficient programme.
  void  __global__ contacts_part2_ILP( double* device_s1_pos_x,double* device_s1_pos_y, double* device_s1_vel_x,double* device_s1_vel_y, double* device_s1_om_z,double* device_s1_radius, double* forces_x,double* forces_y, double* torque_z, contact_list* device_contact_list,double* forces_x_2,double* forces_y_2,double* torque_z_2,int* device_contact_list_count, double LX){

    int gid=blockIdx.x*blockDim.x + threadIdx.x+1;
    int lid = threadIdx.x;

    __shared__ double shared_s1_pos_x[BLOCKSIZEXBULKPARED];
    __shared__ double shared_s1_pos_y[BLOCKSIZEXBULKPARED];
    __shared__ double shared_s1_radius[BLOCKSIZEXBULKPARED];

    double J_nb_pos_x;
    double J_nb_pos_y;
    double J_nb_om_z;
    double J_nb_radius;
    double J_nb_pos_x2;				// two collisions per iteration
    double J_nb_pos_Z2;
    double J_nb_om_z2;
    double J_nb_radius2;
    double ff_x,ff_y;
    vector n12,n10,t10,t12;
    double x0,y0,x1,x2,Z1,Z2,signe12,signe10,ps,angle,nforce,dd,dd0,dd2;
    double xp1,yp1,xp2,yp2,xm1,ym1,xm2,ym2;
    double txm1,tym1,tx,ty,txp1,typ1,txp2,typ2,SC0,SC1,SC2,F1,F2;

    int j_nb;
    int j_nb2;
    int count;
    int i;
    if(gid <= NGRAINS+NFIBERS){

      count = device_contact_list_count[gid];		// read in the number of contacts for the per thread local bubble
      if(count > 0){
        shared_s1_pos_x[lid]=device_s1_pos_x[gid];	// load in per thread local bubble data
        shared_s1_pos_y[lid]=device_s1_pos_y[gid];
        shared_s1_radius[lid]=device_s1_radius[gid];
        device_contact_list_count[gid]=0;
        //	#pragma unroll 4  //didnt help at all putting this in
        for(i=0;i<(count-1);i+=2){				// compute two collisions at the same time
          j_nb=device_contact_list[gid].contacts[i];
          j_nb2=device_contact_list[gid].contacts[i+1];
          J_nb_pos_x=__ldg(&device_s1_pos_x[j_nb]);
          J_nb_pos_y=__ldg(&device_s1_pos_y[j_nb]);
          J_nb_om_z=__ldg(&device_s1_om_z[j_nb]);
          J_nb_radius=__ldg(&device_s1_radius[j_nb]);
          J_nb_pos_x2=__ldg(&device_s1_pos_x[j_nb2]);
          J_nb_pos_Z2=__ldg(&device_s1_pos_y[j_nb2]);

          J_nb_om_z2=__ldg(&device_s1_om_z[j_nb2]);
          J_nb_radius2=__ldg(&device_s1_radius[j_nb2]);

          device_contact_list[gid].contacts[i]=0;
          device_contact_list[gid].contacts[i+1]=0;

#ifdef With_steps
          if(gid==1000) printf("collision2 is about to start...\n");
#endif

          device_collision2(gid,j_nb,j_nb2,shared_s1_pos_x[lid],shared_s1_pos_y[lid],device_s1_vel_x,device_s1_vel_y,device_s1_om_z,shared_s1_radius[lid],J_nb_pos_x,J_nb_pos_y,J_nb_om_z,J_nb_radius,J_nb_pos_x2,J_nb_pos_Z2,J_nb_om_z2,J_nb_radius2,forces_x,forces_y,torque_z,forces_x_2,forces_y_2,torque_z_2,device_s1_pos_y[NGRAINS+NFIBERS+NWALL]);
        }
        if(i==(count-1)){	// if only one contact left compute just one collision
          j_nb=device_contact_list[gid].contacts[i];
          J_nb_pos_x=__ldg(&device_s1_pos_x[j_nb]);
          J_nb_pos_y=__ldg(&device_s1_pos_y[j_nb]);
          J_nb_om_z=__ldg(&device_s1_om_z[j_nb]);
          J_nb_radius=__ldg(&device_s1_radius[j_nb]);
          device_contact_list[gid].contacts[i]=0;
          device_collision1(gid,j_nb,shared_s1_pos_x[lid],shared_s1_pos_y[lid],device_s1_vel_x,device_s1_vel_y,device_s1_om_z,shared_s1_radius[lid],J_nb_pos_x,J_nb_pos_y,J_nb_radius,forces_x,forces_y,torque_z,forces_x_2,forces_y_2,torque_z_2,device_s1_pos_y[NGRAINS+NFIBERS+NWALL]);
        }
      }
    }

    if(gid>NGRAINS&&gid <= NGRAINS+NFIBERS){

      x1=device_s1_pos_x[gid];
      Z1=device_s1_pos_y[gid];
      xp1=device_s1_pos_x[gid+1];
      yp1=device_s1_pos_y[gid+1];

      dd=device_distance_array_abs_i_pared(x1,Z1,xp1,yp1);
      n12=device_normal_vector_array_pared(x1,Z1,xp1,yp1);

      ff_x=K_G*(dd-RM)*n12.x;
      ff_y=K_G*(dd-RM)*n12.z;


      if(gid<NFIBERS+NGRAINS){
        double_atomicAdd(&forces_x_2[gid],ff_x);
        double_atomicAdd(&forces_y_2[gid],ff_y);
        //     forces_x[gid]+=ff_x;
        //     forces_y[gid]+=ff_y;
        double_atomicAdd(&forces_x_2[gid+1],-ff_x);
        double_atomicAdd(&forces_y_2[gid+1],-ff_y);
      }

      xm1=device_s1_pos_x[gid-1];
      ym1=device_s1_pos_y[gid-1];
      xm2=device_s1_pos_x[gid-2];
      ym2=device_s1_pos_y[gid-2];
      // xp1=device_s1_pos_x[gid+1];
      // yp1=device_s1_pos_y[gid+1];
      // if(gid<=NBACTIVE0-2){
      xp2=device_s1_pos_x[gid+2];
      yp2=device_s1_pos_y[gid+2];


      dd0=device_distance_array_abs_i_pared(x1,Z1,xm1,ym1);
      //     dd2=device_distance_array_abs_i_pared(x1,Z1,xp1,yp1);

      SC0=(device_XCP(xm1-xm2,LX)*device_XCP(x1-xm1,LX)+(ym1-ym2)*(Z1-ym1))/(RM*RM);
      SC1=(device_XCP(x1-xm1,LX)*device_XCP(xp1-x1,LX)+(Z1-ym1)*(yp1-Z1))/(RM*RM);
      SC2=(device_XCP(xp1-x1,LX)*device_XCP(xp2-xp1,LX)+(yp1-Z1)*(yp2-yp1))/(RM*RM);

      F1=SC0/dd0+1./dd+SC1/dd0;
      F2=SC1/dd+1./dd0+SC2/dd;
      //
      //
      if(gid==NGRAINS+1){
        //  double_atomicAdd(&forces_x_2[gid],STIFFNESS*(SC2*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)));
        //  double_atomicAdd(&forces_y_2[gid],STIFFNESS*(SC2*(yp1-Z1)-(yp2-yp1)));
        forces_x[gid]+=STIFFNESS*(SC2/dd*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)/dd);
        forces_y[gid]+=STIFFNESS*(SC2/dd*(yp1-Z1)-(yp2-yp1)/dd);
      }
      else if(gid==NGRAINS+2){
        //  double_atomicAdd(&forces_x_2[gid],STIFFNESS*(-(1.+SC1)*device_XCP(x1-xm1,LX)+(SC1+1.+SC2)*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)));
        //  double_atomicAdd(&forces_y_2[gid],STIFFNESS*(-(1.+SC1)*(Z1-ym1)+(SC1+1.+SC2)*(yp1-Z1)-(yp2-yp1)));
        forces_x[gid]+=STIFFNESS*(-(1./dd+SC1/dd0)*device_XCP(x1-xm1,LX)+F2*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)/dd);
        forces_y[gid]+=STIFFNESS*(-(1./dd+SC1/dd0)*(Z1-ym1)+F2*(yp1-Z1)-(yp2-yp1)/dd);

      }
      else if(gid==NGRAINS+NFIBERS-1){
        //  double_atomicAdd(&forces_x_2[gid],STIFFNESS*(device_XCP(xm1-xm2,LX)-(SC0+1.+SC1)*device_XCP(x1-xm1,LX)+(SC1+1.)*device_XCP(xp1-x1,LX)));
        //  double_atomicAdd(&forces_y_2[gid],STIFFNESS*((ym1-ym2)-(SC0+1.+SC1)*(Z1-ym1)+(SC1+1.)*(yp1-Z1)));
        forces_x[gid]+=STIFFNESS*(device_XCP(xm1-xm2,LX)/dd0-F1*device_XCP(x1-xm1,LX)+(SC1/dd+1./dd0)*device_XCP(xp1-x1,LX));
        forces_y[gid]+=STIFFNESS*((ym1-ym2)/dd0-F1*(Z1-ym1)+(SC1/dd+1./dd0)*(yp1-Z1));
      }
      else if(gid==NGRAINS+NFIBERS){
        //  double_atomicAdd(&forces_x_2[gid],STIFFNESS*(device_XCP(xm1-xm2,LX)-SC0*device_XCP(x1-xm1,LX)));
        //  double_atomicAdd(&forces_y_2[gid],STIFFNESS*((ym1-ym2)-SC0*(Z1-ym1)));
        forces_x[gid]+=STIFFNESS*(device_XCP(xm1-xm2,LX)/dd0-SC0/dd0*device_XCP(x1-xm1,LX));
        forces_y[gid]+=STIFFNESS*((ym1-ym2)/dd0-SC0/dd0*(Z1-ym1));
      }
      else {
        // //    double_atomicAdd(&forces_x_2[gid],STIFFNESS*(device_XCP(xm1-xm2,LX)-(SC0+1.+SC1)*device_XCP(x1-xm1,LX)+(SC1+1.+SC2)*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)));
        //    double_atomicAdd(&forces_y_2[gid],3.14);//STIFFNESS*((ym1-ym2)-(SC0+1.+SC1)*(Z1-ym1)+(SC1+1.+SC2)*(yp1-Z1)-(yp2-yp1)));
        forces_x[gid]+=STIFFNESS*(device_XCP(xm1-xm2,LX)/dd0-F1*device_XCP(x1-xm1,LX)+F2*device_XCP(xp1-x1,LX)-device_XCP(xp2-xp1,LX)/dd);
        forces_y[gid]+=STIFFNESS*((ym1-ym2)/dd0-F1*(Z1-ym1)+F2*(yp1-Z1)-(yp2-yp1)/dd);
      }




    }

    //Fin de la routine part2_ILP
    }


    // This is Contacts Part 3 kernel. It is only used with the above contacrts part 2 kernel ( the one with one thread per bubble ). It sums up tow force arrays we used
    // in the collision function, one was written to with expensive atomic adds, and another which was written with non-coherent adds.

    void __global__ contacts_part3_ILP(double* forces_x,double* forces_y,double* torque_z,double* forces_x_2,double* forces_y_2,double* torque_z_2){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;
      if(gid <=NGRAINS+NFIBERS+NWALL){
        forces_x[gid]+=forces_x_2[gid];								// add the forces from the arrays which were written to with atomic adds to the main force arrays
        forces_x_2[gid]=0.;
        forces_y[gid]+=forces_y_2[gid];
        forces_y_2[gid]=0.;

        // torque_z[gid]+=torque_z_2[gid];
        // torque_z_2[gid]=0.;

      }
      // if(gid>NGRAINS+NFIBERS&&gid<=NGRAINS+NWALL){
      //   forces_x[gid]=0.2;
      // }
    }


    //--------------------------------------------- The Collision Functions, called from inside a contacts kernel. These do the actual computations of the forces ----------
    // This function computes forces between two bubbles ( actually two pairs of two bubbles for increased ILP ). It takes in all neccessary bubble data and the force arrays to write out to
    void __device__ device_collision2 (int a, int b, int c,double sA_pos_x, double sA_pos_y, double* sA_vel_x, double* sA_vel_y,double* sA_om_z,double sA_radius,double sB_pos_x, double sB_pos_y,  double sB_om_z,double sB_radius,double sC_pos_x,double sC_pos_y, double sC_om_z,double sC_radius, double* forcesa_x,double* forcesa_y, double* torquea_z,double* forces_x_2,double* forces_y_2,double* torque_z_2,double device_s1_pos_wall){

      vector n12;								// the normal vector betwenn a and b,
      double VRN;								// the norm of the velocity vector between a and b
      vector n12C;								// the same but between a and c
      double VRNC;
      double repulsive_force_x,repulsive_force_y,repulsive_force_xC,repulsive_force_yC;					// replusive force between a and b
      double friction_x,friction_y,friction_xC,friction_yC,couple_z,couple_zC;							// friction force between a and b
      double vr_x,vt_x,vs_x;								// relative velocity between a and b
      double vr_y,vt_y,vs_y;
      double vr_xC,vt_xC,vs_xC;								// relative velocity between a and c
      double vr_yC,vt_yC,vs_yC;
      double delta;								// overlap between a and b
      double deltaC;								// overlap between a and c
      double coeffe,coeffeC,VRD,VRDC,coefft,coefftC,VRT,VRTC,F_n,F_nC,coulomb,coulombC,VS,VSC;
      double romaz,romazC;

      delta=(sA_radius + sB_radius) - device_distance_array_abs_i_pared(sA_pos_x,sA_pos_y,sB_pos_x,sB_pos_y);		// compute overlap between a and b, a and c
      deltaC=(sA_radius + sC_radius) - device_distance_array_abs_i_pared(sA_pos_x,sA_pos_y,sC_pos_x,sC_pos_y);

      n12=device_normal_vector_array_pared(sA_pos_x,sA_pos_y,sB_pos_x,sB_pos_y);				// normal vector from a to b
      n12C=device_normal_vector_array_pared(sA_pos_x,sA_pos_y,sC_pos_x,sC_pos_y);				// normal vector from a to c

      vr_x=sA_vel_x[a]-sA_vel_x[b];																				// relative velocity between a and b
      vr_y=sA_vel_y[a]-sA_vel_y[b];

      vr_xC=sA_vel_x[a]-sA_vel_x[c];																				// relative velocity between a and c
      vr_yC=sA_vel_y[a]-sA_vel_y[c];

      VRN=(vr_x*n12.x+vr_y*n12.z);																	// norm of relative velocities
      VRNC=(vr_xC*n12C.x+vr_yC*n12C.z);


      repulsive_force_x=-K_G*2*R_G/(sA_radius+sB_radius)*delta*n12.x;			// function for the repulsive force between two bubbles
      repulsive_force_y=-K_G*2*R_G/(sA_radius+sB_radius)*delta*n12.z;
      repulsive_force_xC=-K_G*2*R_G/(sA_radius+sC_radius)*deltaC*n12C.x;
      repulsive_force_yC=-K_G*2*R_G/(sA_radius+sC_radius)*deltaC*n12C.z;

      forcesa_x[a]+=(repulsive_force_x+repulsive_force_xC);						// add repulsive force to the force array. We can add to A's force array non-coherently
      // as only one thread acceses it.
      double_atomicAdd(&forces_x_2[b],-(repulsive_force_x));					// we add forces on B and C to second force arrays with atomics as many threads can access
      // these arrays simultaneously. Splitting the array like this minimise expensive atomic adds
      double_atomicAdd(&forces_x_2[c],-(repulsive_force_xC));
      forcesa_y[a]+=(repulsive_force_y+repulsive_force_yC);
      double_atomicAdd(&forces_y_2[b],-(repulsive_force_y));
      double_atomicAdd(&forces_y_2[c],-(repulsive_force_yC));



      //  friction_x=-friction_coeff*pow(VRN,ALPHA-1.)*pow(R_G/device_s1_pos_wall,1.-ALPHA)*vr_x;	// function for friction force
      //  friction_y=-friction_coeff*pow(VRN,ALPHA-1.)*pow(R_G/device_s1_pos_wall,1.-ALPHA)*vr_y;
      //  friction_xC=-friction_coeff*pow(VRNC,ALPHA-1.)*pow(R_G/device_s1_pos_wall,1.-ALPHA)*vr_xC;
      //  friction_yC=-friction_coeff*pow(VRNC,ALPHA-1.)*pow(R_G/device_s1_pos_wall,1.-ALPHA)*vr_yC;

      friction_x=-friction_coeff*vr_x;	// function for friction force
      friction_y=-friction_coeff*vr_y;
      friction_xC=-friction_coeff*vr_xC;
      friction_yC=-friction_coeff*vr_yC;

      if(sA_radius > sB_radius){													// friction force also includes a minimum
        friction_x=friction_x*sB_radius/R_G;
        friction_y=friction_y*sB_radius/R_G;
      }

      else{
        friction_x=friction_x*sA_radius/R_G;
        friction_y=friction_y*sA_radius/R_G;
      }
      if(sA_radius > sC_radius){
        friction_xC=friction_xC*sC_radius/R_G;
        friction_yC=friction_yC*sC_radius/R_G;
      }

      else{
        friction_xC=friction_xC*sA_radius/R_G;
        friction_yC=friction_yC*sA_radius/R_G;
      }

      forcesa_x[a]+=(friction_x+friction_xC);									// add friction forces to force arrays and friction force arrays. Do this in the same
      // manner as before, A's added simply, B and C added to second array with atomics.
      double_atomicAdd(&forces_x_2[b],-(friction_x));
      double_atomicAdd(&forces_x_2[c],-(friction_xC));

      forcesa_y[a]+=(friction_y+friction_yC);
      double_atomicAdd(&forces_y_2[b],-(friction_y));
      double_atomicAdd(&forces_y_2[c],-(friction_yC));




      return;
    }

    // exact same function as above, but we only compute one collision ( No C Bubble)
    void __device__ device_collision1(int a, int b,double sA_pos_x, double sA_pos_y,  double* sA_vel_x, double* sA_vel_y, double* sA_om_z,double sA_radius,double sB_pos_x, double sB_pos_y,  double sB_radius, double* forcesa_x,double* forcesa_y, double* torquea_z,double* forces_x_2,double* forces_y_2,double* torque_z_2,double device_s1_pos_wall){

      vector n12;
      double VRN;
      double repulsive_force_x;
      double repulsive_force_y;
      double friction_x;
      double friction_y;
      double couple_z;
      double vr_x,vt_x,vs_x;
      double vr_y,vt_y,vs_y;
      double delta,F_n;
      double coeffe,VRD,VRT,coefft,coulomb,VS,romaz;

      delta=(sA_radius + sB_radius) - device_distance_array_abs_i_pared(sA_pos_x,sA_pos_y,sB_pos_x,sB_pos_y);

      n12=device_normal_vector_array_pared(sA_pos_x,sA_pos_y,sB_pos_x,sB_pos_y);

      vr_x=sA_vel_x[a]-sA_vel_x[b];
      vr_y=sA_vel_y[a]-sA_vel_y[b];

      VRN=(vr_x*n12.x+vr_y*n12.z);																	// norm of relative velocities
      repulsive_force_x=-K_G*2*R_G/(sA_radius+sB_radius)*delta*n12.x;
      repulsive_force_y=-K_G*2*R_G/(sA_radius+sB_radius)*delta*n12.z;

      forcesa_x[a]+=repulsive_force_x;
      double_atomicAdd(&forces_x_2[b],-repulsive_force_x);
      forcesa_y[a]+=repulsive_force_y;
      double_atomicAdd(&forces_y_2[b],-repulsive_force_y);


      friction_x=-friction_coeff*vr_x;
      friction_y=-friction_coeff*vr_y;

      if(sA_radius > sB_radius){
        friction_x=friction_x*sB_radius/R_G;
        friction_y=friction_y*sB_radius/R_G;
      }

      else{
        friction_x=friction_x*sA_radius/R_G;
        friction_y=friction_y*sA_radius/R_G;
      }

      forcesa_x[a]+=friction_x;
      double_atomicAdd(&forces_x_2[b],-friction_x);

      forcesa_y[a]+=friction_y;
      double_atomicAdd(&forces_y_2[b],-friction_y);



      return;
    }

    // Similar to previous collisino functions. Here overlap is computed in the contacts kernel and loaded into the collsion function. Also this is used with the contacts
    // kernel that has multiple threads per bulk bubble. This means that even forces on A have to be added with Atomics ( as multiple threads are writing to them ).
    // This gets rid of the need for a second force array and all forces are written directly to the main force array ( with atomics).
    void __device__ device_collision_no0_array_streams_no_vectors_no_Forces_no_structs_half_pared (double sA_pos_x, double sA_pos_y, double sA_vel_x, double sA_vel_y, double sA_radius,double sB_pos_x, double sB_pos_y, double sB_vel_x,double sB_vel_y,double sB_radius, double delta , double* forcesa_x,double* forcesa_y, double* torquea_z,double* forcesb_x,double* forcesb_y, double* torqueb_z){

      vector n12;
      double VRN;
      double repulsive_force_x;
      double repulsive_force_y;
      double friction_x;
      double friction_y;
      double vr_x;
      double vr_y;

      n12=device_normal_vector_array_pared(sA_pos_x,sA_pos_y,sB_pos_x,sB_pos_y);

      vr_x=sA_vel_x-sB_vel_x;
      vr_y=sA_vel_y-sB_vel_y;

      VRN=sqrt(vr_x*vr_x+vr_y*vr_y);

      repulsive_force_x=-K_G*2*R_G/(sA_radius+sB_radius)*delta*n12.x;
      repulsive_force_y=-K_G*2*R_G/(sA_radius+sB_radius)*delta*n12.z;


      double_atomicAdd(forcesa_x,repulsive_force_x);
      double_atomicAdd(forcesb_x,-repulsive_force_x);
      double_atomicAdd(forcesa_y,repulsive_force_y);
      double_atomicAdd(forcesb_y,-repulsive_force_y);


      friction_x=-friction_coeff*vr_x;
      friction_y=-friction_coeff*vr_y;

      if(sA_radius > sB_radius){
        friction_x=friction_x*sB_radius/R_G;
        friction_y=friction_y*sB_radius/R_G;
      }

      else{
        friction_x=friction_x*sA_radius/R_G;
        friction_y=friction_y*sA_radius/R_G;
      }

      double_atomicAdd(forcesa_x,friction_x);
      double_atomicAdd(forcesb_x,-friction_x);
      double_atomicAdd(forcesa_y,friction_y);
      double_atomicAdd(forcesb_y,-friction_y);
      //   double_atomicAdd(torquea_z,friction_z);
      //   double_atomicAdd(torqueb_z,-friction_z);

      return;
    }

    //---------------------------------------------------------------------- Finished Contacts kernels and related functions ----------------------------------------//


    /// Verlet function. This integrates position and velocity using bubble poisitions at time t-1 and time t and forces and time t to calculate positinos at time t+1.

    void __global__ verletfunc(double* device_s0_pos_x,double* device_s0_pos_y,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_vel_x,double* device_s1_vel_y,double* device_s1_om_z,double* device_s1_radius, double* device_s1_mass, double* device_s1_moment,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_om_z,double* forces_x,double* forces_y,double* torque_z,double LX, double LY,double* liquid_fraction, double* bubble_volume,int Xblocksi, int i){
      double dx,dy,delta,truc;
      int idx=blockIdx.x*blockDim.x + threadIdx.x;
      int gid=idx+1;										// global Id
      double friction_x=0.,friction_y=0.,friction_z,VS=0.,vtx,vty,vtz,vsx,vsy,vsz,VT=0.;
      double beta=0.*PI/180.;
      int 	j,k,jv;
      vector n12;
      double nforce,dd;
      vector tension;

      if(gid <= NGRAINS+NFIBERS){

        // tension.x=0.;
        // tension.z=0.;
        //   if(gid>NGRAINS){
        //    if((gid-NGRAINS)%50>0){
        // jv=gid+1;
        //     dd=device_distance_array_abs_i_pared(device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_pos_x[jv],device_s1_pos_y[jv]);
        //     nforce=K_G*(dd-RM);
        //      n12=device_normal_vector_array_pared(device_s1_pos_x[gid],device_s1_pos_y[gid],device_s1_pos_x[jv],device_s1_pos_y[jv]);
        // // tension.x=nforce*n12.x;
        // // tension.z=nforce*n12.z;
        //      tension.x=0.001;
        // //     forces_x[gid]+=0.002;
        // //     double_atomicAdd(&forces_y[gid],);
        // //     forces_y[j]+=nforce*n12.z;
        // //     forces_x[jv]+=-nforce*n12.x;
        // //     forces_y[jv]+=-nforce*n12.z;
        //
        //    }
        //   }

        // delta=device_s1_pos_z[gid]-(Z1+device_s1_radius[gid]);

        // 	if(delta>0.){
        // 	device_s2_pos_z[gid]=2.*device_s1_pos_z[gid]-device_s0_pos_z[gid]+dt2*(forces_z[gid]/device_s1_mass[gid]-GRAVITY);	// verlet intagration, no periodic boundarys in z dimension
        device_s2_pos_x[gid]=device_XCP(2.*device_s1_pos_x[gid]-device_s0_pos_x[gid]+dt2*((forces_x[gid]/*+tension.x*/)/device_s1_mass[gid]),LX);	// verlet integration step. XCP computes
        // periodic boundary condition.

        device_s2_pos_y[gid]=2.*device_s1_pos_y[gid]-device_s0_pos_y[gid]+dt2*((forces_y[gid]/*+tension.z*/)/device_s1_mass[gid]);


        // 	}
        // else{
        // 	device_s2_pos_z[gid]=2.*device_s1_pos_z[gid]-device_s0_pos_z[gid]+dt2*((forces_z[gid]-K_G*delta-GAMMA_G*device_s1_vel_z[gid])/device_s1_mass[gid]-GRAVITY);
        //
        // // 		vtx=device_s1_vel_x[gid];
        // // 		vty=device_s1_vel_y[gid]*(1.-sin(alpha));
        // // 		vtz=device_s1_vel_z[gid]*(1.-cos(alpha));
        // // 		VT=sqrt(vtx*vtx+vty*vty+vtz*vtz);
        // 	vsx=device_s1_vel_x[gid]-(device_s1_radius[gid]*device_s1_om_y[gid]);
        // 	vsy=device_s1_vel_y[gid]+(device_s1_radius[gid]*device_s1_om_x[gid]);
        // // 	vsz=vtz-(device_s1_radius[gid]*device_s1_om_y[gid])+(device_s1_radius[gid]*device_s1_om_x[gid]);
        // 	VS=sqrt(vsx*vsx+vsy*vsy);
        // 	VT=sqrt(device_s1_vel_x[gid]*device_s1_vel_x[gid]+device_s1_vel_y[gid]*device_s1_vel_y[gid]);
        // 	// periodic boundary condition.
        // 	friction_x=((MU_G*GRAVITY*device_s1_mass[gid]<GCOULOMB*VS)?-MU_G*GRAVITY*vsx/VS:-GCOULOMB*VS/device_s1_mass[gid]*vsx/VS);
        // 	friction_y=((MU_G*GRAVITY*device_s1_mass[gid]<GCOULOMB*VS)?-MU_G*GRAVITY*vsy/VS:-GCOULOMB*VS/device_s1_mass[gid]*vsy/VS);
        // // 	friction_z=((MU_G*GRAVITY*device_s1_mass[gid]<GCOULOMB*VS)?-MU_G*GRAVITY*vsz/VS:-GCOULOMB*VS/device_s1_mass[gid]);
        //
        // 	  	device_s2_pos_x[gid]=device_XCP(2.*device_s1_pos_x[gid]-device_s0_pos_x[gid]+dt2*(forces_x[gid]/device_s1_mass[gid]+friction_x),LX);	// verlet integration step. XCP computes
        // device_s2_pos_y[gid]=device_YCP(2.*device_s1_pos_y[gid]-device_s0_pos_y[gid]+dt2*(forces_y[gid]/device_s1_mass[gid]+friction_y+GRAVITY*sin(beta)),LY);
        //
        // }
        // z velocity easy  as no periodic boundary condition

        // need to be careful if on the sides

        dx=device_s2_pos_x[gid]-device_s0_pos_x[gid];					// need to be careful if x or y passed through a side, need te reduce distance to account for periodic
        // boundarys. If this wasnt here velocity could be wayy too large( if a bubble passes through a wall).
        if(dx<-LX/2.){
          dx=dx+LX;
        }
        else if(dx > LX/2.){
          dx=dx-LX;
        }

        device_s2_vel_x[gid]=0.5*dx/dt;

        dy=device_s2_pos_y[gid]-device_s0_pos_y[gid];
        // 	if(dy<-LY/2.){
        // 		dy=dy+LY;
        // 		}
        // 	else if(dy > LY/2.){
        // 		dy=dy-LY;
        // 	}

        device_s2_vel_y[gid]=0.5*dy/dt;
        //printf(" gid %d finished verlet \n",gid);

        /* 	device_s2_the_x[gid]=2.*device_s1_the_x[gid]-device_s0_the_x[gid]+dt2*torque_x[gid]/device_s1_moment[gid];	// verlet integration step. XCP computes
            device_s2_the_y[gid]=2.*device_s1_the_y[gid]-device_s0_the_y[gid]+dt2*torque_y[gid]/device_s1_moment[gid];
            device_s2_the_z[gid]=2.*device_s1_the_z[gid]-device_s0_the_z[gid]+dt2*torque_z[gid]/device_s1_moment[gid];*/
        //  truc=dt/device_s1_moment[gid];

        //  	device_s2_om_z[gid]=device_s1_om_z[gid]+truc*torque_z[gid];

        // 		if(gid==6&&i%100==0) printf("Hola qui va là ? \t %d %f %f %f \t %f %f \t%f  %f\n",i,VS,VT,friction_y,device_s1_om_x[gid],device_s2_om_x[gid],torque_x[gid],dt2*1.E12*torque_x[gid]/device_s1_moment[gid]);


        //	if(gid==6&&fabs((device_s2_the_x[gid]-device_s1_the_x[gid])/device_s1_the_x[gid])>0.)
        //	 printf("youpi %d\t %.12f \t %f %f %f omega: %f \n",i,torque_x[gid],device_s0_the_x[gid],device_s1_the_x[gid],device_s2_the_x[gid],device_s2_om_x[gid]);

      }


      // have to check liquid fraction somewhere, might as well be here //

      if(gid == NGRAINS+NFIBERS){										// need to compute liquid fraction, and only need a single thread to do it.
        liquid_fraction[0]=1.0-bubble_volume[0]/((X2-X1)*(device_s1_pos_y[gid+NWALL-1]-Z1));
      }

      return;
    }

    // This function computes the stress forces on the wall ( both normal and tangental ), it reduces forces from all wall bubbles into a total stress force
    void __global__ wall_forces_final_2(double* forces_x,double* forces_y,double* torque_z,double* doforce, double* upforce, double* xforce, int Xblocks, int i){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;
      int t = threadIdx.x;							// thread ID
      int gid= idx+1+NGRAINS+NFIBERS;						// global bubble ID, global thread ID starts from zero so need to add 1+NGRAINS to start from wall Ids

      __shared__ double upforceblock[BLOCKSIZEXWALL/2];	// shared memory to do the reductions in, first reduction step is done in loading in values from global memory,
      __shared__ double xforceblock[BLOCKSIZEXWALL/2];	// henace we only need half a blocksize in array length.
      __shared__ double doforceblock[BLOCKSIZEXWALL/2];
      if(threadIdx.x%2==0){								// initialise shared memory to zero
        upforceblock[threadIdx.x/2]=0.0;
        doforceblock[threadIdx.x/2]=0.0;
        xforceblock[threadIdx.x/2]=0.0;
      }
      __syncthreads();
      if( (gid <= (NGRAINS +NFIBERS+NWALL/2))){					// if adding forces from the bottom wall


        double_atomicAdd(&doforceblock[threadIdx.x/2],forces_y[gid]);				// add normal force (z dimension total force) to a down force
        double_atomicAdd(&xforceblock[threadIdx.x/2],-forces_x[gid]);				// add tangental friction force (x dimension firction force) to overall tangental friction
        // force with minus ( as force is pulling against wall motion)
        // double_atomicAdd(&xforceeblock[threadIdx.x/2],torque_x[gid]);				// add tangental friction force( x dimension friction force ) to overall tangental repulsion
        // force ( this is taken away from total tangental force ( friction + repulsion) to get
        // just repulsion force. This is why it has a positive sign
        // double_atomicAdd(&xforceeblock[threadIdx.x/2],-forces_x[gid]); 				// add total force to overall tangental repulsion force ( this adds to the positive friction
        // component leaving just the repulsion tangental force
      }

      if((gid > (NGRAINS +NFIBERS+ NWALL/2)) & (gid <= (NGRAINS +NFIBERS+ NWALL))){		// if adding forces from the top wall
        double_atomicAdd(&upforceblock[threadIdx.x/2],forces_y[gid]);		// the same as above but to up force
        double_atomicAdd(&xforceblock[threadIdx.x/2],forces_x[gid]);		// the same as above but with opposite sign as they are being added from the bottom wall
        // double_atomicAdd(&xforceeblock[threadIdx.x/2],-torque_x[gid]);
        // double_atomicAdd(&xforceeblock[threadIdx.x/2],forces_x[gid]);

      }
      __syncthreads();													// need the sync threads as the reduction is happening over multiple iterations on a single block.


      int stride=blockDim.x/4;											// redutction is in second iteration, so we start at stride 2^2=4. each stride is a power of 2.
      for(stride=blockDim.x/4;stride>=1;stride=stride/2){					// reduce stride by a half at every iteration
        __syncthreads();
        if(t<stride){													// for the first t threads add data from t+stride. Doing the reduction in this manner leaves the
          // first t threads to be activeat every iteration step, and these will be in the same, or a small
          // number of warps. This decreases thread divergence (if the active threads were spread over more warps)

          doforceblock[t]+=doforceblock[t+stride];
          upforceblock[t]+=upforceblock[t+stride];
          xforceblock[t]+=xforceblock[t+stride];
          // 		xforceeblock[t]+=xforceeblock[t+stride];
        }
      }


      __syncthreads();
      if(threadIdx.x%256==0){												// each block then adds to the global stress forces with atomics
        double_atomicAdd(upforce,upforceblock[threadIdx.x/2]);
        double_atomicAdd(doforce,doforceblock[threadIdx.x/2]);
        double_atomicAdd(xforce,xforceblock[threadIdx.x/2]);
        // double_atomicAdd(xforcee,xforceeblock[threadIdx.x/2]);
      }
    }


    //---------------------------------------------------------------- Kernels that do the list updates ---------------------------------------------------//
    // this kernel checks which bubbles are moving cells, and marks the cells as either losing or gaining that bubble ID.
    void __global__ serialupdate(int Xblocks,int*  s1_cell_i,int* s1_cell_j, double* s2_pos_x,double* s2_pos_y, double* b2_pos_z, int* s2_cell_i, int* s2_cell_j, double LX, double LY, int i, int* AL, int* PL,int* HoL){
      int gid;
      for(gid=1;gid<=NGRAINS+NFIBERS+NWALL;gid++){
        s2_cell_i[gid]=device_CPI((int)((s2_pos_x[gid]-X1)*NX/LX));			// compute new cell ( b2 is new position)
        s2_cell_j[gid]=(int)((s2_pos_y[gid]-Z1)*NY/LY);

        // If bubble j has moved
        if(s2_cell_i[gid]!=s1_cell_i[gid]||s2_cell_j[gid]!=s1_cell_j[gid]){
          int cid1=s1_cell_i[gid]*(NY+1)+s1_cell_j[gid];
          int cid2=s2_cell_i[gid]*(NY+1)+s2_cell_j[gid];
          // then we rearrange its former cell
          if(AL[gid]>0){	PL[AL[gid]]=PL[gid];}
          else		{HoL[cid1]=PL[gid];	}
          AL[PL[gid]]=AL[gid];

          // and then we rearrange its new cell

          AL[HoL[cid2]]=gid;
          AL[gid]=0;
          PL[gid]=HoL[cid2];
          HoL[cid2]=gid;

          // and we store the new cell
          // 			b1[j].cell.i=b2[j].cell.i;
          // 			b1[j].cell.j=b2[j].cell.j;
          // 			b1[j].cell.k=b2[j].cell.k;
        }
      }

    }

    void __global__ SEAN_parallel_list_update(int Xblocks,int* move_list,int*  s1_cell_i,int* s1_cell_j, double* s2_pos_x,double* s2_pos_y,  int* s2_cell_i, int* s2_cell_j, cell_up* cell_list,  int* device_mutex, double LX, double LY, int i){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;							// global ID

      if(gid <= NGRAINS+NFIBERS+NWALL){				// must update lists for all bubbles, including wall bubbles

        s2_cell_i[gid]=device_CPI((int)((s2_pos_x[gid]-X1)*NX/LX));			// compute new cell ( b2 is new position)
        s2_cell_j[gid]=(int)((s2_pos_y[gid]-Z1)*NY/LY);
        if(s2_cell_i[gid]!=s1_cell_i[gid]||s2_cell_j[gid]!=s1_cell_j[gid]){		// if bubble has moved cell
          //   printf("ALORS VOYONS %d %d %d\t mutex %d \n",gid,s1_cell_j[5],s2_cell_j[6],device_mutex[s2_cell_i[gid]*(NY+1)*(NZ+1)+s2_cell_j[gid]*(NZ+1)+b2_cell_k[gid]]);

          check_bubble_move(gid,move_list,cell_list,s1_cell_i,s1_cell_j,s2_cell_i,s2_cell_j,device_mutex);	// mark the cells as losing or gaining ID.
        }
      }
      // 	if(i>40000&&i%10==0&&gid==NGRAINS+NWALL-1) printf("ben voila c fini pour parallel iter %d \t%d %.12f %d\n",i,b1_cell_k[NGRAINS+NWALL-3],b2_pos_z[NGRAINS+NWALL-3],b2_cell_k[NGRAINS+NWALL-3]);
      return;
    }


    void __global__ parallel_list_update(int Xblocks,int dimListUpdate,int* move_list,int*  s1_cell_i,int* s1_cell_j, double* s2_pos_x,double* s2_pos_y,  int* s2_cell_i, int* s2_cell_j, cell_up* cell_list,  int* device_mutex, double LX, double LY, int i){
      int ind;
      // int idx=blockIdx.x*blockDim.x + threadIdx.x;
      // if(i==1245) printf("merde idx %d \t %d \t %d %d %d\n",idx, blockIdx.x,blockDim.x,threadIdx.x,dimListUpdate);
      for(ind=1;ind<=dimListUpdate;ind++){
        int gid=(blockIdx.x)*dimListUpdate+ind;							// global ID

        if(gid <= NGRAINS+NFIBERS+NWALL){				// must update lists for all bubbles, including wall bubbles
          // if(i==1245&&gid==1845) printf("plouf gid %d \t %d %d %d\n",gid,ind, blockIdx.x,threadIdx.x);
          s2_cell_i[gid]=device_CPI((int)((s2_pos_x[gid]-X1)*NX/LX));			// compute new cell ( b2 is new position)
          s2_cell_j[gid]=(int)((s2_pos_y[gid]-Z1)*NY/LY);
          if(s2_cell_i[gid]!=s1_cell_i[gid]||s2_cell_j[gid]!=s1_cell_j[gid]){		// if bubble has moved cell
            //   printf("ALORS VOYONS %d %d %d\t mutex %d \n",gid,s1_cell_j[5],s2_cell_j[6],device_mutex[s2_cell_i[gid]*(NY+1)*(NZ+1)+s2_cell_j[gid]*(NZ+1)+b2_cell_k[gid]]);
            //
            check_bubble_move(gid,move_list,cell_list,s1_cell_i,s1_cell_j,s2_cell_i,s2_cell_j,device_mutex);	// mark the cells as losing or gaining ID.
          }
        }
      }
      // 	if(i>40000&&i%10==0&&gid==NGRAINS+NWALL-1) printf("ben voila c fini pour parallel iter %d \t%d %.12f %d\n",i,b1_cell_k[NGRAINS+NWALL-3],b2_pos_z[NGRAINS+NWALL-3],b2_cell_k[NGRAINS+NWALL-3]);
      return;
    }

    void __device__ check_bubble_move(int gid, int* move_list,cell_up* cell_list, int* s1_cell_i,int* s1_cell_j,int* s2_cell_i,int* s2_cell_j, int* mutex){
      int cellout;								// cell moving out from
      int cellin;									// cell moiving in to

      bool leaveloopout=false;						// need for the locks
      bool leaveloopin=false;

      cellout=s1_cell_i[gid]*(NY+1)+s1_cell_j[gid];		// compute cell in and cell out
      cellin=s2_cell_i[gid]*(NY+1)+s2_cell_j[gid];


      // 		do{
      while(!leaveloopout){
        // 		  		    printf("ALORS BIS %d %d %d\t mutex %d \n",gid,s1_cell_j[5],s1_cell_j[6],mutex[s1_cell_i[gid]*(NY+1)*(NZ+1)+s1_cell_j[gid]*(NZ+1)+b1_cell_k[gid]]);
        // // try to grab a lock on cell out
        if(atomicCAS(&(mutex[cellout]),0,1)==0){
          // 		  		    printf("ALORS BIS %d %d %d\t mutex %d \n",gid,s1_cell_j[5],s1_cell_j[6],mutex[s1_cell_i[gid]*(NY+1)*(NZ+1)+s1_cell_j[gid]*(NZ+1)+b1_cell_k[gid]]);
          atomicExch(&cell_list[cellout].bubout[cell_list[cellout].countout],gid);	// must use atomics ( or thread fence) in critical ( mutexed) section.
          // add bubble ID to cells ID leaving list
          atomicAdd(&cell_list[cellout].countout,1);									// add 1 to cells Bubble leaving counter
          // 			if(cell_list[cellout].countout > 17){										// we only allow for 9 bubbles to be leaving a cell in a single time step.
          // 					printf(" HOUStON PROBLEM WITH COUNTOUT \n");				// this has been measured as ok ( max ... ).
          // 			}
          leaveloopout=true;
          atomicExch(&(mutex[cellout]),0);											// release lock on cell
        }
      }
      // 	while(!leaveloopout);

      // reset boolean
      while(!leaveloopin){																	// same as above, but adding ID to cells entering list.
        // 	  		  		    printf("ALORS TER %d %d %d\t mutex %d \n",gid,s2_cell_j[5],s2_cell_j[6],mutex[s2_cell_i[gid]*(NY+1)*(NZ+1)+s2_cell_j[gid]*(NZ+1)+b2_cell_k[gid]]);
        if(atomicCAS(&(mutex[cellin]),0,1)==0){
          atomicExch(&cell_list[cellin].bubin[cell_list[cellin].countin],gid);
          atomicAdd(&cell_list[cellin].countin,1);
          // 			if(cell_list[cellin].countin > 17){
          // 				printf(" HOUStON PROBLEM WITH COUNTIN \n");
          // 			}
          leaveloopin=true;
          atomicExch(&(mutex[cellin]),0);
        }
      }


    }

    // this updates the lists ( HoL, Pl and Al ) for cells that have a bubble leaving them
    void __global__ cell_lists_out_update(int i,cell_up* cell_list, int* AL, int* PL, int* HoL, int Xblocks){
      int idx=blockIdx.x*blockDim.x + threadIdx.x;		// Global ID, this kernel runs at one thread per cell, not 1 thread per bubble.

      int gid=idx;
      int bubid;											// bubble ID
      int old_head;										// old head of cell
      int count;					// counter for number of bubble leaving the cell
      if(gid < ((NX+1)*(NY+1))){

        //   	if(i>37000&&i%1==0&&HoL[gid]>0) printf("diable %d %d: %d %d %d %d\t \t out: %d\n",i,gid, HoL[gid],PL[HoL[gid]],PL[PL[HoL[gid]]],PL[PL[PL[HoL[gid]]]],cell_list[gid].countout);// If GID is an cell in our setup
        old_head=HoL[gid];								// old head of cell
        count=cell_list[gid].countout;					//
        cell_list[gid].countout=0;						// reset cells bubble leaving counter

        while(count >0){	 							// while there are bubbles leaving the cell that have not yet been computed
          count-=1;									// reduce counter
          bubid=cell_list[gid].bubout[count];			// load bubble ID from cell leaving list
          cell_list[gid].bubout[count]=0;				// set that cell leaving list entry to zero
          if(AL[bubid]>0){PL[AL[bubid]]=PL[bubid];}	// if bubble has a parent, set theparent of its child to be its parent
          else{old_head=PL[bubid];}					// otherwise set its child as the head of the list
          AL[PL[bubid]]=AL[bubid];					// set the parent of its child to be its parent
        }
        HoL[gid]=old_head;								// update the head of list in global memory with our register value at the end of the loop.
      }
    }

    // this updates the lists for cells that have a bubble entering them. The entering and leaving cells need to be in two seperate kernels as there could be race conditions
    //  in the lists if they werent. ( because we have to parallelise by cell and not by thread, two cells could update the same bubble simultaneously.
    void __global__ cell_lists_in_update(int i, cell_up* cell_list, int* AL, int* PL, int* HoL, int Xblocks){
      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx;
      int bubid;
      int new_head;
      int count;

      if(gid < ((NX+1)*(NY+1))){
        new_head=HoL[gid];								// new head of list stored in registers
        count=cell_list[gid].countin;
        cell_list[gid].countin=0;
        while(count>0){									// while there are bubbles entering the cell that havent been dealt with
          count-=1;									// reduce counter
          bubid=cell_list[gid].bubin[count];			// load bubbler ID
          cell_list[gid].bubin[count]=0;
          //		AL[HoL[gid]]=bubid;
          AL[new_head]=bubid;							// Parent of the previous head of list is this bubble
          AL[bubid]=0;								// this bubbles parent is 0
          //		PL[bubid]=HoL[gid];
          PL[bubid]=new_head;							// this bubbles child is the old head of the list
          //		HoL[gid]=bubid;
          new_head=bubid;								// this bubble is the new head of the list
        }
        HoL[gid]=new_head;								// update global head of list value with the register value.
      }
    }
    // This function moves the top wall. It either moves it in a compressing b=motion in the Z plance, or a shearing motino in the X plane. Which is defined by the switch
    void __global__ move_walls(double* device_s1_pos_x,double* device_s1_pos_y,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_vel_x,double* device_s2_vel_y, int* sw,double*
        liquid_fraction,double LX,int Xblocks, int i,double* device_U1){


      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx + NGRAINS +NFIBERS+ NWALL/2;					// global ID starts from the top wall bubble ID

      if(gid > (NGRAINS+NFIBERS+NWALL/2) && gid <= (NGRAINS +NFIBERS+ NWALL)){	// only work on the top wall bubbles

        if( sw[0]==0 && liquid_fraction[0]>=phi0) { // if switch isnt tripped and liquid fraction is too high compress

          device_s2_pos_y[gid]=device_s1_pos_y[gid]-0.6*dt;		// move walls z position down with predefined velocity
          device_s2_vel_y[gid]=-0.6; // velocity data for wall bubbles isnt really important, this could probably be omitted
          device_s2_pos_x[gid]=device_s1_pos_x[gid];				// x and y positions ont change, they also havent changed in verlet function so this
          device_U1[0]=DE*device_s1_pos_y[NGRAINS+NFIBERS+NWALL-1]/tau_d;
          //device_s2_pos_y[gid]=device_s1_pos_y[gid];				// can probably be omitted
        }

        else{
          sw[0]=1;							// if we have finished compressing then we flip the switch

          //device_s2_pos_z[gid]=device_s1_pos_z[gid];		// z position stays the same
          device_s2_vel_x[gid]=device_U1[0];						//
          device_s2_vel_y[gid]=0.;
          device_s2_pos_x[gid]=device_XCP(device_s1_pos_x[gid]+device_U1[0]*dt,LX); // x position of walls moves with predefined velocity.
          device_s2_pos_y[gid]=device_s1_pos_y[gid];		// y position stays the same, again probably not needed.
        }
      }
    }

    // this function splits the force structures to arrays of force structure components
    void __global__ forces_to_arrays(force* device_forces, double* device_torque, double* device_forces_x,double* device_forces_y,double* device_torque_z){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if (gid <= NGRAINS+NFIBERS+ NWALL){

        device_forces_x[gid]=device_forces[gid].x;
        device_forces_y[gid]=device_forces[gid].z;

        device_torque_z[gid]=device_torque[gid];

      }
    }
    // this function brings arrays of force structure componenets back into the actual structures
    void __global__ arrays_to_forces(force* device_forces, double* device_torque, double* device_forces_x,double* device_forces_y,double* device_torque_z){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if (gid <= NGRAINS+NFIBERS+NWALL){


        device_forces[gid].x=device_forces_x[gid];
        device_forces[gid].z=device_forces_y[gid];

        device_torque[gid]=device_torque_z[gid];

      }
    }
    // Splits bubble structures to arrays of bubble structure components
    void __global__ bubbles_to_arrays(double* device_s0_pos_x,double* device_s0_pos_y,double* device_s0_the_z,double* device_s0_vel_x,double* device_s0_vel_y,double* device_s0_om_z,double* device_s0_radius, double* device_s0_mass,double* device_s0_moment, int* device_s0_cell_i,int* device_s0_cell_j,int* device_s0_fiber,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_the_z,double* device_s1_vel_x,double* device_s1_vel_y,double* device_s1_om_z,double* device_s1_radius, double* device_s1_mass, double* device_s1_moment,int* device_s1_cell_i,int* device_s1_cell_j,int* device_s1_fiber,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_the_z,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_om_z,double* device_s2_radius, double* device_s2_mass,double* device_s2_moment, int* device_s2_cell_i,int* device_s2_cell_j,int* device_s2_fiber,sphere* device_s0, sphere* device_s1, sphere* device_s2,int Xblocks){


      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if (gid <= NGRAINS+NFIBERS+ NWALL){


        device_s0_pos_x[gid]=device_b0[gid].position.x;
        device_s0_pos_y[gid]=device_b0[gid].position.z;
        device_s0_vel_x[gid]=device_b0[gid].velocity.x;
        device_s0_vel_y[gid]=device_b0[gid].velocity.z;
        device_s0_the_z[gid]=device_b0[gid].theta;
        device_s0_om_z[gid]=device_b0[gid].omega;
        device_s0_radius[gid]=device_b0[gid].radius;
        device_s0_mass[gid]=device_b0[gid].mass;
        device_s0_moment[gid]=device_b0[gid].moment;
        device_s0_cell_i[gid]=device_b0[gid].cell.i;
        device_s0_cell_j[gid]=device_b0[gid].cell.j;
        device_s0_fiber[gid]=device_b0[gid].fiber;

        device_s1_pos_x[gid]=device_b1[gid].position.x;
        device_s1_pos_y[gid]=device_b1[gid].position.z;
        device_s1_vel_x[gid]=device_b1[gid].velocity.x;
        device_s1_vel_y[gid]=device_b1[gid].velocity.z;
        device_s1_the_z[gid]=device_b1[gid].theta;
        device_s1_om_z[gid]=device_b1[gid].omega;
        device_s1_radius[gid]=device_b1[gid].radius;
        device_s1_mass[gid]=device_b1[gid].mass;
        device_s1_moment[gid]=device_b1[gid].moment;
        device_s1_cell_i[gid]=device_b1[gid].cell.i;
        device_s1_cell_j[gid]=device_b1[gid].cell.j;
        device_s1_fiber[gid]=device_b1[gid].fiber;

        device_s2_pos_x[gid]=device_b2[gid].position.x;
        device_s2_pos_y[gid]=device_b2[gid].position.z;
        device_s2_vel_x[gid]=device_b2[gid].velocity.x;
        device_s2_vel_y[gid]=device_b2[gid].velocity.z;
        device_s2_the_z[gid]=device_b2[gid].theta;
        device_s2_om_z[gid]=device_b2[gid].omega;
        device_s2_radius[gid]=device_b2[gid].radius;
        device_s2_mass[gid]=device_b2[gid].mass;
        device_s2_moment[gid]=device_b2[gid].moment;
        device_s2_cell_i[gid]=device_b2[gid].cell.i;
        device_s2_cell_j[gid]=device_b2[gid].cell.j;
        device_s2_fiber[gid]=device_b2[gid].fiber;
      }
    }
    // Splits bubble structures to arrays of bubble structure components
    void __global__ beams_to_arrays(unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur, double* device_b1_tx,double* device_b1_ty,unsigned int* device_b1_actif, beam* device_b1,int XBBeams,int NBEAMS){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;
      int gid=idx+1;

      if (gid <= NBEAMS){
        device_b1_j1[gid]=device_b1[gid].j1;
        device_b1_j2[gid]=device_b1[gid].j2;
        device_b1_longueur[gid]=device_b1[gid].longueur;
        device_b1_actif[gid]=device_b1[gid].actif;
        device_b1_tx[gid]=device_b1[gid].tx;
        device_b1_ty[gid]=device_b1[gid].ty;
      }
    }

    // brings bubble structure components back to bubble structures
    void __global__ arrays_to_bubbles(double* device_s0_pos_x,double* device_s0_pos_y,double* device_s0_the_z,double* device_s0_vel_x,double* device_s0_vel_y,double* device_s0_om_z,double* device_s0_radius, double* device_s0_mass,double* device_s0_moment, int* device_s0_cell_i,int* device_s0_cell_j,int* device_s0_fiber,double* device_s1_pos_x,double* device_s1_pos_y,double* device_s1_the_z,double* device_s1_vel_x,double* device_s1_vel_y,double* device_s1_om_z,double* device_s1_radius, double* device_s1_mass, double* device_s1_moment,int* device_s1_cell_i,int* device_s1_cell_j,int* device_s1_fiber,double* device_s2_pos_x,double* device_s2_pos_y,double* device_s2_the_z,double* device_s2_vel_x,double* device_s2_vel_y,double* device_s2_om_z,double* device_s2_radius, double* device_s2_mass,double* device_s2_moment, int* device_s2_cell_i,int* device_s2_cell_j,int* device_s2_fiber,sphere* device_s0, sphere* device_s1, sphere* device_s2,int Xblocks){


      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if (gid <= NGRAINS+NFIBERS+ NWALL){


        device_b0[gid].position.x=device_s0_pos_x[gid];
        device_b0[gid].position.z=device_s0_pos_y[gid];
        device_b0[gid].velocity.x=device_s0_vel_x[gid];
        device_b0[gid].velocity.z=device_s0_vel_y[gid];
        device_b0[gid].theta=device_s0_the_z[gid];
        device_b0[gid].omega=device_s0_om_z[gid];
        device_b0[gid].radius=device_s0_radius[gid];
        device_b0[gid].mass=device_s0_mass[gid];
        device_b0[gid].moment=device_s0_moment[gid];
        device_b0[gid].cell.i=device_s0_cell_i[gid];
        device_b0[gid].cell.j=device_s0_cell_j[gid];
        device_b0[gid].fiber=device_s0_fiber[gid];

        device_b1[gid].position.x=device_s1_pos_x[gid];
        device_b1[gid].position.z=device_s1_pos_y[gid];
        device_b1[gid].velocity.x=device_s1_vel_x[gid];
        device_b1[gid].velocity.z=device_s1_vel_y[gid];
        device_b1[gid].theta=device_s1_the_z[gid];
        device_b1[gid].omega=device_s1_om_z[gid];
        device_b1[gid].radius=device_s1_radius[gid];
        device_b1[gid].mass=device_s1_mass[gid];
        device_b1[gid].moment=device_s1_moment[gid];
        device_b1[gid].cell.i=device_s1_cell_i[gid];
        device_b1[gid].cell.j=device_s1_cell_j[gid];
        device_b1[gid].fiber=device_s1_fiber[gid];

        device_b2[gid].position.x=device_s2_pos_x[gid];
        device_b2[gid].position.z=device_s2_pos_y[gid];
        device_b2[gid].velocity.x=device_s2_vel_x[gid];
        device_b2[gid].velocity.z=device_s2_vel_y[gid];
        device_b2[gid].theta=device_s2_the_z[gid];
        device_b2[gid].omega=device_s2_om_z[gid];
        device_b2[gid].radius=device_s2_radius[gid];
        device_b2[gid].mass=device_s2_mass[gid];
        device_b2[gid].moment=device_s2_moment[gid];
        device_b2[gid].cell.i=device_s2_cell_i[gid];
        device_b2[gid].cell.j=device_s2_cell_j[gid];
        device_b2[gid].fiber=device_s2_fiber[gid];

      }
    }
    // brings bubble structure components back to beam structures
    void __global__ arrays_to_beams(unsigned int* device_b1_j1,unsigned int* device_b1_j2,double* device_b1_longueur, double* device_b1_tx,double* device_b1_ty,unsigned int* device_b1_actif, beam* device_b1,int XBBeams, int *device_nbactive){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;
      int gid=idx+1;

      if (gid <= *device_nbactive){
        device_b1[gid].j1=device_b1_j1[gid];
        device_b1[gid].j2=device_b1_j2[gid];
        device_b1[gid].longueur=device_b1_longueur[gid];
        device_b1[gid].actif=device_b1_actif[gid];
        device_b1[gid].tx=device_b1_tx[gid];
        device_b1[gid].ty=device_b1_ty[gid];
      }
    }

    // This function copies important data from the GPU over to the CPU
    void write_out(sphere* host_s0, sphere* host_s1, sphere* host_s2, beam* host_b1, force* host_forces,double* host_torque,sphere* device_s0, sphere* device_s1, sphere* device_s2, beam* device_b1,force* device_forces,double* device_torque,double* host_doforcept,double* host_upforcept,double* host_xforcept,double* device_doforce,double* device_upforce,double* device_xforce,  int* host_AL, int* host_PL, int* device_AL, int* device_PL,contact_list* host_contact_list, contact_list* device_contact_list,int* device_sw, int* host_sw,int *device_nbactive,int *host_nbactive){


      cudaError_t ret;
      ret=cudaMemcpy(host_s0,device_s0,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_s0\n");
      ret=cudaMemcpy(host_s1,device_s1,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_s1\n");
      ret=cudaMemcpy(host_s2,device_s2,(NGRAINS+NFIBERS+NWALL+1)*sizeof(sphere),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_s2\n");
      ret=cudaMemcpy(host_b1,device_b1,(NBEAMS+1)*sizeof(beam),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_b1\n");
      ret=cudaMemcpy(host_forces,device_forces,(NGRAINS+NFIBERS+NWALL+1)*sizeof(force),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_forces\n");
      ret=cudaMemcpy(host_torque,device_torque,(NGRAINS+NFIBERS+NWALL+1)*sizeof(double),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_torque\n");

      ret=cudaMemcpy(host_doforcept,device_doforce,sizeof(double),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_doforce\n");
      ret=cudaMemcpy(host_upforcept,device_upforce,sizeof(double),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_upforce\n");
      ret=cudaMemcpy(host_xforcept,device_xforce,sizeof(double),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_xforce\n");
      // ret=cudaMemcpy(host_xforceept,device_xforcee,sizeof(double),cudaMemcpyDeviceToHost);
      // if(ret!=cudaSuccess)printf("Cuda memcpy error with host_xforcee\n");
      ret=cudaMemcpy(host_AL,device_AL,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_xforcev\n");
      ret=cudaMemcpy(host_PL,device_PL,(NGRAINS+NFIBERS+NWALL+1)*sizeof(int),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_xforcee\n");
      ret=cudaMemcpy(host_contact_list,device_contact_list,(NGRAINS+NFIBERS+NWALL+1)*sizeof(contact_list),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with host_contact_list\n");
      ret=cudaMemcpy(host_sw,device_sw,sizeof(int),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with sw\n");
      ret=cudaMemcpy(host_nbactive,device_nbactive,sizeof(int),cudaMemcpyDeviceToHost);
      if(ret!=cudaSuccess)printf("Cuda memcpy error with nbactive\n");
    }

    // this function writes the wall forces ( the tangental and normal stresses ) to a file.
    void write_wall_forces_to_file(char* wfname,FILE* wallfile,double* host_doforce,double* host_upforce,double* host_xforce,sphere* host_s1,int i){
      int print_step2=10000;
      double dt1=dt;

      wallfile=fopen(wfname,"a");
      fprintf(wallfile,"%.6f \t %.12e \t %.12e \t %.6f \n",i*dt1*U0*host_b1[NWALL+NFIBERS+NGRAINS].position.z, -*host_xforce/print_step2, (*host_upforce-*host_doforce)/print_step2,i*dt1);
      fclose(wallfile);

    }

    // this function counts the nuber of bubbles in cells. It gives the total number of cells with N bubbles, n ranging from 0 to 17
    void __global__ cell_counter(int* HoL, int* PL, int* counter){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;					// global ID
      int i=0;
      int j;
      int celli;						// cell co-ords
      int cellj;
      if(gid < ((NX+1)*(NY+1))){	// if cell is inside range
        j=HoL[gid];						// start at head of cell
        if(j > 0){
          i+=1;						// add one to counter
          j=PL[j];
          while(PL[j] > 0){			// while there is still a child in the cell add one to the counter
            i+=1;
            j=PL[j];
          }
        }
        atomicAdd(&counter[i],1);		// add 1 to the number of cells with that contain this number of bubbles ( specified by the counter i)
      }
    }


    // prints info about the cell counter function
    void __global__ cell_counter_print(int* counter){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;
      int i=0;
      if(gid==1){
        for(i=0;i<9;i++){
          printf("Cells with %d number %d \n",i,counter[i]);
          counter[i]=0;
        }
      }
    }

    // Computes the bubble voume on the GPU, could be improved with reduction algorithms but it is only called once and hence isnt very expensive
    void __global__ bubble_volume_update(double* bubble_volume, double* a_radius ){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;		// global ID

      double add;
      int gid=idx+1;
      if(gid <= NGRAINS+NFIBERS){									// if its a bulk bubble
        add=PI*pow(a_radius[gid],2);
        double_atomicAdd(bubble_volume,add);				// add bubbles volum to total bubble volume
      }
      if((gid > (NGRAINS+NFIBERS)) && (gid <= (NGRAINS +NFIBERS+ NWALL))){	// if its a wal bubble, add half the bubbles volume to total bubble volume
        add=0.5*PI*pow(a_radius[gid],2);
        double_atomicAdd(bubble_volume,add);
      }
      return;
    }
    // kernel to reset the stress forces on the wall, just sets them all to zero.
    void __global__ reset_wall_forces(double* doforce, double* upforce, double* xforce, int Xblocks){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if(gid==1){
        *doforce=0.0;
        *upforce=0.0;
        *xforce=0.0;
        // 	*xforcee=0.0;
      }
    }

    // kernel that sets all the force component arrays to zero.
    void __global__ reset_forces(double* forces_x,double* forces_y,double* torque_z,  int Xblocks){
      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if(gid<=NGRAINS+NFIBERS+NWALL){
        forces_x[gid]=0.;
        forces_y[gid]=0.;
        //    	torque_z[gid]=0.;
      }
    }
    // kernel that updates bubble data, copies B1 to B0 and B2 to B1, also resets force components to zero.
    void __global__ update_2bubbles_reset_forces(double*  s0_pos_x,double*  s0_pos_y,double*  s0_the_z,double*  s0_vel_x,double*  s0_vel_y,double*  s0_om_z,double*  s0_radius, double*  s0_mass,double*  s0_moment, int*  s0_cell_i,int*  s0_cell_j,int* s0_fiber,double*  s1_pos_x,double*  s1_pos_y,double*  s1_the_z,double*  s1_vel_x,double*  s1_vel_y,double*  s1_om_z,double*  s1_radius, double*  s1_mass, double*  s1_moment,int*  s1_cell_i,int*  s1_cell_j,int* s1_fiber,double*  s2_pos_x,double*  s2_pos_y,double*  s2_the_z,double*  s2_vel_x,double*  s2_vel_y,double*  s2_om_z,double*  s2_radius, double*  s2_mass,double*  s2_moment, int*  s2_cell_i,int*  s2_cell_j,int* s2_fiber,double* forcesx, double* forcesy, double* torquez, int start, int end,int Xblocks,int* device_nbactive){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;
      //if(gid==1) device_nbactive[0]=0;
      if(gid > 0 && gid <= NGRAINS+NFIBERS+NWALL){
        s0_pos_x[gid]=s1_pos_x[gid];
        s0_pos_y[gid]=s1_pos_y[gid];
        s0_vel_x[gid]=s1_vel_x[gid];
        s0_vel_y[gid]=s1_vel_y[gid];
        // 	b0_the_x[gid]=b1_the_x[gid];
        // 	b0_the_y[gid]=b1_the_y[gid];
        s0_the_z[gid]=s1_the_z[gid];
        s0_om_z[gid]=s1_om_z[gid];
        s0_cell_i[gid]=s1_cell_i[gid];
        s0_cell_j[gid]=s1_cell_j[gid];
        s0_fiber[gid]=s1_fiber[gid];

        s1_pos_x[gid]=s2_pos_x[gid];
        s1_pos_y[gid]=s2_pos_y[gid];
        s1_vel_x[gid]=s2_vel_x[gid];
        s1_vel_y[gid]=s2_vel_y[gid];
        // 	b1_the_x[gid]=b2_the_x[gid];
        // 	b1_the_y[gid]=b2_the_y[gid];

        s1_the_z[gid]=s2_the_z[gid];
        s1_om_z[gid]=forcesx[gid];
        s1_cell_i[gid]=s2_cell_i[gid];
        s1_cell_j[gid]=s2_cell_j[gid];
        s1_fiber[gid]=s2_fiber[gid];

        if(gid<=NGRAINS+NFIBERS+NWALL){
          forcesx[gid]=0.;
          forcesy[gid]=0.;
          // 	torquez[gid]=0.;
        }
        // 	else {
        // 		forcesx[gid]=100.;
        // 	forcesy[gid]=-100.;
        //
        // 	}
      }
    }

    // function to zero contact lists
    void __global__ zero_contact_lists(int Xblocks, contact_list* device_contact_list){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;
      int k;
      if(gid <= NGRAINS+NFIBERS){

        device_contact_list[gid].count=0;

        for(k=0;k<9;k++){
          device_contact_list[gid].contacts[k]=0;
        }
      }
    }


    //kernel to zero max contacts variables and arrays
    void __global__ device_max_contacts_zero( int* device_max_contacts){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;
      if(gid==1){
        device_max_contacts[0]=0;
      }
      if(gid<=NGRAINS+NFIBERS+NWALL){
        device_max_contacts[gid]=0;
      }
    }

    // kernel to find the maximum contacts a bubble can have
    void __global__ device_max_contacts_func(contact_list* device_contact_list, int* device_max_contacts){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;
      if(gid<=NGRAINS+NFIBERS){
        atomicAdd(&device_max_contacts[0],device_contact_list[gid].count);
        if(device_contact_list[gid].count > device_max_contacts[gid]){
          device_max_contacts[gid]=device_contact_list[gid].count;
        }
      }
    }

    // kernel to compute the total maximum contacts any bubble has, and the average number of contacts per bubble, note that the average doesnt include
    // the fact that if B1 is in contact with B2, then B2 is also in contact with B1. The average only counts this as a single contact for either B1 or B2
    void __global__ device_max_contacts_max( int* device_max_contacts){

      int gid=blockIdx.x*blockDim.x + threadIdx.x+1;
      int max;
      int i;

      if(gid==1){
        max=device_max_contacts[1];
        for(i=1;i<=NGRAINS;i++){
          if(device_max_contacts[i] > max){
            max=device_max_contacts[i];
          }
        }
        printf("Maximum number of contacts is %d\n",max);
        printf("Average number of contacts is %f from %d and %d\n",((double)device_max_contacts[0])/((double)NGRAINS*print_step),device_max_contacts[0],NGRAINS);
        device_max_contacts[0]=0;
      }
    }

    //kernel to zero device bubble volume
    void __global__ zero_bubble_volume(double* device_bubble_volume){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx+1;

      if(gid==1){
        device_bubble_volume[0]=0.;
      }
    }


    // kernel to print some interesting data to the scree. Like liquid fraction, bubble voloume, the switch, container volume and gamma (not sure what gamma is)
    void __global__ print_liquid_fraction(double* device_liquid_fraction, int* device_sw,double* device_bubble_volume, double* device_s1_pos_y,int Xblocks, double t, double u0){
      int idx=blockIdx.x*blockDim.x + threadIdx.x;
      int gid=idx+1;

      if(gid==1){
        // 	printf("printing liquid fraction and switch info \n");

        printf("Bubble volume is %e and container volume is %e and z3 is %lf, liquid fraction %f switch is %d shear is %.2f \n",device_bubble_volume[0],(X2-X1)*device_s1_pos_y[NGRAINS+NFIBERS+NWALL-1],device_s1_pos_y[NGRAINS+NFIBERS+NWALL-1],1.-device_bubble_volume[0]/((X2-X1)*device_s1_pos_y[NGRAINS+NFIBERS+NWALL-1]),device_sw[0],t*u0/device_s1_pos_y[NGRAINS+NFIBERS+NWALL-1]);
      }
    }

    // this kernel computes the distance between two bubbles, taking into account periodic boundary conditions
    double __device__ device_distance_array_abs_i_pared(double sA_pos_x,double sA_pos_y,double sB_pos_x,double sB_pos_y){
      double d=0.,dx,dy;

      dx=fabs(sB_pos_x -sA_pos_x);
      if(dx > LXd/2.){				// bubbles can only ever be half the system length away from each other in any direction due to periodic boundary condidions.
        dx=dx-LXd;
      }


      dy=fabs(sB_pos_y -sA_pos_y);
      //      if(dy > LYd/2.){
      //          dy=dy-LYd;
      //          }

      d = sqrt(pow(dx,2)+pow(dy,2));

      return d;
    }

    // computes the euclidean norm of a vector
    double __device__ device_norm(vector v){
      double n=sqrt(v.x*v.x+v.z*v.z);
      return n;
    }

    // computes periodic boundary condition on cells, that is -1 maps to NX-1, and NX+1 maps to 1, etc
    int __device__ device_CPI (int i){
      int icp;
      if(i<=NX-1){
        if(i>=0){
          icp=i;
        }
        else{
          icp=i+NX;
        }
      }
      else{
        icp=i-NX;
      }
      return icp;
    }

    // periodic boundary conditions on bubble positions, if X > X2 it is remapped to X-LX, andif X< X1 its remapped to X+LX
    double __device__ device_XCP(double x, double LX){

      double xcp=x;
      if(x>=X2){
        while(xcp>=X2)
          xcp =xcp-LX;
      }
      else if(x<X1){
        while(xcp<X1)
          xcp =xcp+LX;
      }
      return xcp;
    }

    // periodic boundary condition on cells in y dimension
    int __device__ device_CPJ(int j) {
      int jcp;
      if(j<=NY-1){
        if(j>=0) jcp=j;
        else jcp=j+NY;
      }
      else jcp=j-NY;
      return jcp;
    }
    // periodic boundary conditions on bubble poisition in y dimension
    double __device__ device_YCP(double y, double LY) {
      double ycp=y;
      if(y>=Z2){
        while(ycp>=Z2)
          ycp =ycp-LY;
      }
      else if(y<Z1){
        while(ycp<Z1)
          ycp =ycp+LY;
      }
      return ycp;
    }

    // computes the unit normal vector between bubble A and B, oreiented from A to B
    vector __device__ device_normal_vector_array_pared(double sA_pos_x,double sA_pos_y,double sB_pos_x,double sB_pos_y){

      vector n12;
      double N;

      n12.x=sB_pos_x-sA_pos_x;
      if(n12.x<=-LXd/2.) n12.x=n12.x+LXd;
      else if(n12.x >=LXd/2.) n12.x=n12.x-LXd;

      n12.z=sB_pos_y-sA_pos_y;
      if(n12.z<=-LYd/2.) n12.z=n12.z+LYd;
      else if(n12.z >=LYd/2.) n12.z=n12.z-LYd;


      N=device_norm(n12);
      n12.x=n12.x/N;
      n12.z=n12.z/N;

      return n12;
    }

    // Provides a double atomic add using multiple atomic CAS (compare and swaps) (taken from cuda  c users guide)
    __device__ double double_atomicAdd(double* address, double val)
    {
      unsigned long long int* address_as_ull = (unsigned long long int*)address;
      unsigned long long int old = *address_as_ull, assumed;
      do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val + __longlong_as_double(assumed)));
        // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
      } while (assumed != old);
      return __longlong_as_double(old);
    }

    // Provides a double atomic add using multiple atomic CAS (compare and swaps) (taken from cuda  c users guide)
    __device__ int int_atomicAdd(int* address, int val)
    {
      unsigned long long int* address_as_ull = (unsigned long long int*)address;
      unsigned long long int old = *address_as_ull, assumed;
      do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, (val + (assumed)));
        // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
      } while (assumed != old);
      return (old);
    }

    // zeros the lists containing bubbles moving in and out of cells.
    void __global__ zero_cells_old(cell_up* cell_list, int Xblocks){

      int idx=blockIdx.x*blockDim.x + threadIdx.x;

      int gid=idx;
      int m=0;
      if(gid < ((NX+1)*(NY+1))){
        for(m=0;m<9;m++){
          cell_list[gid].bubout[m]=0;
          cell_list[gid].bubin[m]=0;
        }
        cell_list[gid].countout=0;
        cell_list[gid].countin=0;
      }
    }



#undef MBIG
#undef MSEED
#undef MZ
#undef FAC
