// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	Constants   :- holedef.h    
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
// #include <gsl/gsl_linalg.h>

// Les constantes physiques
#define GRAVITY 9.8

//#define M_PI (M_PI)
#define PIC  4./3.*M_PI
// Les grains
#define NBUBBLES 1000
#define NGRAINS NBUBBLES
//+(int)(LHOLE*(HFINAL/4.-DHOLE)/(M_PI*R_B*R_B))
#define DENSITY 10.
#define RHO_water 1000

#define R_B 1.e-4
#define R_G 1.e-4
#define M_B 4./3.*M_PI*R_B*R_B*R_B*DENSITY

#define M_G 4./3.*M_PI*R_G*R_B*R_B*DENSITY
#define bubble_volume 0
// Deborah number
#define DE 8.E-4

//Taken from line 41 of fcouette2D.h

#define LFIBERS 50
#define NF 8
#define NFIBERS NF*LFIBERS


#define RIGIDITY (double)(100)
#define R_F (double)(0.67*R_B)
#define M_F 4./3.*M_PI*R_F*R_F*R_F*DENSITY
#define RM (double)(1.8*R_F)


#define K_G 300
#define GAMMA_G 0.1
#define MU_G 0.58
#define STIFFNESS (double)(RIGIDITY*K_G*R_G)

//#define TCOLL ((double)(M_PI*sqrt(M_G/K_G)))
///sqrt(1.-GAMMA_G*GAMMA_G/(4*M_G*K_G))

#define STICKY K_G*R_G/5.

#define GCOULOMB 50

#define COHESION 0.*M_G*GRAVITY
#define K_B 0.3

#define MU_B 0.3


// Le FLUIDE

// Dynamic viscosity (in Pa.s)
#define VISCOSITY 30

// Bubble-bubble friction coefficient
#define friction_coeff (double)(VISCOSITY*R_B)

// Exponent of the bubble-bubble friction
#define ALPHA 1.

// Ratio between wall-bubble and bubble-bubble friction
#define Facteur 0.


#define GAMMA_B 1.e-2*VISCOSITY

// #define tau_d (double)(VISCOSITE*R_B/K_B)
#define tau_d (double)(friction_coeff/K_G)
#define tau_v (double)(M_G/friction_coeff)


// FOAM PROPERTIES

// Shear velocity (of the upper wall) (m/s)


#define DE 8.E-4


#define U0 (double)(DE*HFINAL*K_G/(VISCOSITY*R_B))



// Slope (gravity)
#define sinTHETA 0.

// Effective liquid fraction
#define phi0 0.1

// Polydispersity
#define poly 10.e-2

// DEFINING THE WALLS

// Position of the walls (m)


#define dossier 0

#define X1 -100*R_B
#define X2 100*R_B


#define XP (double)(-30*R_B)
#define Z1 0.
#define Z2 500*R_B

#define HFINAL 0.09024

#define NWALL (int)(2*(X2-X1)/(2*R_B)+2)

#define VFAST 0.02
#define VSLOW 0.003
// #define VX 0.003

// NUMBER OF BUBBLES ON THE WALLS (82)
//#define NWALL 82
//(int)((X2-X1)/(R_B*1.))

#define ANGLE M_PI/2.
// NUMBER OF BUBBLES IN THE OBSTACLE
//newhole1.05
// #define NHOLE (int)(2*(HFINAL)/(2*R_B)+4)
//newhole 1.04
#define NHOLE 20
//(int)((HFINAL-DHOLE)/(2*R_B)+1+40)
// DIAMETER OF THE HOLE
#define DHOLE HFINAL
//HFINAL/5.
#define LHOLE 0.5e-4 
//HFINAL/4.
#define EPSIHOLE 1.e-4
//R_B


// NUMBER OF CELLS IN EACH DIRECTION
#define NX (int)((X2-X1)/(3*R_B))
#define NZ (int)((Z2-Z1)/(3*R_B))

#define NCONTACTS 10

// MECHANICAL PROPERTIES OF THE WALLS (if needed)
#define K_SOL 40000.
# define GAMMA 0.5
#define MU_SOL 1.


// ITERATIONS

// Print position files every ... steps
//#define print_step 10000
#define print_step 10

// Time-step
#define dt (double)(2.0e-7)

// Total number of iterations
//#define NITER (int)(8000*print_step)
#define NITER (int)(80*print_step)



// INITIAL CONDITIONS

// init -1 = start from scratch
// init N = start from the file N in the given directory
#define init -1
#define directory0 600

// not quite sure what this is, but I think it's for pushing in bubbles
#define VDOWN 0.

