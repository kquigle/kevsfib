

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	OPERATIONS  :- misc.c
// 
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 




#include "BubbleModel.h"

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMPUTE THE WEIGHT (usually useless)
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

force gravite(sphere s){
  force poids={0.,GRAVITY*s.mass};
  //	printf("test poids %f %f %f \n", GRAVITY, s.mass, poids.z);

  return poids;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMMON OPERATIONS ON VECTORS, SPHERES, etc
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


double mom(vector v,force f){
  return f.x*v.z-f.z*v.x;
}

/*
   vector vs(sphere s1,sphere sB){

   return;
   }
   */

double distance(sphere sA,sphere sB) {
  double d=0.;

  if(fabs(sB.position.x-sA.position.x)<LX/2.)
    d=sqrt(pow(sA.position.x-sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
  else {
    if(sB.position.x-sA.position.x>=LX/2.)
      d=sqrt(pow(LX+sA.position.x-sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
    /*if(sA.position.x-sB.position.x>LX/2.)*/
    else d=sqrt(pow(LX-sA.position.x+sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
  }
  return d;
}



double norm(vector v)
{
  double n=sqrt(v.x*v.x+v.z*v.z);
  return n;
}


double angle(vector v)
{

  double theta=atan(v.z/v.x);
  return theta;
}


/* Routines de Conditions Periodiques*/
int CP(int i) {
  int icp;
  if(i<=NX-1){
    if(i>=0) icp=i;
    else icp=i+NX;
  }
  else icp=i-NX;
  return icp;
}

double XCP(double x) {
  double xcp=x;
  if(x>=X2){
    while(xcp>=X2)
      xcp =xcp-LX;
  }

  else if(x<X1){
    while(xcp<X1)
      xcp =xcp+LX;
  }
  /*
     if(x>=X1) xcp=x;
     else xcp=x+LX;
     }
     else xcp=x-LX;
     */
return xcp;
}


