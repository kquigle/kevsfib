#include "holedef.h"
#ifndef BUBBLEMODEL_H
#define BUBBLEMODEL_H

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	STRUCTURES  :- BUBBLEMODEL.H
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

// A double 2-vector
typedef struct{
  double x;
  double z;
} vector;

typedef vector force;

// An int 2-vector
typedef struct{
  int i;
  int k;
} vectint;


typedef struct{
  int bubin[9];
  int bubout[9];
  int countin;
  int countout;
} cell_up;
#endif
#ifndef vectorint_struct
#define vectorint_struct
// An int 2-vector
#endif
#ifndef __contact_list__
#define __contact_list__
typedef struct{
  int contacts[9];
  int count;
} contact_list;
#endif

#ifndef __overlap_list__
#define __overlap_list__
typedef struct{
  double overlap[9];
} overlap_list;
// A sphere
typedef struct{
  int index;
  double theta;
  vector position;
  vector velocity;
  double radius;
  double mass;
  double omega;
  vectint cell;
  int fiber;
  int voisins[NCONTACTS];
} sphere;

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	GLOBAL VARIABLES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

// Time t
extern double t;
// Dimensions of the box
extern double LX;
extern double LZ;

// Working directory
extern char directory[70];

// The lists of bubbles
//CAREFUL: first bubble is j=1;
extern sphere b1[NBUBBLES+NWALL+NFIBERS+1],b2[1+NBUBBLES+NWALL+NFIBERS],b0[1+NBUBBLES+NWALL+NFIBERS];

// Total force on a bubble
extern force forces[NBUBBLES+NWALL+1+NFIBERS];
// Total viscous force on a bubble
extern force forcesv[NBUBBLES+NWALL+1+NFIBERS];

double torque[NGRAINS+NFIBERS+NWALL+1];
// Contact lists
extern int HoL[NX+1][NZ+1];
extern int AL[1+NBUBBLES+NWALL+NFIBERS],PL[1+NBUBBLES+NWALL+NFIBERS];



// Total forces on the walls: transverse (upper wall), normal (bottom wall), tangential (viscous part)
extern double upforce;
extern double doforce;
extern double xforceg;
extern double xforced;

// Liquid fraction and bubble area
extern double liquid_fraction;
extern double bubble_area;

// Various things
extern double dt2;
extern double dteff;
extern double Z3;
extern double U1;
extern double tstart;
extern int iter;
extern double cste_stokes;
extern double overlap;
extern double overlap1;
extern double overlap0;
extern int i_cell,k_cell,iv,kv;
extern int switchy;
extern int switchy0;

extern int debit;


// All routines and functions
// init.c
void initialisation();
//equil.c
void evolution();
void euler(int);
void verlet(int);
force gravite(sphere s);
force plate_friction(sphere);
force bounce(sphere);
void collision(int,int,sphere, sphere,double,double);
double norm(vector);
double angle(vector);
double distance(sphere, sphere);
void list_update(int);
double mom(vector,force);
vector vs(sphere sA,sphere sB);
int CP(int);
double XCP(double);
void contacts(int,int);
float ran3(long *idum);


#endif
