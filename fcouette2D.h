 // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	Constants   :- fcouette2D.h    
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
// #include <gsl/gsl_linalg.h>

#define beams_on

// Deborah number
#define DE 8.E-4

// Les constantes physiques
#define GRAVITY 0.
#define GRAVITYX GRAVITY*sin(SLOPE)
#define GRAVITYY GRAVITY*cos(SLOPE)

#define PI 3.14159
#define PIC 4./3.*PI

// Les grains

#define NGRAINS 5000
	//1200
#define DENSITY 10.
#define RHO_water 1000

#define R_G 1e-3
#define M_G 4./3.*PI*R_G*R_G*R_G*DENSITY

#define LFIBERS 50
#define NF 8
#define NFIBERS NF*LFIBERS
#define ANGLE PI/3.
#define RIGIDITY (double)(100)
#define R_F (double)(0.67*R_G)
#define M_F 4./3.*PI*R_F*R_F*R_F*DENSITY
#define RM (double)(1.8*R_F)

#define K_G 300
#define GAMMA_G 0.1
#define MU_G 0.58
#define STIFFNESS (double)(RIGIDITY*K_G*R_G)

#define TCOLL PI*sqrt(M_G/K_G)
///sqrt(1.-GAMMA_G*GAMMA_G/(4*M_G*K_G))

#define STICKY K_G*R_G/5.

#define GCOULOMB 50

#define COHESION 0.*M_G*GRAVITY

// Le FLUIDE

// Dynamic viscosity (in Pa.s)
#define VISCOSITY 30

// Bubble-bubble friction coefficient 
#define friction_coeff (double)(VISCOSITY*R_G)

// Exponent of the bubble-bubble friction
#define ALPHA 1.

// Ratio between wall-bubble and bubble-bubble friction
#define Facteur 0.

// #define GAMMA_B 1.e-2*VISCOSITY

#define tau_d (double)(friction_coeff/K_G)
#define tau_v (double)(M_G/friction_coeff)


// FOAM PROPERTIES

// Shear velocity (of the upper wall) (m/s)
#define U0 (double)(DE*HFINAL*K_G/(VISCOSITY*R_G))

// Effective liquid fraction
#define phi0 0.1

// Polydispersity
#define poly 20.e-2

// COHESIVE PROPERTIES
#define BRIDGE 0.25*R_G

// DEFINING THE WALLS

// Position of the walls
#define dossier 0
#define Y1 0.
#define Y2 500*R_G
//1000*R_G
// #define X3 -dossier*R_G
// #define X4 0*R_G
#define X1 -100*R_G
#define X2 100*R_G
//3000*R_G

#define HFINAL 0.09024

#define NWALL (int)(2*(X2-X1)/(2*R_G)+2)

// NUMBER OF BUBBLES ON THE WALLS

//#define NWALL 2550
//*4       //1200

//#define NBEAMS (int)(4*(NWALL+NGRAINS+1))

// NUMBER OF CELLS IN EACH DIRECTION
#define NX (int)((X2-(X1))/(3*R_G))
#define NY (int)((Y2-(Y1))/(3*R_G))


// alternative NX, NY< NZ to fit compilation problems


// #define NX 48	//1200	
// #define NY 32	//


// MECHANICAL PROPERTIES OF THE WALLS (if needed)
#define K_SOL 40000.
# define GAMMA 0.5
#define MU_SOL 1.


// ITERATIONS

// Print position files every ... steps
#define print_step 10000

// Time-step 
#define dt (double)(2.e-7)
#define dt2 (double)(dt*dt)
// Total number of iterations
// #define NITER (int)(4.00002e-1/dt)
#define NITER 500000000

// INITIAL CONDITIONS

//init -1 = start from scratch
//init N = start from the file N in the given directory

#define init -1
#define directory0 00


