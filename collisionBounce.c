// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	Functions   :- CollisionBounce.c
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

#include "BubbleModel.h"

// Updating the contact lists without rebuilding everything
void list_update(int j)
{

  b2[j].cell.i=CP((int)((b2[j].position.x-X1)*NX/LX));
  b2[j].cell.k=(int)((b2[j].position.z-Z1)*NZ/LZ);

  // If bubble j has moved
  if(b2[j].cell.k!=b1[j].cell.k||b1[j].cell.i!=b2[j].cell.i) {

    // then we rearrange its former cell
    if(AL[j]>0){	PL[AL[j]]=PL[j];}
    else		{HoL[b1[j].cell.i][b1[j].cell.k]=PL[j];	}
    AL[PL[j]]=AL[j];

    // and then we rearrange its new cell

    AL[HoL[b2[j].cell.i][b2[j].cell.k]]=j;
    AL[j]=0;

    PL[j]=HoL[b2[j].cell.i][b2[j].cell.k];

    HoL[b2[j].cell.i][b2[j].cell.k]=j;

    // and we store the new cell
    b1[j].cell.i=b2[j].cell.i;
    b1[j].cell.k=b2[j].cell.k;

  }
  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	LOOKING FOR NEIGHBOURS
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //

void contacts(int j,int critere){

  int epsi,delta,lim3,lim4,j_nb,k;

  // We scan all neighbouring cells in search of a collision
  lim3=-1;
  lim4=1;
  if(b1[j].cell.k==0) lim3=0;
  if(b1[j].cell.k==NZ-1) lim4=0;

  for(epsi=-1;epsi<=+1;epsi++){
    for(delta=lim3;delta<=lim4;delta++){


      iv=CP(b1[j].cell.i+epsi);
      kv=b1[j].cell.k+delta;


      // If the cell is not empty
      if(HoL[iv][kv]>0) {

        // Then let's see if the head of list is a colliding neighbour
        j_nb=HoL[iv][kv];

        // No need to do twice the same contact: if j_nb<j, we've seen it already
        if(j_nb>j){

          overlap1=(b1[j].radius+b1[j_nb].radius)-distance(b1[j],b1[j_nb]);

          // Just check if the overlap is not too big (remove when sure the program runs correctly)
          if(overlap1/b1[j].radius>1.5|b1[j].position.z>Z3) {
            printf("HOUSTON WE HAVE A PROBLEM  A %f, %d %d %f %f\n",overlap1/b1[j].radius,j,j_nb,b1[j].position.x/R_B,b1[j_nb].position.x/R_B);
            exit(0);
          }

          // If overlap is positive, then we have a collision.
          if(overlap1>0.){
            // We'll need to know if it's a new or an old collision
            overlap0=(b0[j].radius+b0[j_nb].radius)-distance(b0[j],b0[j_nb]);
            collision(j,j_nb,b1[j],b1[j_nb],overlap1,overlap0);
            // Let's store the link in the voisins list
            k=0;
            while(b1[j].voisins[k]>0.) {
              k=k+1;}
            b1[j].voisins[k]=j_nb;
            k=0;
            while(b1[j_nb].voisins[k]>0.) {
              k=k+1;}
            b1[j_nb].voisins[k]=j;
          }
        }


        // Once it's done with the Head of List, we can unfold the list for the chosen cell, and do the same stuff
        while (PL[j_nb]>0) {
          j_nb=PL[j_nb];

          if(j_nb>j){
            overlap1=(b1[j].radius+b1[j_nb].radius)-distance(b1[j],b1[j_nb]);

            if(overlap1/b1[j].radius>1.5|b1[j].position.z>Z3) {
              printf("HOUSTON WE HAVE A PROBLEM B %f, %d %d %f %f \n",overlap1/b1[j].radius,j,j_nb,(b1[j].radius+b1[j_nb].radius)/R_B,distance(b1[j],b1[j_nb])/R_B);
              exit(0);
            }


            if(overlap1>0.){
              overlap0=(b0[j].radius+b0[j_nb].radius)-distance(b0[j],b0[j_nb]);
              collision(j,j_nb,b1[j],b1[j_nb],overlap1,overlap0);
              // Let's store the link in the voisins list
              k=0;
              while(b1[j].voisins[k]>0.) {
                k=k+1;}
              b1[j].voisins[k]=j_nb;
              k=0;
              while(b1[j_nb].voisins[k]>0.) {
                k=k+1;}
              b1[j_nb].voisins[k]=j;
            }
          }

        }

      }

    }}

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMPUTE WHAT HAPPENS DURING A COLLISION
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
void collision(int ja,int jb,sphere sA,sphere sB,double delta,double delta0){
  vector n12;
  double N;
  force repulsive_force;
  force friction;
  vector v_n,vr;

  double VRN,VRR;

  // Let's compute the normal vector (oriented sA -> sB)
  n12.x=sB.position.x-sA.position.x;
  n12.z=sB.position.z-sA.position.z;
  if(n12.x<=-LX/2.) n12.x=n12.x+LX;
  else{
    if(n12.x>=LX/2.) n12.x=n12.x-LX;
  }

  N=norm(n12);
  n12.x=n12.x/N;
  n12.z=n12.z/N;

  // Relative velocity of sA with respect to sB
  vr.x=sA.velocity.x-sB.velocity.x;
  vr.z=sA.velocity.z-sB.velocity.z;

  // VRN=norm(vr);
  VRR=norm(vr)/b1[NWALL+NBUBBLES].position.z*R_B;

  // Normal velocity of sA, with respect to sB
  double vn=vr.x*n12.x+vr.z*n12.z;
  v_n.x=vn*n12.x;
  v_n.z=vn*n12.z;


  // Elastic force
  repulsive_force.x=-K_B*2.*R_B/(sA.radius+sB.radius)*delta*n12.x;
  repulsive_force.z=-K_B*2.*R_B/(sA.radius+sB.radius)*delta*n12.z;


  // Dissipative (viscous) force
  if(jb>NBUBBLES){
    friction.x=0.;
    friction.z=0.;
  }
  else {
    friction.x=-friction_coeff*pow(VRR,ALPHA-1.)*vr.x;
    friction.z=-friction_coeff*pow(VRR,ALPHA-1.)*vr.z;
  }
  //  	friction.x=-friction_coeff*pow(VRN,ALPHA-1.)*pow(R_B/b1[NWALL+NBUBBLES].position.z,1.-ALPHA)*vr.x;
  //  	friction.z=-friction_coeff*pow(VRN,ALPHA-1.)*pow(R_B/b1[NWALL+NBUBBLES].position.z,1.-ALPHA)*vr.z;

  if(sA.radius>sB.radius){
    friction.x=friction.x*sB.radius/R_B;
    friction.z=friction.z*sB.radius/R_B;
  }
  else{
    friction.x=friction.x*sA.radius/R_B;
    friction.z=friction.z*sA.radius/R_B;
  }


  forces[ja].x=forces[ja].x+repulsive_force.x+friction.x;
  forces[ja].z=forces[ja].z+repulsive_force.z+friction.z;

  forces[jb].x=forces[jb].x-repulsive_force.x-friction.x;
  forces[jb].z=forces[jb].z-repulsive_force.z-friction.z;

  forcesv[ja].x=forcesv[ja].x+friction.x;
  forcesv[ja].z=forcesv[ja].z+friction.z;

  forcesv[jb].x=forcesv[jb].x-friction.x;
  forcesv[jb].z=forcesv[jb].z-friction.z;


  //  	vector AM={(sA.radius-delta)*n12.x,(sA.radius-delta)*n12.z};

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	BOUNCE IF THERE IS A SOLID BOUNDARY
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
force bounce(sphere s)
{
  // force hooke;
  // vector n12;
  // int condition;
  double N,friction,F_n;
  double delta=0.;
  force tors2;
  double truc;

  tors2.x=0.;
  tors2.z=0.;

  // Rebond sur le mur du haut
  delta=s.position.z+s.radius-Z3;
  if(delta>0.)
  {
    tors2.z=tors2.z-K_B*delta*R_B/s.radius-friction_coeff*(s.velocity.z+VDOWN*(1.-switchy));
    // 	tors2.x=0.;//10.*friction_coeff*U1;
    //if(s.index==822) printf("bounce Z3 \n");
  }
  else{
    // Rebond sur le mur du bas
    delta=Z1-(s.position.z-s.radius);
    if(delta>0.)
    {
      tors2.z=tors2.z+K_B*delta*R_B/s.radius-friction_coeff*s.velocity.z;
      //         tors2.x=0.;
      // 		if(delta/s.radius>1.5) {
      // 		printf("HOUSTON WE HAVE A PROBLEM  Z1 %f, %d %f\n",delta/s.radius,s.index,s.position.x/R_B);
      // 		exit(0);
      // 		}
      //if(s.index==822) printf("bounce Z1 \n");
    }
  }

  // Murs gauches de la constriction
  delta=(s.position.x+s.radius)+LHOLE/2.;
  if(delta>0.&&s.position.x<0.&&(s.position.z>DHOLE/2.+EPSIHOLE+Z3-HFINAL/2.|s.position.z<-(DHOLE/2.+EPSIHOLE)))
  {
    truc=-K_B*delta*R_B/s.radius-friction_coeff*s.velocity.x;
    tors2.x=tors2.x+truc;
    xforceg=xforceg-truc;
    // 	tors2.z=tors2.z-friction_coeff*s.velocity.z;
    //if(s.index==822) printf("bounce haut gauche\n");
  }

  // Murs droits de la constriction
  delta=LHOLE/2.-(s.position.x-s.radius);
  if(delta>0.&&s.position.x>0.&&(s.position.z>DHOLE/2.+EPSIHOLE+Z3-HFINAL/2.|s.position.z<-(DHOLE/2.+EPSIHOLE)))
  {
    truc=+K_B*delta*R_B/s.radius-friction_coeff*s.velocity.x;
    tors2.x=tors2.x+truc;
    xforced=xforced-truc;
    // 	tors2.z=tors2.z-friction_coeff*s.velocity.z;
    //if(s.index==822) printf("bounce haut gauche\n");
  }

  // Mur haut de la constriction
  delta=(s.position.z+s.radius)-(DHOLE/2.+Z3-HFINAL/2.);
  if(delta>0&&fabs(s.position.x)<(LHOLE/2.-EPSIHOLE))
  {
    tors2.z=tors2.z-K_B*delta*R_B/s.radius-friction_coeff*(s.velocity.z+VDOWN*(1-switchy));
    // 	tors2.x=0.;
    //if(s.index==822) printf("bounce haut bas %f %f \n",s.position.z,DHOLE/2.+Z3-HFINAL/2.);
  }

  // Mur bas de la constriction
  delta=-DHOLE/2.-(s.position.z-s.radius);
  if(delta>0&&fabs(s.position.x)<(LHOLE/2.-EPSIHOLE))
  {
    tors2.z=tors2.z+K_B*delta*R_B/s.radius-friction_coeff*(s.velocity.z+VDOWN*(1-switchy));
    // 	tors2.x=0.;
    //if(s.index==822) printf("bounce haut bas %f %f \n",s.position.z,DHOLE/2.+Z3-HFINAL/2.);
  }

  return tors2;
}


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
//
//	COMPUTE THE FRICTION ALONG THE CONFINING PLATES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
force plate_friction(sphere s){

  force stokes;


  stokes.x=-cste_stokes*s.radius/R_B*s.velocity.x;
  stokes.z=-cste_stokes*s.radius/R_B*s.velocity.z;


  return stokes;
}



