import matplotlib.pyplot as plt	
import numpy as np

#This program takes a data number that's been created by poisuille_clean.exe and takes the information from the file and plots it.


# Filenumber is fn
# File is xzr



fn =int( input('Hey!\n \n  Get comfortable.\n Tell me which data file you want plotted.\n Relax.\n\n Take it easy.'))	

#printing from their initial positions
filenum = 90

xzr = np.loadtxt("runfiber_{0:d}/positions{1:06d}.dat".format(fn,filenum), usecols=(0,1,4))
#print (xzr)
with open("sablier_{0:d}/holedef.h".format(fn)) as fi:
	for li in fi:
		if li.startswith("#define NHOLE"):
			hn=int(li[14:])
			print("HoleNumber={}".format(hn))

		if li.startswith("#define NWALL"):
			wn=int(li[14:])
			print("WallNumber={}".format(wn))

max_x = max(xzr[:,0])
min_x = min(xzr[:,0])


max_z = max(xzr[:,1])
min_z = min(xzr[:,1])

fig, ax = plt.subplots()

ax.set_xlim([-max_x,2*max_x])
ax.set_ylim([-max_z,max_z*2])

for x,z,r in xzr[:-hn-wn]:
	circle = plt.Circle((x, z), r, color='k',fill=False)
	ax.add_artist(circle)


for x,z,r in xzr[-hn-wn:-wn]:
	circle = plt.Circle((x, z), r, color='g',fill=False)
	ax.add_artist(circle)

for x,z,r in xzr[-wn:]:
	circle = plt.Circle((x, z), r, color='r',fill=False)
	ax.add_artist(circle)

fig.savefig('plotcircles.png')
plt.show()
	

 




quit()

print("/n well don you cheaky monkey!")
