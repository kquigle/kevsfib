import matplotlib.pyplot as plt	
import numpy as np

#This program takes a data number that's been created by poisuille_clean.exe and takes the information from the file and plots it.


# Filenumber is fn
# File is xzr



#fn =int( input('\n\nHey!\n \nGet comfortable.\n\nTell me which data file you want plotted.\n\nRelax.\n\nTake it easy.\n\n\n'))	
fn = 10
#printing from their initial positions
#filenum = int(input('\n\n And tell me how much should the code proceed?\n\n 1 or 80 are both good numbers.\n\n'))
filenum= 10
xzr = np.loadtxt("runfiber_{0:d}/positions{1:06d}.dat".format(fn,filenum), usecols=(0,1,4))
#print (xzr)
with open("holedef.h".format(fn)) as fi:
	for li in fi:
		if li.startswith("#define LFIBERS"):
			lf=int(li[16:])
			print("lfiber={}".format(lf))

		if li.startswith("#define N_F"):
			nf=int(li[11:])
			print("number of fibers={}".format(nf))

		if li.startswith("#define NHOLE"):
			kf=int(li[14:])
			print("Holenumber={}".format(kf))
		if li.startswith("#define NWALL"):
			wn=int(li[14:])
			print("WallNumber={}".format(wn))


		if li.startswith("#define X31"):
			X31=int(li[12:])
			print("X1={}".format(X31))
		if li.startswith("#define X32"):
			X32=int(li[12:])
			print("X2={}".format(X32))
		if li.startswith("#define Z31"):
			Z31=int(li[12:])
			print("Z1={}".format(Z31))
		if li.startswith("#define Z32"):
			Z32=int(li[12:])
			print("Z2={}".format(Z32))
		if li.startswith("#define R_B"):
			R_B=float(li[12:])
			print("R_B={}".format(R_B))


hn = lf*nf
max_x = max(xzr[:,0])
min_x = min(xzr[:,0])


max_z = max(xzr[:,1])
min_z = min(xzr[:,1])

fig = plt.figure()
ax = fig.add_subplot(1,1,1,aspect='equal')



ax.set_xlim([-max_x,2*max_x])
ax.set_ylim([-max_z,max_z*2])

X1 = X31*R_B
X2 = X32*R_B
Z1 = Z31*R_B
Z2 = Z32*R_B

NX = (X32-X31)/3
NZ = (Z32-Z31)/3



for i in range(NX):
	plt.plot([(i*3*R_B+X1),Z1],[(i*3*R_B+X1),Z2])

for i in range(NZ):
	plt.plot([X1,(i*3*R_B+Z1)],[X2,(i*3*R_B+Z1)])

for i, (x,z,r) in enumerate(xzr[:-hn-wn]):
	circle = plt.Circle((x, z), r, color='k',fill=False)
	ax.add_artist(circle)
  #	ax.text(x,z,'{0}'.format(i))

for x,z,r in xzr[-hn-wn:-wn]:
	circle = plt.Circle((x, z), r, color='g',fill=False)
	ax.add_artist(circle)

for x,z,r in xzr[-wn:]:
	circle = plt.Circle((x, z), r, color='r',fill=False)
	ax.add_artist(circle)

fig.savefig('plotcircles.png')
plt.show()
	

 




quit()

print("/n well don you cheaky monkey!")
