//#include "fdef.h" 
#include "holedef.h"

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	STRUCTURES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// A double 2-vector
typedef struct{
  double x;
  double z;
} vector;

// An int 2-vector
typedef struct{
  int i;
  int k;
} vectint;


// A sphere
typedef struct{
  vector position;
  vector velocity;
  double radius;
  double mass;
  vectint cell;
  int fiber;
} sphere;



// A force (same as vector)
typedef struct{
  double x;
  double z;
} force;


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	GLOBAL VARIABLES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// Time t
double t;
// Dimensions of the box
double LX=X2-X1;
double LZ=Z2-Z1;

// Working directory
char directory[70];

// The lists of bubbles 
//CAREFUL: first bubble is j=1
sphere b1[NBUBBLES+NWALL+NFIBERS+1],b2[1+NBUBBLES+NWALL+NFIBERS],b0[1+NBUBBLES+NWALL+NFIBERS];

// Total force on a bubble
force forces[NBUBBLES+NWALL+NFIBERS+1];
// Total viscous force on a bubble
force forcesv[NBUBBLES+NWALL+NFIBERS+1];

// Contact lists
int HoL[NX][NZ];
int AL[1+NBUBBLES+NWALL+NFIBERS],PL[1+NBUBBLES+NWALL+NFIBERS];

double t0=0.;

// Total forces on the walls: transverse (upper wall), normal (bottom wall), tangential (viscous part)
double upforce=0.,doforce=0.,xforcev=0.,xforcee=0.;

// Liquid fraction and bubble area
double liquid_fraction=0.;
double bubble_area=0.;

// Various things
double dt2=dt*dt;
double dteff=dt;
double Z3=Z2;
double U1=0.;
double tstart;
int iter=2;
double cste_stokes;
double overlap=0.,overlap1=0.,overlap0=0.;
int i_cell,k_cell,iv,kv;
int switchy=0,switchy0;
double U0=0.;

// All routines and functions
void initialisation();
void evolution();
force gravite(sphere s);
force plate_friction(sphere);
force bounce(sphere, sphere,int);
void collision(int,int,sphere, sphere,double,double);
double norm(vector);
double angle(vector);
double distance(sphere, sphere);
void list_update(int);
void euler(int);
void verlet(int);
double mom(vector,force);
vector vs(sphere sA,sphere sB);
int CP(int);
double XCP(double);
void contacts(int);
void fibertension(int);





// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	MAIN ROUTINE
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// Takes as an argument the number of the directory
int main(int argc, char ** argv)
{
  char chaine[70];

  printf("Let's go !\n");


  // create files
  sprintf(directory,"./runfiber_%s",argv[1]);
  sprintf(chaine,"mkdir ./runfiber_%s",argv[1]);

  printf("We'll be working in the directory%s \n",directory);
  system(chaine);

  sprintf(chaine,"cp ./fdef.h %s",directory);
  system(chaine);

  sprintf(chaine,"cp ./fibers_1.02.c %s",directory);
  system(chaine);

  liquid_fraction=0.;
  bubble_area=0.;
  cste_stokes=Facteur*VISCOSITY;	

  // We start by building the initial state of the foam
  initialisation();

  printf("We have %d bubbles in the bulk and %d on the edges) \n",NBUBBLES,NWALL);//S, NWALL,M_B*1000);


  double TCOLL=M_PI*sqrt(M_B/K_B);

  printf("The time of viscous relaxation is %e \n",tau_d);
  printf("The inertial time is  %e \n",tau_v);

  printf("The elastic timescale is  %e   \n",TCOLL);

  // printf("The shear timescale %e \n",b0[NBUBBLES+NWALL+NFIBERS].position.z/U0);

  printf("The Deborah number is %f \n",DE);
  //U0/b0[NBUBBLES+NWALL+NFIBERS].position.z*tau_d);

  // printf("The typical overlap is %e \n",U0/b0[NBUBBLES+NWALL+NFIBERS].position.z*R_B*friction_coeff/K_B);


  printf("Let's go, then \n \n \n");


  evolution();
  //    }
  // }

  printf("\n I love it when a plan comes together \n");

  return 0;
  }

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	BUILDING THE INITIAL STATE
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

void initialisation(){
  char namefile5[100];
  FILE *fp0;
  float ran3(long *idum);
  long graine=150;
  float radius,z,x,vx,vz,drop;
  double delta=4*R_B;
  //double DD=(35*2*R_B-N_F/2.*delta)/(N_F/2.+1.);
  // double DD=NBUBBLES*3.*R_B/(50.*(N_F/2.+1.));
  double DD=9*R_B;
  int i,j;


  // If we start from scratch, we put the bubbles on a rectangular grid, with some noise
  if(init==-1) {

    sprintf(namefile5,"%s/positions000000.dat",directory);
    fp0=fopen(namefile5,"w");

    // starting by putting the first bubble
    b0[1].velocity.x=0.005*(ran3(&graine)-0.5);
    b0[1].velocity.z=0.;
    b0[1].radius=R_B*(1.+poly*2.*(ran3(&graine)-0.5));
    b0[1].mass=pow(b0[1].radius,3)*4./3.*M_PI*DENSITY;
    b0[1].position.x=X1+3*R_B+b0[1].radius;
    b0[1].position.z=2.5*R_B;
    bubble_area=bubble_area+M_PI*b0[1].radius*b0[1].radius;
    b0[1].fiber=0;

    // then the other bubbles one by one
    for(j=2;j<=NBUBBLES;j++){

      // We pack them by x-layers
      b0[j].velocity.x=0.005*(ran3(&graine)-0.5);
      b0[j].velocity.z=0.;
      b0[j].radius=R_B*(1.+poly*2.*(ran3(&graine)-0.5));
      b0[j].position.x=b0[j-1].position.x+b0[j-1].radius+b0[j].radius*1.4;
      b0[j].position.z=b0[j-1].position.z;
      //for(i=1;i<=N_F;i++){
      //	if(fabs(b0[j].position.x-(-35.*R_B+(i-1)*12*R_B))<2.5*R_B&&b0[j].position.z>3.e-3) 
      //b0[j].position.x=XCP(b0[j].position.x+5*R_B);
      //if(b0[j].position.z>5*R_B+(i-1)*12*R_B&&b0[j].position.z<5*R_B+i*12*R_B)
      //b0[j].position.z=b0[j].position.z+5*R_B;
      //}
      // Go to the next layer
      if(b0[j].position.x>X2-R_B*2.5){
        b0[j].position.x=X1+2.2*R_B+R_B*0.5*(ran3(&graine)-0.5);
        b0[j].position.z=b0[j].position.z+R_B*(2.3+poly);
      }
      for(i=1;i<=N_F;i++){
        //      if(fabs(b0[j].position.x-(-35.*R_B+(i-1)*12*R_B))<2.5*R_B&&b0[j].position.z>3.e-3)
        //b0[j].position.x=XCP(b0[j].position.x+5*R_B);
        // if(fabs(b0[j].position.z-(DD+delta/2.+(i-1)*(DD+delta)))<delta/2.)
        if(fabs(b0[j].position.z-((DD+delta/2.)+(i-1.)*(DD+delta)))<delta/2.)
          b0[j].position.z=b0[j].position.z+delta;
      }

      b0[j].mass=pow(b0[j].radius,3)*4./3.*M_PI*DENSITY;
      bubble_area=bubble_area+M_PI*b0[j].radius*b0[j].radius;
      b0[j].fiber=0;
    }

    // Now let's create vertical fibers around x=-10R_B+i*7R_B
    for(j=NBUBBLES+1;j<=NBUBBLES+NFIBERS;j++){
      i=(int)((j-NBUBBLES)/LFIBERS)+1;
      b0[j].velocity.x=0.;
      b0[j].velocity.z=0.;
      b0[j].radius=R_F;
      b0[j].fiber=(int)((j-NBUBBLES-1)/LFIBERS)+1;
      if((j-NBUBBLES-1)%LFIBERS==0){
        /*if(i%2==1)*/  b0[j].position.x=XCP(X1+70*R_B*(ran3(&graine)));
			printf("XPos Fib = %d", b0[j].position.x);
        // else b0[j].position.x=XCP(b0[j-1].position.x+8*R_B);
        b0[j].position.z=DD+delta/2.+(i-1.)*(DD+delta)+1.5*R_F*ran3(&graine);
      }
      else	{
        b0[j].position.x=XCP(b0[j-1].position.x+1.8*R_F);
        b0[j].position.z=b0[j-1].position.z;
      }
      b0[j].mass=M_F;
      bubble_area=bubble_area+M_PI*R_F*R_F;
    }

    // Now we stack bubbles on the walls, symmetrically 
    for(j=NBUBBLES+NFIBERS+1;j<=NBUBBLES+NFIBERS+NWALL/2;j++){
      b0[j].velocity.x=0.;
      b0[j].velocity.z=0.;
      b0[j+NWALL/2].velocity.x=0.;
      b0[j+NWALL/2].velocity.z=0.;
      b0[j].radius=R_B*(1.+poly*2.*(ran3(&graine)-0.5));
      b0[j+NWALL/2].radius=b0[j].radius;
      b0[j].fiber=0;
      b0[j+NWALL/2].fiber=0;
      if(j==NBUBBLES+NFIBERS+1) {
        b0[j].position.x=X1+b0[j].radius;
        b0[j+NWALL/2].position.x=X1+b0[j+NWALL/2].radius;
      }
      else{
        if(j==(NBUBBLES+NFIBERS+NWALL/2)) {
          b0[j].radius=.5*(X2-(b0[j-1].position.x+b0[j-1].radius));
          b0[j+NWALL/2].radius=.5*(X2-(b0[j-1+NWALL/2].position.x+b0[j-1+NWALL/2].radius));
        }

        b0[j].position.x=b0[j-1].position.x+b0[j-1].radius+b0[j].radius;
        b0[j+NWALL/2].position.x=b0[j-1+NWALL/2].position.x+b0[j-1+NWALL/2].radius+b0[j+NWALL/2].radius;
      }

      b0[j].position.z=0.;
      b0[j+NWALL/2].position.z=b0[NBUBBLES].position.z+2.5*R_B;
      b0[j].mass=pow(b0[j].radius,3)*4./3.*M_PI*DENSITY;

      // each wall bubble counts for 1/2 in the surface, but it's symmetrical
      bubble_area=bubble_area+M_PI*R_B*R_B;
    }

    for(j=1;j<=NBUBBLES+NWALL+NFIBERS;j++){
      fprintf(fp0,"%.12f \t %.12f \t %.12f \t %.12f \t %.6f \n",b0[j].position.x,b0[j].position.z,b0[j].velocity.x,b0[j].velocity.z,b0[j].radius);
    }

    fclose(fp0);
  }

  // Else, we start by reading a position file, and we take the initial positions from there
  else{
    sprintf(namefile5,"./start_8fib_1.dat");
    printf("I'm reading the file %s \n",namefile5);
    fp0=fopen(namefile5,"r");

    for(j=1;j<=NBUBBLES+NWALL+NFIBERS;j++){
      fscanf(fp0,"%f \t %f \t %f \t %f \t %f \n",&x,&z,&vx,&vz,&radius);

      b0[j].position.x=(double)(x);
      b0[j].position.z=(double)(z);

      b0[j].radius=(double)(radius);
      b0[j].mass=(double)(pow(b0[j].radius,3)*4./3.*M_PI*DENSITY);

      b0[j].velocity.x=DE*b0[j].position.z/tau_d;
      b0[j].velocity.z=0.;

      if(j<=NBUBBLES) b0[j].fiber=0;
      else  {
        if(j>NBUBBLES+NFIBERS) b0[j].fiber=0;
        else   b0[j].fiber=(int)((j-NBUBBLES-1)/LFIBERS)+1;
      }

      if(j<=NBUBBLES+NFIBERS)	bubble_area=bubble_area+M_PI*b0[j].radius*b0[j].radius;
      else 	bubble_area=bubble_area+M_PI*R_B*R_B/2.;

    }
    for(j=1;j<=NBUBBLES+NWALL+NFIBERS;j++){
      // 		if(j<=NBUBBLES+NFIBERS+NWALL/2.){

      // 		printf("YOUPI %d %f\n",j,b0[j].position.z/b0[NBUBBLES+NWALL+NFIBERS].position.z);

      // 		}
      // 		else{
      // 		b0[j].velocity.x=U0;
      // 		b0[j].velocity.z=0.;
      // 		}
    }

    fclose(fp0);
  }

  printf("The bubble assembly has been successfully built !\n");
}


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	EVOLUTION ROUTINE
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

void evolution() {
  char namefile3[70],namefile2[70],namefile1[70],namefile4[70],namefile0[70];
  //char namefile5[30],namefile1[30],
  FILE *fp2,*fp4,*fp5,*fp3=0;
  int init2,ic,kc,i,j,jv;
  float ran3(long *idum);
  long graine=1;
  double dd;


  // The contact lists are set to zero
  for(ic=0;ic<=NX-1;ic++)
    for(kc=0;kc<=NZ-1;kc++){
      HoL[ic][kc]=0;
    }
  PL[0]=0;
  AL[0]=0;

  printf("Contact list is ready !\n");


  // We create useful files

  sprintf(namefile0,"%s/forces_%.3f_%.4f_%.1f.dat",directory,phi0,U0,Facteur);
  fp5=fopen(namefile0,"a");
  fclose(fp5);

  if(init==-1){
    sprintf(namefile2,"%s/positions000001.dat",directory);
    fp2=fopen(namefile2,"w");
  }
  else {
    sprintf(namefile2,"%s/positions%.6d.dat",directory,init);
    fp2=fopen(namefile2,"w");
  }

  sprintf(namefile3,"%s/contacts000.dat",directory);
  //		fp1=fopen(namefile3,"w");



  // We initialize all bubbles
  for(j=1;j<=NBUBBLES+NFIBERS+NWALL;j++){
    // b0 is the bubble at timestep  i-1
    // b1                                       i
    // b2                                       i+1

    b1[j]=b0[j];
    b2[j]=b0[j];

    PL[j]=0;
    AL[j]=0;
    forces[j].x=0.;
    forces[j].z=0.;
    forcesv[j].x=0.;
    forcesv[j].z=0.;
    // and print the first position file
    fprintf(fp2,"%.12f \t %.12f \t %.12f \t %.12f \t %.6f \n",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius);
  }


  /*if(init==-1)*/ fclose(fp2);

  printf("Please fasten your seat belt\n");

  if(init==-1) {
    init2=0;
  }
  else {
    init2=init;
  }

  if(init==-1) switchy=0;
  else switchy=1;


  // The first iteration has to be integrated with an Euler method
  iter=init2*print_step+1;


  // what's the time ?
  t=iter*dt;

  // For each bubble, we compute the cell number, and adjust the neighbourhood
  for(j=1;j<=NBUBBLES+NFIBERS+NWALL;j++){

    b1[j].cell.i=CP((int)((b1[j].position.x-X1)*NX/LX));
    b1[j].cell.k=(int)((b1[j].position.z-Z1)*NZ/LZ);

    PL[j]=HoL[b1[j].cell.i][b1[j].cell.k];
    AL[HoL[b1[j].cell.i][b1[j].cell.k]]=j;
    HoL[b1[j].cell.i][b1[j].cell.k]=j;
  }


  // Now for each bubble except the walls
  for(j=1;j<=NBUBBLES+NFIBERS;j++){
    b2[j]=b1[j];
    //Looking for contacts
    contacts(j);
    for(i=1;i<=N_F;i++){
      if(j>NBUBBLES+(i-1)*LFIBERS&&j<NBUBBLES+i*LFIBERS) fibertension(j);
    }
  }
  for(j=1;j<=NBUBBLES+NFIBERS;j++){
    // and integrate new positions
    euler(j);
  }

  // Now for each bubble
  for(j=1;j<=NBUBBLES+NWALL+NFIBERS;j++){
    // we update the contact list
    list_update(j);
    // and replace old positions by new ones
    b0[j].position=b1[j].position;
    b0[j].velocity=b1[j].velocity;
    b1[j].position=b2[j].position;
    b1[j].velocity=b2[j].velocity;
    // all forces are reset to zero
    forces[j].x=0.;
    forces[j].z=0.;
    forcesv[j].x=0.;
    forcesv[j].z=0.;
  }

  // 	 		sprintf(namefile2,"%s/positions%.6db.dat",directory,init);
  // 		fp2=fopen(namefile2,"w");
  // for(j=1;j<=NBUBBLES+NFIBERS+NWALL;j++){
  //  fprintf(fp2,"%.12f \t %.12f \t %.12f \t %.12f \t %.6f \n",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius);
  // }
  // fclose(fp2);


  // NOW WE CAN GO FOR THE REAL THING (Verlet integration)

  printf("You signed up for %d iterations, with U0=%f and phi0= %f \n\n\n",NITER,U0,phi0);


  // b1[NBUBBLES+1].velocity.z=0.;
  // b1[NBUBBLES+1].velocity.x=0.5;

  for(iter=2;iter<=NITER;iter++){

    bubble_area=0.;
    t=iter*dt;

    // For each bubble 	
    for(j=1;j<=NBUBBLES+NFIBERS;j++){
      b2[j]=b1[j];
      bubble_area=bubble_area+M_PI*b1[j].radius*b1[j].radius;
      // If we have solid walls and/or friction along the confining plate and/or gravity

      // 	forces[j].x=forces[j].x/*+bounce(b1[j],b0[j],switchy).x*/+plate_friction(b1[j]).x;
      // 	forces[j].z=forces[j].z+/*bounce(b1[j],b0[j],switchy).z*/+plate_friction(b1[j]).z;
      // Let's look for all contacts
      contacts(j);
      // if(j>NBUBBLES) {
      //   forces[j].z=forces[j].z-GRAVITY*b1[j].mass;
      for(i=1;i<=N_F;i++){
        if(j>NBUBBLES+(i-1)*LFIBERS&&j<NBUBBLES+i*LFIBERS) fibertension(j);
      }
      //	if(j==NBUBBLES+1&&iter%(print_step/10)==0) printf("\t %.12f \n",forces[NBUBBLES+1].z);
      // }
    }
    for(j=1;j<=NBUBBLES+NFIBERS;j++){
      // and integrate over time
      verlet(j);
    }

    // b2[NBUBBLES+1].position.z=b1[NBUBBLES+1].position.z;
    // b2[NBUBBLES+1].position.x=b1[NBUBBLES+1].position.x;
    // b1[NBUBBLES+1].velocity.z=0.;
    // b1[NBUBBLES+1].velocity.x=0.;

    // for(j=NBUBBLES+1;j<NBUBBLES+NFIBERS;j++){
    // 		b2[j]=b1[j];
    // bubble_area=bubble_area+M_PI*b1[j].radius*b1[j].radius;
    // // If we have solid walls and/or friction along the confining plate and/or gravity
    // // and integrate over time
    // verlet(j);
    // }



    // Computing the forces on the wall bubbles
    for(j=NBUBBLES+NFIBERS+NWALL/2+1;j<=NBUBBLES+NFIBERS+NWALL;j++){
      upforce=upforce+forces[j].z;
      xforcev=xforcev+forcesv[j].x;
      xforcee=xforcee+forces[j].x-forcesv[j].x;
      bubble_area=bubble_area+M_PI*R_B*R_B/2.;
    }
    for(j=NBUBBLES+NFIBERS+1;j<=NBUBBLES+NFIBERS+NWALL/2;j++){
      doforce=doforce+forces[j].z;
      xforcev=xforcev-forcesv[j].x;
      xforcee=xforcee-forces[j].x+forcesv[j].x;
      bubble_area=bubble_area+M_PI*R_B*R_B/2.;
    }
    //Computing the liquid fraction
    liquid_fraction=1.-bubble_area/((X2-X1)*(b1[NBUBBLES+NFIBERS+NWALL-1].position.z-Z1));

    // We impose the motion of the upper wall bubbles: 
    for(j=NBUBBLES+NFIBERS+NWALL/2+1;j<=NBUBBLES+NFIBERS+NWALL;j++){
      // First downward to give the foam the chosen liquid fraction
      if(liquid_fraction>phi0&&switchy==0)	{
        b2[j].position.z=b1[j].position.z-0.1*dt;
        Z3=b2[j].position.z;
        U1=0.;
        b2[j].position.x=b1[j].position.x;
        t0=t;
      }
      // and then with a given x-velocity U0
      else{
        U0=b1[NBUBBLES+NFIBERS+NWALL].position.z/tau_d*DE;
        switchy=1;
        b2[j].position.z=b1[j].position.z;
        b2[j].velocity.x=U0;
        b2[j].position.x=XCP(b1[j].position.x+U0*dt);
      }
    }

    // Every 'print_step' timesteps, we print out a position file, and other useful stuff
    if(iter%print_step==0) {
      sprintf(namefile3,"%s/positions%.6d.dat",directory,(int)(init2+iter/print_step));
      fp3=fopen(namefile3,"w");

      // Print out positions, speeds, forces for each bubble
      for(j=1;j<=NBUBBLES+NWALL+NFIBERS;j++){
        fprintf(fp3,"%.12f \t %.12f \t %.12f \t %.12f \t %.12f \t %.6f \t %.12f \t %.12f \t %.12f  \n",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius,forces[j].x-forcesv[j].x,forcesv[j].x,forces[j].z-forcesv[j].z,forcesv[j].z);
        //fprintf(fp3,"%.12f \t %.12f \t %.12f \t %.12f \t %.12f  \n",b1[j].position.x,b1[j].position.z,b1[j].velocity.x,b1[j].velocity.z,b1[j].radius);
      }	

      printf("hello there i=%d ... gamma= %f     epsilon=%f     U1=%f    Z3=%f \n",iter,t*U0/b1[NWALL+NBUBBLES+NFIBERS].position.z,liquid_fraction,U0,b1[NBUBBLES+NWALL+NFIBERS].position.z);
      // Print out the stresses on walls
      if(switchy==1){
        // Give some news on screen
        //printf("hello there i=%d ... gamma= %f     epsilon=%f     U1=%f    Z3=%f\n",iter,t*U0/b1[NWALL+NBUBBLES+NFIBERS].position.z,liquid_fraction,U0,b1[NBUBBLES+NWALL+NFIBERS].position.z);
        fp5=fopen(namefile0,"a");
        fprintf(fp5,"%.6f \t %.12f \t %.12f \t %.12f \t %.12f \t %.6f \n",(t-t0)*U0/b1[NWALL+NBUBBLES+NFIBERS].position.z,-(xforcev+xforcee)/print_step,-xforcev/print_step,-xforcee/print_step,(upforce-doforce)/print_step,t);
        fclose(fp5);
      }
      // Reset to zero all global forces
      upforce=0.;
      doforce=0.;
      xforcev=0.;
      xforcee=0.;

      fclose(fp3);

    }


    // Updating contact lists and bubbles
    for(j=1;j<=NBUBBLES+NWALL+NFIBERS;j++){
      list_update(j);
      b0[j]=b1[j];
      b1[j]=b2[j];

      // Reset forces to zero
      forces[j].x=0.;
      forces[j].z=0.;
      forcesv[j].x=0.;
      forcesv[j].z=0.;
    }

    switchy0=switchy;


  }

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	IMPORTANT ROUTINES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

// Lennard Jones
void fibertension(int j){
  double dd,N,nforce,N0,angle,signe12,signe10,ps;
  vector n12, n10,t12,t10;
  int jv,jp;

  //if(j<NBUBBLES+NFIBERS){
  //   for(jv=j+1;jv<=NBUBBLES+NFIBERS;jv++){
  jv=j+1;

  dd=distance(b1[j],b1[jv]);

  // nforce=10.*K_B*R_B*RM/dd*(pow(RM/dd,6.)-pow(RM/dd,12.));
  nforce=K_B*(dd-RM);
  n12.x=b1[jv].position.x-b1[j].position.x;
  n12.z=b1[jv].position.z-b1[j].position.z;
  if(n12.x<=-LX/2.) n12.x=n12.x+LX;
  else{
    if(n12.x>=LX/2.) n12.x=n12.x-LX;
  }
  N=norm(n12);
  n12.x=n12.x/N;
  n12.z=n12.z/N;
  forces[j].x=forces[j].x+nforce*n12.x;
  forces[j].z=forces[j].z+nforce*n12.z;
  forces[jv].x=forces[jv].x-nforce*n12.x;
  forces[jv].z=forces[jv].z-nforce*n12.z;

  // Voyons si on peut ajouter une rigidite tangentielle
  if(j>NBUBBLES+1+(b1[j].fiber-1)*LFIBERS){
    //   printf("YPUOPLAD %d %d \n",j,b1[j].fiber);
    jp=j-1;
    n10.x=b1[jp].position.x-b1[j].position.x;
    n10.z=b1[jp].position.z-b1[j].position.z;
    if(n10.x<=-LX/2.) n10.x=n10.x+LX;
    else{
      if(n10.x>=LX/2.) n10.x=n10.x-LX;
    }
    N0=norm(n10);
    n10.x=n10.x/N0;
    n10.z=n10.z/N0;
    t10.x=n10.z;
    t10.z=-n10.x;
    t12.x=n12.z;
    t12.z=-n12.x;
    if(t12.x*n10.x+t12.z*n10.z<0.) signe12=1.;
    else signe12=-1.;
    if(t10.x*n12.x+t10.z*n12.z<0.) signe10=1.;
    else signe10=-1.;
    ps=n12.x*n10.x+n12.z*n10.z;
    if(ps<-1) {
      //     printf("C QUOI CE BORDEL %.12f %d \n",ps,j);
      ps=-1.;
    }
    angle=acos(ps);
    // printf("ANGLE %f \t %f \t %d\n",n12.x*n10.x+n12.z*n10.z,angle,j);
    nforce=signe12*(M_PI-angle)/M_PI*K_B*R_B*RIGIDITY;
    // printf("nforce %f \t %.16f\t %f\t%d\n",nforce,n12.x*n10.x+n12.z*n10.z,angle,j);
    forces[j].x=forces[j].x-nforce*n12.z;
    forces[j].z=forces[j].z+nforce*n12.x;
    forces[jv].x=forces[jv].x+nforce*n12.z;
    forces[jv].z=forces[jv].z-nforce*n12.x;
    nforce=signe10*(M_PI-angle)/M_PI*K_B*R_B*RIGIDITY;
    forces[j].x=forces[j].x-nforce*n10.z;
    forces[j].z=forces[j].z+nforce*n10.x;    
    forces[jp].x=forces[jp].x+nforce*n10.z;
    forces[jp].z=forces[jp].z-nforce*n10.x;

  }

  //   }
  // }
  return;
}

// Time-integration of positions, with 1st-order Euler method
void euler(int j){
  b2[j].position.x=XCP(b1[j].position.x+dt*b1[j].velocity.x);
  b2[j].velocity.x=b1[j].velocity.x+dt*(forces[j].x/b1[j].mass);
  b2[j].position.z=b1[j].position.z+dt*b1[j].velocity.z;
  b2[j].velocity.z=b1[j].velocity.z+dt*(forces[j].z/b1[j].mass);
  return;
}

// Time-integration of positions and speeds, with 2nd-order Verlet method
void verlet(int j){
  double dx;

  b2[j].position.x=XCP(2.*b1[j].position.x-b0[j].position.x+dt2*forces[j].x/b1[j].mass);

  b2[j].position.z=2.*b1[j].position.z-b0[j].position.z+dt2*forces[j].z/b1[j].mass;
  b2[j].velocity.z=0.5*(b2[j].position.z-b0[j].position.z)/dt;

  // Careful if we are on the sides
  dx=b2[j].position.x-b0[j].position.x;
  if(dx<-LX/2.){
    dx=dx+LX;
  }
  else{
    if(dx>LX/2.) {
      dx=dx-LX;
    }
  }
  b2[j].velocity.x=0.5*dx/dt;
  return;
}


// Updating the contact lists without rebuilding everything
void list_update(int j) 
{

  b2[j].cell.i=CP((int)((b2[j].position.x-X1)*NX/LX));
  b2[j].cell.k=(int)((b2[j].position.z-Z1)*NZ/LZ);

  // If bubble j has moved
  if(b2[j].cell.k!=b1[j].cell.k||b1[j].cell.i!=b2[j].cell.i) {

    // then we rearrange its former cell
    if(AL[j]>0){	PL[AL[j]]=PL[j];}
    else		{HoL[b1[j].cell.i][b1[j].cell.k]=PL[j];	}
    AL[PL[j]]=AL[j];

    // and then we rearrange its new cell

    AL[HoL[b2[j].cell.i][b2[j].cell.k]]=j;
    AL[j]=0;

    PL[j]=HoL[b2[j].cell.i][b2[j].cell.k];

    HoL[b2[j].cell.i][b2[j].cell.k]=j;

    // and we store the new cell
    b1[j].cell.i=b2[j].cell.i;
    b1[j].cell.k=b2[j].cell.k;

  }
  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	LOOKING FOR NEIGHBOURS
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

void contacts(int j){

  int epsi,delta,lim1,lim2,lim3,lim4,j_nb;
  int condition;
  double ffx,ffz;
  double rip;

  // We scan all neighbouring cells in search of a collision
  lim3=-1;
  lim4=1;
  //Accounts for bubbles at the bottom and the top
  if(b1[j].cell.k==0) lim3=0;
  if(b1[j].cell.k==NZ-1) lim4=0;

  for(epsi=-1;epsi<=+1;epsi++){
    for(delta=lim3;delta<=lim4;delta++){

      //iv and kv are the cell index of the neighbouring bubble, 
      iv=CP(b1[j].cell.i+epsi);
      kv=b1[j].cell.k+delta;




      // If the cell is not empty
      if(HoL[iv][kv]>0) {

        // Then let's see if the head of list is a colliding neighbour
        j_nb=HoL[iv][kv];

        // No need to do twice the same contact: if j_nb<j, we've seen it already
        if(j_nb>j&&(j<=NBUBBLES||(j>NBUBBLES&&j_nb>j+1))){

          overlap1=(b1[j].radius+b1[j_nb].radius)-distance(b1[j],b1[j_nb]);

          // Just check if the overlap is not too big (remove when sure the program runs correctly)
          if(overlap1/b1[j].radius>1.5) {
            printf("HOUSTON WE HAVE A PROBLEM  %f, %d %d %f\n",overlap1/b1[j].radius,j,j_nb,b1[j].position.z);
            //exit(0);
          }

          // If overlap is positive, then we have a collision.
          if(overlap1>0.){
            // We'll need to know if it's a new or an old collision
            overlap0=(b0[j].radius+b0[j_nb].radius)-distance(b0[j],b0[j_nb]);
            collision(j,j_nb,b1[j],b1[j_nb],overlap1,overlap0);
          }
        }


        // Once it's done with the Head of List, we can unfold the list for the chosen cell, and do the same stuff
        while (PL[j_nb]>0) {
          j_nb=PL[j_nb];

          if(j_nb>j&&(j<=NBUBBLES||(j>NBUBBLES&&j_nb>j+1))){
            overlap1=(b1[j].radius+b1[j_nb].radius)-distance(b1[j],b1[j_nb]);

            if(overlap1/b1[j].radius>1.5) {
              printf("HOUSTON WE HAVE A PROBLEM %f, %d %d %f %f \n",overlap1/b1[j].radius,j,j_nb,(b1[j].radius+b1[j_nb].radius),distance(b1[j],b1[j_nb]));
             //exit(0);
            }


            if(overlap1>0.){
              overlap0=(b0[j].radius+b0[j_nb].radius)-distance(b0[j],b0[j_nb]);
              collision(j,j_nb,b1[j],b1[j_nb],overlap1,overlap0);
            }
          }

        }

      }

    }}

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	COMPUTE WHAT HAPPENS DURING A COLLISION
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
void collision(int ja,int jb,sphere sA,sphere sB,double delta,double delta0){
  vector n12;
  double N;
  force repulsive_force;
  force friction;
  force dissipation={0.,0.};
  vector v_n,vr;
  vector v_t,u_t;
  force tors;

  double VRN;

  // Let's compute the normal vector (oriented sA -> sB)
  n12.x=sB.position.x-sA.position.x;
  n12.z=sB.position.z-sA.position.z;
  if(n12.x<=-LX/2.) n12.x=n12.x+LX;
  else{
    if(n12.x>=LX/2.) n12.x=n12.x-LX;
  }

  N=norm(n12);
  n12.x=n12.x/N;
  n12.z=n12.z/N;

  // Relative velocity of sA with respect to sB
  vr.x=sA.velocity.x-sB.velocity.x;
  vr.z=sA.velocity.z-sB.velocity.z;

  VRN=norm(vr);

  // Normal velocity of sA, with respect to sB
  double vn=vr.x*n12.x+vr.z*n12.z;
  v_n.x=vn*n12.x;
  v_n.z=vn*n12.z;


  // Elastic force
  repulsive_force.x=-K_B*2.*R_B/(sA.radius+sB.radius)*delta*n12.x;
  repulsive_force.z=-K_B*2.*R_B/(sA.radius+sB.radius)*delta*n12.z;


  // Dissipative (viscous) force
  friction.x=-friction_coeff*pow(VRN,ALPHA-1.)*pow(R_B/b1[NWALL+NBUBBLES].position.z,1.-ALPHA)*vr.x;
  friction.z=-friction_coeff*pow(VRN,ALPHA-1.)*pow(R_B/b1[NWALL+NBUBBLES].position.z,1.-ALPHA)*vr.z;

  if(sA.radius>sB.radius){
    friction.x=friction.x*sB.radius/R_B;
    friction.z=friction.z*sB.radius/R_B;
  }
  else{
    friction.x=friction.x*sA.radius/R_B;
    friction.z=friction.z*sA.radius/R_B;
  }


  forces[ja].x=forces[ja].x+repulsive_force.x+friction.x;
  forces[ja].z=forces[ja].z+repulsive_force.z+friction.z;

  forces[jb].x=forces[jb].x-repulsive_force.x-friction.x;
  forces[jb].z=forces[jb].z-repulsive_force.z-friction.z;

  forcesv[ja].x=forcesv[ja].x+friction.x;
  forcesv[ja].z=forcesv[ja].z+friction.z;

  forcesv[jb].x=forcesv[jb].x-friction.x;
  forcesv[jb].z=forcesv[jb].z-friction.z;


  vector AM={(sA.radius-delta)*n12.x,(sA.radius-delta)*n12.z};

  return;
}



// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	BOUNCE IF THERE IS A SOLID BOUNDARY
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
force bounce(sphere s,sphere s0, int critere){
  force hooke;
  vector n12;
  int condition;
  double N,friction,F_n;

  force tors2;
  force dissipation={0.,0.};

  tors2.x=0.;
  tors2.z=0.;



  if(s.position.z+s.radius>Z3)
  {
    double delta=s.position.z+s.radius-b1[NBUBBLES+NWALL].position.z;


    tors2.z=-K_B*delta*R_B/s.radius;
    tors2.x=10.*friction_coeff*U1;

  }

  if(s.position.z-s.radius<Z1)
  {
    double delta=b1[NBUBBLES+1].position.z-(s.position.z-s.radius);

    tors2.z=K_B*delta*R_B/s.radius;
    tors2.x=0.;


  }

  return tors2;
}


// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	COMPUTE THE FRICTION ALONG THE CONFINING PLATES
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
force plate_friction(sphere s){

  force stokes;


  stokes.x=-cste_stokes*s.radius/R_B*s.velocity.x;
  stokes.z=-cste_stokes*s.radius/R_B*s.velocity.z;


  return stokes;
}




// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	COMPUTE THE WEIGHT (usually useless)
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

force gravite(sphere s){  
  force poids={0.,GRAVITY*s.mass};
  //	printf("test poids %f %f %f \n", GRAVITY, s.mass, poids.z);

  return poids;
}





// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	COMMON OPERATIONS ON VECTORS, SPHERES, etc
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 


double mom(vector v,force f){
  return f.x*v.z-f.z*v.x;
}

/*
   vector vs(sphere s1,sphere sB){

   return;
   }
 */

double distance(sphere sA,sphere sB) {
  double d=0.;

  if(fabs(sB.position.x-sA.position.x)<LX/2.)
    d=sqrt(pow(sA.position.x-sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
  else {
    if(sB.position.x-sA.position.x>=LX/2.)
      d=sqrt(pow(LX+sA.position.x-sB.position.x,2)+pow(sA.position.z-sB.position.z,2)); 
    /*if(sA.position.x-sB.position.x>LX/2.)*/
    else d=sqrt(pow(LX-sA.position.x+sB.position.x,2)+pow(sA.position.z-sB.position.z,2));
  }
  return d;
}



double norm(vector v)
{
  double n=sqrt(v.x*v.x+v.z*v.z);
  return n;
}


double angle(vector v)
{

  double theta=atan(v.z/v.x);
  return theta;
}


/* Routines de Conditions Periodiques*/
int CP(int i) {
  int icp;
  if(i<=NX-1){
    if(i>=0) icp=i;
    else icp=i+NX;
  }
  else icp=i-NX;
  return icp;
}

double XCP(double x) {
  double xcp=x;
  if(x>=X2){
    while(xcp>=X2)
      xcp =xcp-LX;
  }

  else if(x<X1){
    while(xcp<X1)
      xcp =xcp+LX;
  }
  /*
     if(x>=X1) xcp=x;
     else xcp=x+LX;
     }
     else xcp=x-LX;
   */
return xcp;
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
//
//	RANDOM NUMBER GENERATOR
//
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 


#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

float ran3(long *idum)
{
  static int inext,inextp;
  static long ma[56];
  static int iff=0;
  long mj,mk;
  int i,ii,k;

  if (*idum < 0 || iff == 0) {
    iff=1;
    mj=MSEED-(*idum < 0 ? -*idum : *idum);
    mj %= MBIG;
    ma[55]=mj;
    mk=1;
    for (i=1;i<=54;i++) {
      ii=(21*i) % 55;
      ma[ii]=mk;
      mk=mj-mk;
      if (mk < MZ) mk += MBIG;
      mj=ma[ii];
    }
    for (k=1;k<=4;k++)
      for (i=1;i<=55;i++) {
        ma[i] -= ma[1+(i+30) % 55];
        if (ma[i] < MZ) ma[i] += MBIG;
      }
    inext=0;
    inextp=31;
    *idum=1;
  }
  if (++inext == 56) inext=1;
  if (++inextp == 56) inextp=1;
  mj=ma[inext]-ma[inextp];
  if (mj < MZ) mj += MBIG;
  ma[inext]=mj;
  return mj*FAC;
}
#undef MBIG
#undef MSEED
#undef MZ
#undef FAC

